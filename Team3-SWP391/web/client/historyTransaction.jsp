<%-- 
    Document   : historyTransaction
    Created on : Jun 20, 2023, 10:44:30 PM
    Author     : hieub
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.User" %>
<%@page import = "model.Transaction" %>
<%@page import = "dao.TransactionDAO" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link
            rel="stylesheet"
            href="https://cardvip.vn/assets/default/plugins/bootstrap/css/bootstrap.min.css"
            />
        <style>
            .external-link {
                cursor: pointer;
                color: blue;
            }

            body {
                background: #eee;
            }

            input[type=checkbox] {
                position: relative;
                width: 15px;
                height: 15px;
                color: #000;
                border: 1px solid grey;
                border-radius: 4px;
                appearance: none;
                outline: 0;
                cursor: pointer;
                transition: background 175ms cubic-bezier(0.1, 0.1, 0.25, 1);
                :before {
                    position: absolute;
                    content: '';
                    display: block;
                    top: 2px;
                    left: 7px;
                    width: 8px;
                    height: 16px;
                    border-style: solid;
                    border-color: #fff;
                    border-width: 0 2px 2px 0;
                    transform: rotate(45deg);
                    opacity: 0;
                }
                checked {
                    color: #000;
                    border-color: green;
                    background: green;
                    :before {
                        opacity: 1;
                    }
                    label :before {
                        clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
                    }
                </style>
            </head>
            <%Object obj = session.getAttribute("account");
   User u = null;
   if(obj !=null){
   u = (User)obj;
                }
   TransactionDAO td = new TransactionDAO();
   List<Transaction> lst = (List<Transaction>) td.getTransactionByUser(5,0,"", "2023-06-18T20:39", "2023-07-25 T06:45", "10000", "999999", "", "","",String.valueOf(u.getId()));
 
   int count = (int) td.countHistoryTransaction("", "2023-06-18T20:39", "2023-07-25 T06:45", "10000", "999999", "","",String.valueOf(u.getId()));
   request.setAttribute("list",lst);
            %>
            <jsp:include page="header.jsp"></jsp:include>
                <body style="margin-top: 8%;">
                    <input hidden="" id="count" value="<%=count%>"/>
                <input hidden="" id="user" value="<%=u.getId()%>"/>
                <div style="width: 90%;
                     margin-left: 5%;" >
                    <div class="table-responsive">
                        <table class="table table-striped table-dark text-dark" style="background-color: white;">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th colspan="2">Mã giao dịch</th>
                                    <th colspan="2">Số tiền</th>
                                    <th colspan="2">Ngày tạo</th>
                                    <th>Chi tiết</th>
                                    <th>Trạng thái</th>
                                    <th>Loại GD</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tr id="loc">
                                <td>Lọc</td>
                                <td colspan="2" style="width: 18%;"><input style="width: 50%;" name="timma" id="transac" placeholder="Mã GD" onchange="search()"/></td>
                                <td colspan="2" style="width: 25%;">
                                    <input type="text" style="width: 35%;" name="moneyFrom" id="moneyFrom" placeholder="From" min="10000" max="99999999" value="10000" onchange="search()"/>
                                    <input type="text" style="width: 35%;" name="moneyTo" id="moneyTo" min="10000" max="99999999" placeholder="To" value="999999" onchange="search()"/>
                                </td>
                                <td colspan="2" style="width: 10%;">
                                    <input type="datetime-local" style="width: 35%;" name="timeFrom" id="timeFrom" placeholder="Mã GD" onchange="search()"/>
                                    <input type="datetime-local" style="width: 35%;" name="timeTo" id="timeTo" placeholder="Mã GD" onchange="search()"/>
                                </td>

                                <td>
                                    <select id="content" name="payment-type" onchange="search()">
                                        <option value="">All</option>
                                        <option value="nap tien">Nạp tiền</option>
                                        <option value="mua the">Mua thẻ</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="status" name="payment-type" onchange="search()">
                                        <option value="">All</option>
                                        <option value="success">Thành công</option>
                                        <option value="failed">Thất bại</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="type" name="payment-type" onchange="search()">
                                        <option value="">All</option>
                                        <option value="1">+</option>
                                        <option value="0">-</option>
                                    </select>
                                </td>

                                <!--                                <td style=" width: 20%;">
                                                                    <input style="width: 50%;" type="datetime-local" id="datetime" name="datetime"> comment                                    
                                                                </td>
                                                                 <td style="width: 20%;">
                                                                    <input style="width: 50%;" type="datetime-local" id="datetime" name="datetime"> comment                                    
                                                                </td>-->
                            </tr>
                            <tbody id="content1">

                                <c:set var="i" value="${1}"></c:set>
                                <c:forEach items="${list}" var="o">
                                    <c:if test="${o.getStatus().equalsIgnoreCase('success')}">
                                        <tr>
                                            <td>${i}</td>
                                            <c:set var="i" value="${i+1}"></c:set>
                                                <td colspan="2">
                                                    <h6>${o.id}</h6>
                                            </td>
                                            <td colspan="2"><fmt:formatNumber value="${o.total}" type="number" pattern="#,###" /></td>
                                            <td colspan="2">${o.createdAt}<br></td>
                                            <td>${o.content}</td>
                                            <td style="color: green" class="font-weight-bold">Thành công</td>

                                                <c:if test="${o.type == true}"><td>+</td></c:if>
                                                <c:if test="${o.type == false}"><td>-</td></c:if>
                                                </tr>
                                        </c:if>
                                        <c:if test="${o.getStatus().equalsIgnoreCase('success') == false}">
                                            <tr>
                                                <td>${i}</td>
                                                <c:set var="i" value="${i+1}"></c:set>
                                                    <td colspan="2">
                                                        <h6>${o.id}</h6>
                                                </td>
                                                <td colspan="2"><fmt:formatNumber value="${o.total}" type="number" pattern="#,###" /></td>
                                                <td colspan="2">${o.createdAt}<br></td>
                                                <td>${o.content}</td>
                                                <td class="font-weight-bold text-danger">Thất bại</td>
                                                <c:if test="${o.type == true}"><td>+</td></c:if>
                                                <c:if test="${o.type == false}"><td>-</td></c:if>

                                                </tr>
                                        </c:if>
                                    </c:forEach>

                                </tbody>
                            </table>
                        </div>
                        <div class="pagination-bottom" style="display: flex;">
                        <div class="-pagination" style="display: flex;
                             position: fixed;
                             bottom: 0;
                             left: 0;
                             right: 0;
                             background-color: white;
                             z-index: 1;
                             display: -ms-flexbox;
                             display: flex;
                             -ms-flex-pack: justify;
                             justify-content: space-between;
                             -ms-flex-align: stretch;
                             align-items: stretch;
                             -ms-flex-wrap: wrap;
                             flex-wrap: wrap;
                             padding: 3px;
                             -webkit-box-shadow: 0 0 15px 0 rgba(0,0,0,0.1);
                             box-shadow: 0 0 15px 0 rgba(0,0,0,0.1);
                             border-top: 2px solid rgba(0,0,0,0.1);">
                            <div class="-previous" style="display: flex;"><button id="nuttruoc" type="button" disabled="" class="-btn">Trước</button></div>
                            <div class="-center" style="display: flex;"><span style="display: flex;" class="-pageInfo">Trang <div class="-pageJump"><input id="page" type="number" value="1"
                                                                                                                                                           fdprocessedid="2tada8" min="1" max="" onchange="loadPage()"></div> / <span id="showpage" class="-totalPages">1</span></span>
                                <span class="select-wrap -pageSizeOptions">
                                    <select id="selectpage" fdprocessedid="g1rmyr" onchange="loadGhi()">
                                        <option value="5">5 Bản ghi</option>
                                        <option value="10">10 Bản ghi</option>
                                        <option value="20">20 Bản ghi</option>
                                        <option value="25">25 Bản ghi</option>
                                        <option value="50">50 Bản ghi</option>                                        
                                    </select>
                                </span>
                            </div>
                            <div class="-next"><button id="nutsau" type="button" disabled="" class="-btn">Sau</button></div>
                        </div>
                    </div>
                </div>
                <script>
                    // Lấy ngày hiện tại
                    var now = new Date();
// Tính toán ngày trước đó 30 ngày
                    var bientami = new Date();
                    now.setDate(bientami.getDate() + 1);
                    var thirtyDaysAgo = new Date(now.getTime() - 30 * 24 * 60 * 60 * 1000);
// Định dạng ngày cho định dạng chuẩn ISO 8601
                    var fromDateString = thirtyDaysAgo.toISOString().slice(0, 16);
                    var toDateString = now.toISOString().slice(0, 16);
// Đặt giá trị cho các thẻ input
                    document.getElementById('timeFrom').value = fromDateString;
                    document.getElementById('timeTo').value = toDateString;
                    var selectpage = document.getElementById("selectpage");
                    var selected = parseFloat(selectpage.value);
                    var total = parseFloat(document.getElementById("count").value);
                    console.log("tong trang page" + total + ";" + "tong trang chon" + selected);
                    console.log(typeof total);
                    var page = Math.ceil(total / selected);
                    var inputpage = document.getElementById("page");
                    var showpage = document.getElementById("showpage").innerHTML = page;
                    inputpage.setAttribute("max", page);
                    console.log(page);
                    function loadGhi() {
                        var moneyFrom = document.getElementById("moneyFrom");
                        var moneyTo = document.getElementById("moneyTo");
                        var moneyToValue = parseInt(moneyTo.value);
                        var moneyFromValue = parseInt(moneyFrom.value);
                        var transac = document.getElementById("transac");
                        var dateFrom = document.getElementById("timeFrom");
                        var dateTo = document.getElementById("timeTo");
                        var dateFromValue = dateFrom.value;
                        var dateToValue = dateTo.value;
                        var content = document.getElementById("content");
                        var status = document.getElementById("status");
                        var type = document.getElementById("type");
                        //var amount = document.getElementsByClassName("product").length;
                        var totalPage = parseFloat(document.getElementById("count").value);
                        var userid = document.getElementById("user").value;
                        var pageNow = parseFloat(document.getElementById("selectpage").value); // ban ghi
                        var selectedpage = document.getElementById("page").value;
                        var countPage = Math.ceil(totalPage / pageNow);
                        var nutsau = document.getElementById("nutsau");
                        if (countPage > 1) {
                            nutsau.removeAttribute("disabled");
                        }
                        var showpage = document.getElementById("showpage").innerHTML = countPage;
                        document.getElementById("page").value = 1;
                        inputpage.setAttribute("max", countPage);
                        $.ajax({
                            url: "/autothecao/historyTransaction",
                            type: "get", //send it through get method
                            data: {
                                page: 1,
                                ghi: pageNow,
                                totalGhi: totalPage,
                                moneyFrom: moneyFromValue,
                                moneyTo: moneyToValue,
                                timeFrom: dateFromValue,
                                timeTo: dateToValue,
                                transac: transac.value,
                                content: content.value,
                                status: status.value,
                                type: type.value,
                                id: userid
                            },
                            success: function (data) {
                                var row = document.getElementById("content1");
                                row.innerHTML = data;
                            },
                            error: function (xhr) {
                                document.window.alert("hieuuu");
                                //Do Something to handle error
                                alert("loi load ghi");
                            }
                        });
                    }
                    function loadPage() {
                        var moneyFrom = document.getElementById("moneyFrom");
                        var moneyTo = document.getElementById("moneyTo");
                        var moneyToValue = parseInt(moneyTo.value);
                        var moneyFromValue = parseInt(moneyFrom.value);
                        var transac = document.getElementById("transac");
                        var dateFrom = document.getElementById("timeFrom");
                        var dateTo = document.getElementById("timeTo");
                        var dateFromValue = dateFrom.value;
                        var dateToValue = dateTo.value;
                        var content = document.getElementById("content");
                        var status = document.getElementById("status");
                        var type = document.getElementById("type");
                        //var amount = document.getElementsByClassName("product").length;
                        var userid = document.getElementById("user").value;
                        var pageNow = document.getElementById("selectpage").value; // ban ghi
                        var selectedpage = document.getElementById("page").value;
                        var totalPage = parseFloat(document.getElementById("count").value);
                        $.ajax({
                            url: "/autothecao/historyTransaction",
                            type: "get", //send it through get method
                            data: {
                                page: selectedpage,
                                ghi: pageNow,
                                totalGhi: totalPage,
                                moneyFrom: moneyFromValue,
                                moneyTo: moneyToValue,
                                timeFrom: dateFromValue,
                                timeTo: dateToValue,
                                transac: transac.value,
                                content: content.value,
                                status: status.value,
                                type: type.value,
                                id: userid
                            },
                            success: function (data) {
                                var row = document.getElementById("content1");
                                row.innerHTML = data;
                            },
                            error: function (xhr) {
                                document.window.alert("hieuuu");
                                lert("loi load page");
                            }
                        });
                    }

                    function search() {
                        console.log("da qua day");
                        var userid = document.getElementById("user").value;
                        var moneyFrom = document.getElementById("moneyFrom");
                        var moneyTo = document.getElementById("moneyTo");
                        var moneyToValue = parseInt(moneyTo.value);
                        var moneyFromValue = parseInt(moneyFrom.value);
                        console.log(moneyFromValue);
                        console.log(typeof moneyToValue);
                        console.log(moneyToValue < moneyFromValue);
                        var totalPage = parseFloat(document.getElementById("count").value);
                        if (moneyFromValue < 10000) {
                            alert("Xin lỗi, vui lòng nhập số tiền lớn hơn 10.000 ");
                            moneyFrom.value = "10000";
                            return;
                        }
                        if (moneyToValue < moneyFromValue) {
                            alert("Xin lỗi, vui lòng nhập số tiền nhỏ hơn ");
                            moneyFrom.value = "10000";
                            return;
                        }
                        var transac = document.getElementById("transac");
                        var dateFrom = document.getElementById("timeFrom");
                        var dateTo = document.getElementById("timeTo");
                        var dateFromValue = dateFrom.value;
                        var dateToValue = dateTo.value;
                        var tomorrow = new Date(now.getTime() + (24 * 60 * 60 * 1000));
                        tomorrow.setDate(now.getDate() + 1);
                        if (dateToValue > tomorrow.value) {
                            alert("Xin lỗi, vui lòng chọn ngày trước ngày hiện tại");
                            dateTo.value = tomorrow.value;
                            return;
                        }
                        if (dateFromValue > dateToValue) {
                            alert("Xin lỗi, vui lòng nhập ngày bắt đầu nhỏ hơn ngày đến.");
                            var datetam = new Date(dateToValue);
                            var datemoi = new Date(datetam.getTime() - 30 * 24 * 60 * 60 * 1000);
                            var fromDateString1 = datemoi.toISOString().slice(0, 16);
                            dateFrom.value = fromDateString1;
                            return;
                        }
                        console.log(dateFromValue + " from:to " + dateToValue);
                        var content = document.getElementById("content");
                        var status = document.getElementById("status");
                        var type = document.getElementById("type");
                        $.ajax({
                            url: "/autothecao/countHistoryTransaction",
                            type: "get", //send it through get method
                            data: {
                                moneyFrom: moneyFromValue,
                                moneyTo: moneyToValue,
                                timeFrom: dateFromValue,
                                timeTo: dateToValue,
                                transac: transac.value,
                                content: content.value,
                                status: status.value,
                                type: type.value,
                                id: userid
                            },
                            success: function (data) {
                                var numberData = parseInt(data);
                                var changeCount = document.getElementById("count");
                                changeCount.value = numberData;
                                changeCount.setAttribute("value", numberData);
                                if (numberData === 0) {
                                    var row = document.getElementById("content1");

                                }
                                var countPage = Math.ceil(numberData / 5);
                                document.getElementById("page").value = 1;
                                document.getElementById("selectpage").value = 5;
                                if (countPage === 0)
                                    countPage = 1;
                                inputpage.setAttribute("max", countPage);
                                document.getElementById("showpage").innerHTML = countPage;
                            },
                            error: function (xhr) {
                                document.window.alert("hieuuu");
                                //Do Something to handle error
                            }
                        });
                        $.ajax({
                            url: "/autothecao/historyTransaction",
                            type: "get", //send it through get method
                            data: {
                                page: 1,
                                ghi: 5,
                                totalGhi: totalPage,
                                moneyFrom: moneyFromValue,
                                moneyTo: moneyToValue,
                                timeFrom: dateFromValue,
                                timeTo: dateToValue,
                                transac: transac.value,
                                content: content.value,
                                status: status.value,
                                type: type.value,
                                id: userid
                            },
                            success: function (data) {
                                var row = document.getElementById("content1");
                                row.innerHTML = data;
                            },
                            error: function (xhr) {
                                document.window.alert("hieuuu");
                                //Do Something to handle error
                                alert("loi load ghi");
                            }
                        });
                    }
                    ;
                </script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            </body>
        </html>
