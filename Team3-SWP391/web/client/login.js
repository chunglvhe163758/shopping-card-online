function Validator(options) {
    var formElement = document.querySelector(options.form);
    var validate = function (inputElement, rule) {
        var errorElement = inputElement.parentElement.querySelector('.form-message');

        var errorMessage = rule.test(inputElement.value);
        if (errorMessage) {

            errorElement.innerText = errorMessage;
            inputElement.parentElement.classList.add('invalid');
            console.log(inputElement.parentElement.classList);
        } else {
            errorElement.innerText = '';
            inputElement.parentElement.classList.remove('invalid');
        }
    }
    if (formElement) {
        options.rules.forEach(function (rule) {
            var inputElement = formElement.querySelector(rule.selector);
            if (inputElement) {
                inputElement.onblur = function () {
                    validate(inputElement, rule);
                }
                inputElement.oninput = function () {
                    var errorElement = inputElement.parentElement.querySelector('.form-message');
                    errorElement.innerText = '';
                    inputElement.parentElement.classList.remove('invalid');
                }


            }
        })
    }

}

Validator.isRequired = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            var regex = /^[a-zA-Z0-9]+$/;
            if (!regex.test(value)) {
                return 'Vui lòng chỉ nhập chữ và số';
            }

        }
    };
};

Validator.isRequiredd = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            var regex = /^[a-zA-Z0-9]+$/;
            var regex1 = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (!regex.test(value) || regex1.test(value)) {
                return 'Vui lòng nh?p tên dang nh?p ho?c email';
            }

        }
    };
};


Validator.isEmail = function (selector) {
    return{
        selector: selector,
        test: function (value) {
            var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            return regex.test(value) ? undefined : 'Vui lòng nhập email';
        }
    };
}


Validator.ispassword = function (selector) {
    return {
        selector: selector,
        test: function (value) {


        }
    };
};

Validator.isPassword = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            var regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()])[A-Za-z\d!@#$%^&*()]{8,}$/;
            if (!regex.test(value)) {
                return 'Mật khẩu phải có ít nhất 8 ký tự, chữ cái đầu viết hoa và ít nhất 1 ký tự đặc biệt';
            }
            return undefined;
        }
    };
};
//captcha


// Lấy phần tử trên HTML
const form = document.getElementById('form-1');
const form2 = document.getElementById('form-2');
const captchaImage = document.getElementById('captcha-image');
const captchaImage1 = document.getElementById('captcha-image1');
const captchaInput = document.getElementById('captcha-input');
const refreshCaptchaButton = document.getElementById('refresh-captcha');
const refreshCaptchaButton1 = document.getElementById('refresh-captcha1');
// Gán sự kiện click cho nút "Refresh"
refreshCaptchaButton.addEventListener('click', refreshCaptcha);
refreshCaptchaButton.addEventListener('click', console.log("click here"));
refreshCaptchaButton1.addEventListener('click', refreshCaptcha);
// Hàm xử lý khi nhấn nút "Refresh"
function refreshCaptcha(event) {
    event.preventDefault(); // Ngăn chặn hành vi mặc định của nút "Refresh"
    captchaInput.value = ''; // Xóa giá trị captcha cũ

    var s = 'Captcha?' + new Date().getTime(); // Gọi lại servlet Captcha với thời gian mới để cập nhật hình ảnh captcha
    captchaImage.src = s;
    captchaImage1.src = s;
}

// Gán sự kiện submit cho form
form.addEventListener('submit', validateForm);
form2.addEventListener('submit', validateForm);
// Hàm xử lý khi submit form
//    function validateForm(event) {
//        if (captchaInput.value === '') {
//            
//            event.preventDefault(); // Ngăn chặn hành vi mặc định của form khi captcha chưa được nhập
//            // Thực hiện xử lý thông báo lỗi hoặc hành động khác tại đây
//        }
//}
