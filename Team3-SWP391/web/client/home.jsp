
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "dao.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
            />
        <meta http-equiv="content-language" content="vi" />
        <meta name="robots" content="index, follow" />

        <title>Mua thẻ điện thoại, thẻ game</title>
        <meta name="description" content="Mua thẻ điện thoại, thẻ game" />
        <meta name="keywords" content="Mua thẻ điện thoại, thẻ game" />

        <!-- Open Graph data -->
        <meta property="og:title" content="Mua thẻ điện thoại, thẻ game" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="https://cardvip.vn/card" />
        <meta property="og:image" content="" />
        <meta property="og:description" content="Mua thẻ điện thoại, thẻ game" />
        <meta property="og:site_name" content="Mua thẻ điện thoại, thẻ game" />
        <meta property="article:published_time" content="" />
        <meta property="article:modified_time" content="" />
        <meta property="article:section" content="Mua thẻ điện thoại, thẻ game" />
        <meta property="article:tag" content="Mua thẻ điện thoại, thẻ game" />

        <!-- Twitter Card data -->
        <meta name="twitter:card" content="" />
        <meta name="twitter:site" content="@wmt24h" />
        <meta name="twitter:title" content="Mua thẻ điện thoại, thẻ game" />
        <meta name="twitter:description" content="Mua thẻ điện thoại, thẻ game" />
        <meta name="twitter:creator" content="@wmt24h" />
        <meta name="twitter:image:src" content="" />
        <link rel="alternate" hreflang="vi-vn" href="https://cardvip.vn/card" />
        <link rel="canonical" href="https://cardvip.vn/card" />

        <script
            src="https://connect.facebook.net/vi_VN/sdk.js?hash=2d1deb15a4603ce2fd91ae06be403ec5"
            async=""
            crossorigin="anonymous"
        ></script>
        <script
            id="facebook-jssdk"
            src="https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js"
        ></script>
        <script type="application/ld+json">
            {
            "@context": "https://schema.org",
            "@type": "TechArticle",
            "headline": "Mua thẻ điện thoại, thẻ game",
            "dependencies": "Mua thẻ điện thoại, thẻ game",
            "proficiencyLevel": "Expert",
            "alternativeHeadline": "Mua thẻ điện thoại, thẻ game",
            "image": "",
            "author": "",
            "award": "",
            "editor": "",
            "genre": "Mua thẻ điện thoại, thẻ game",
            "keywords": "Mua thẻ điện thoại, thẻ game",
            "wordcount": "1200",
            "publisher": "Admin",
            "url": "https://cardvip.vn",
            "datePublished": "",
            "dateCreated": "",
            "dateModified": "",
            "description": "Mua thẻ điện thoại, thẻ game",
            "articleBody": ""
            }
        </script>
        <meta
            name="csrf-token"
            content="11k5hzS4HEudqoA1P3fadDeB8HxmqQ77qQr4gtNR"
            />
        <link
            rel="shortcut icon"
            href="https://cardvip.vn/storage/userfiles/images/favicon.png"
            />
        <style>
            :root {
                --primary-color: #f2546b;
                --primary-hover: #f0ad4e;
                --primary-rgb: 1, 36, 103;
            }
        </style>

        <link
            rel="icon"
            href="https://cardvip.vn/storage/userfiles/images/favicon.png"
            />
        <link
            rel="apple-touch-icon"
            href="https://cardvip.vn/storage/userfiles/images/favicon.png"
            />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="" />
        <link
            href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;0,900;1,300;1,400;1,500;1,700;1,900&amp;display=swap"
            rel="stylesheet"
            />
        <link
            rel="stylesheet"
            href="https://cardvip.vn/assets/default/plugins/bootstrap/css/bootstrap.min.css"
            />
        <link
            rel="stylesheet"
            href="https://cardvip.vn/assets/default/plugins/bootstrap-flatpickr/bootstrap-flatpickr.min.css"
            />
        <link
            rel="stylesheet"
            href="https://cardvip.vn/assets/default/plugins/select2/css/select2.min.css"
            />
        <link
            rel="stylesheet"
            href="https://cardvip.vn/assets/default/plugins/swiper/swiper-bundle.min.css"
            />
        <link
            rel="stylesheet"
            href="https://cardvip.vn/assets/default/fonts/fontawesome/css/all.min.css"
            />
        <link
            rel="stylesheet"
            href="https://cardvip.vn/assets/default/plugins/fancybox/fancybox.min.css"
            />
        <link
            rel="stylesheet"
            href="https://cardvip.vn/assets/default/css/style.css"
            />

        <style
            type="text/css"
            data-fbcssmodules="css:fb.css.base css:fb.css.dialog css:fb.css.iframewidget css:fb.css.customer_chat_plugin_iframe"
            >
            
            
        </style>
    </head>
    <%
      ProductDAO pd = new ProductDAO();
      List<Supplier> lst = (List<Supplier>) pd.getAllSupplier();
      request.setAttribute("lst",lst);
      String money1 = (String) request.getParameter("money")+"";
      String error = (String) request.getParameter("error")+"";
    %>
       <%Object obj = session.getAttribute("account");
    User u = null;
    if(obj !=null) u = (User)obj;
    %>
    <header id="header-m1" class="header-m1">
        <jsp:include page="header.jsp"></jsp:include>
        
        </header>
        <input hidden="" id="trangthai" value="true" type="text" />
           <%if(!error.trim().equals("null")){%>
        <input hidden="" id="error" value="<%=error%>"/>
         <div id="showmon"  style="font-family: 'Roboto', sans-serif; margin-left: 65%; position: relative; z-index: 99999; margin-top: -4%; position: fixed;"class="">
            <div class="card mt-5 p-3" style="	width: 350px; height: 90px;border: none;border-radius: 20px;background-color: #597AFD;">
                <div class="media">
                    <img src="../img/thongbao1.png" class="mr-3">
                    <div style="" class="media-body">
                        <h6 id="content" style="color: #E5F3FF;" class="mt-2 mb-0">You have received</h6>
                        <small id="datem" style="color: #B2C9FF;" class="text">Mar 20,2020</small>
                    </div>
                </div>
            </div>
        </div>
         <%}%>
        <%if(!money1.trim().equals("null")){%>
        <input hidden="" id="adm" value="<%=money1%>"/>
         <div id="showmon"  style="font-family: 'Roboto', sans-serif; margin-left: 65%; position: relative; z-index: 99999; margin-top: -4%; position: fixed;"class="">
            <div class="card mt-5 p-3" style="	width: 350px; height: 90px;border: none;border-radius: 20px;background-color: #597AFD;">
                <div class="media">
                    <img src="../img/addmoney.png" class="mr-3">
                    <div style="" class="media-body">
                        <h6 id="money" style="color: #E5F3FF;" class="mt-2 mb-0">You have received</h6>
                        <small id="datem" style="color: #B2C9FF;" class="text">Mar 20,2020</small>
                    </div>
                </div>
            </div>
        </div>
         <%}%>
         
        <script>
            try {
     var adm = document.getElementById("adm").value;
     var money = document.getElementById("money");
     money.innerHTML = "Nạp thành công "+adm+" vnđ";
} catch (e) {
    
}
try {
     var error = document.getElementById("error").value;
            var content = document.getElementById("content");
             if(error === 'process'){
                content.innerHTML = "Giao dịch của bạn đã được ghi nhận và đang xử lý.";
            } else if(error === 'failed') {
                content.innerHTML = "Số dư hiện tại không đủ.";
            }else{
                content.innerHTML = "Hệ thống hiện tại đang lỗi vui lòng thử lại sau.";
            }
} catch (e) {
    
}

           
           
            
            var datem = document.getElementById("datem");
            var currentdate = new Date();
           
            var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth() + 1) + "/"
                    + currentdate.getFullYear() + " @ "
                    + currentdate.getHours() + ":"
                    + currentdate.getMinutes() + ":";
            
            datem.innerHTML = currentdate;
            var showmon = document.getElementById("showmon");
            setTimeout(function (){
                showmon.setAttribute("hidden","");
            },2000);
        </script>
       
        <body style="margin-left: 11%;margin-top: 7%; ">
            <div style="position: relative;" id="toast"></div>

            <input hidden=""  id="error"  value="hello"/>


            <div style="" id="main" class="main">
            
                

                <div class="section-gap">
                    <div class="container">
                        <div class="description mb-3">
                            <div class="title">Mua mã thẻ</div>
                            <p>
                                Các loại thẻ cào điện thoại, thẻ game trực tuyến, hỗ trợ thanh
                                toán bằng ví điện tử, các ngân hàng của Việt Nam, thẻ Visa/Master.
                                Sau khi thanh toán thành công, thẻ sẽ đc trả ngay lập tức trên
                                website và gửi vào điạ&nbsp;chỉ email của bạn.
                            </p>
                        </div>
                        <div class="tabs-m1 mt-5">
                            <ul class="nav nav-tabs justify-content-start mb-4">
                                <li class="nav-item">
                                    <a
                                        class="nav-link active"
                                        data-toggle="tab"
                                        id="tab-1"
                                        href="#cate1"
                                        >
                                        Thẻ điện thoại
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" id="tab-2" href="#cate2">
                                        Thẻ game
                                    </a>
                                </li>
                            </ul>
                            <div class="row">
                                <div class="col-xl-8 col-lg-7">
                                    <div class="tab-content mb-5">
                                        <div class="tab tab-pane fade active show" id="cate1">
                                            <div class="row row10">
                                            <c:set var="i" value="0" />
                                            <c:forEach var ="s" items="${lst}">
                                                <div class="col-lg-3 col-4 active">
                                                    <a
                                                        href="#option${s.getId()}"
                                                        data-toggle="tab"
                                                        class="card-product card card-cloneTab"
                                                        >
                                                        <div class="card-image">
                                                            <img
                                                                src="../img/${s.getLogo()}"
                                                                alt=""
                                                                />
                                                            <svg
                                                                width="30"
                                                                height="28"
                                                                viewBox="0 0 30 28"
                                                                fill="none"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                >
                                                            <path
                                                                d="M17.9636 18.8194C17.2561 19.5282 16.3136 19.9183 15.3123 19.9183C14.3111 19.9183 13.3686 19.5282 12.6612 18.8194L7.34863 13.5071C6.64002 12.7994 6.25001 11.8571 6.25001 10.8558C6.25001 9.85442 6.64002 8.91213 7.34863 8.20466C8.0561 7.49582 8.99862 7.10581 9.99997 7.10581C11.0011 7.10581 11.9436 7.49582 12.6511 8.20466L15.3123 10.8645L22.6586 3.51835C20.2586 1.47217 17.1511 0.230957 13.7499 0.230957C6.15617 0.230957 0 6.38713 0 13.9807C0 21.5744 6.15617 27.7306 13.7499 27.7306C21.3435 27.7306 27.4996 21.5744 27.4996 13.9807C27.4996 12.5595 27.2836 11.1895 26.8835 9.89951L17.9636 18.8194Z"
                                                                fill="var(--primary-color)"
                                                                ></path>
                                                            <path
                                                                d="M15.3123 17.4181C14.9923 17.4181 14.6723 17.2956 14.4286 17.0519L9.11627 11.7394C8.62738 11.2507 8.62738 10.4606 9.11627 9.97195C9.60493 9.48307 10.3948 9.48307 10.8837 9.97195L15.3123 14.4005L27.8661 1.84696C28.3547 1.3583 29.1446 1.3583 29.6335 1.84696C30.1221 2.33585 30.1221 3.12571 29.6335 3.6146L16.196 17.0519C15.9524 17.2956 15.6322 17.4181 15.3123 17.4181Z"
                                                                fill="#25597B"
                                                                ></path>
                                                            </svg>
                                                        </div>
                                                    </a>
                                                </div>
                                            </c:forEach>                                           
                                        </div>

                                    </div>
                                </div>
                                <div class="description mb-3">
                                    <div class="sub-title">Chọn mệnh giá thẻ</div>
                                </div>
                                <div class="tab-content">
                                    <c:forEach var ="s" items="${lst}">
                                        <jsp:useBean id="pk" class="dao.ProductDAO"/>
                                        <c:set var="list" value="${pk.getProductBySup(s.getId())}"/>
                                        <div class="tab tab-pane fade" id="option${s.getId()}">
                                            <div class="row row10">
                                                <c:forEach var="pro" items="${list}">                                          
                                                    <div class="col-lg-3 col-4">
                                                        <div class="card-price sc-item-btn" title="${pro.getName()}đ" data-pricee="${pro.getPrice()}" data-title="${pro.getName()}đ" data-id="${pro.getId()}" id="${pro.getKey()}" data-row="">
                                                            <svg width="30" height="28" viewBox="0 0 30 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M17.9636 18.8193C17.2561 19.5281 16.3136 19.9181 15.3123 19.9181C14.3111 19.9181 13.3686 19.5281 12.6612 18.8193L7.34863 13.507C6.64002 12.7993 6.25001 11.857 6.25001 10.8556C6.25001 9.8543 6.64002 8.912 7.34863 8.20454C8.0561 7.4957 8.99862 7.10569 9.99997 7.10569C11.0011 7.10569 11.9436 7.4957 12.6511 8.20454L15.3123 10.8643L22.6586 3.51823C20.2586 1.47205 17.1511 0.230835 13.7499 0.230835C6.15617 0.230835 0 6.38701 0 13.9805C0 21.5743 6.15617 27.7305 13.7499 27.7305C21.3435 27.7305 27.4996 21.5743 27.4996 13.9805C27.4996 12.5594 27.2836 11.1894 26.8835 9.89939L17.9636 18.8193Z"
                                                                fill="white"></path>
                                                            <path
                                                                d="M15.3123 17.4181C14.9923 17.4181 14.6723 17.2956 14.4286 17.0519L9.1163 11.7394C8.62741 11.2507 8.62741 10.4606 9.1163 9.97195C9.60496 9.48307 10.3948 9.48307 10.8837 9.97195L15.3123 14.4005L27.8661 1.84696C28.3548 1.3583 29.1446 1.3583 29.6335 1.84696C30.1222 2.33585 30.1222 3.12571 29.6335 3.6146L16.196 17.0519C15.9525 17.2956 15.6323 17.4181 15.3123 17.4181Z"
                                                                fill="white"></path>
                                                            </svg>
                                                            <div class="text">${pro.getName()}đ</div>
                                                        </div>
                                                    </div>                                                
                                                </c:forEach>
                                            </div>
                                            <div class="text-muted text-center mt-5 d-none d-lg-block">
                                                <p>
                                                    Hướng dẫn cách nạp thẻ ${s.getName()} trả trước bằng thẻ cào<br />
                                                    Bước 1: Bạn có thể tới các cửa hàng đại lí có cung cấp
                                                    thẻ cào ${s.getName()}, sau đó mua thẻ điện thoại giấy gồm có
                                                    số seri, mã thẻ, và lớp tráng bạc. Hoặc nếu muốn nhanh
                                                    chóng hơn bạn cũng có thể nạp thẻ ${s.getName()} trả trước tại
                                                    các website cung cấp dịch vụ hoặc mua thẻ bằng tin nhắn
                                                    SMS.<br />
                                                    Bước 2: Cạo lớp bạc để lấy dãy số sau đó nhấn mã:
                                                    *100*Mã số thẻ cào# và bấm phím Gọi<br />
                                                    Nhà mạng ${s.getName()} còn cung cấp dịch vụ nạp tiền cho thuê
                                                    bao trả trước khác bằng thẻ cào giấy, để nạp tiền
                                                    ${s.getName()} bạn cần nhấn *103*Số điện thoại nhận*Mã số thẻ
                                                    cào# &nbsp;và bấm phím Gọi, phí nạp cho thuê bao khác là
                                                    1000đ/ lần &nbsp;sẽ bị trừ vào tài khoản gốc của thuê
                                                    bap nạp hộ.
                                                </p>
                                            </div>
                                        </div>
                                    </c:forEach>

                                    /////


                                </div>
                            </div>
                            <div id="cart1" class="col-xl-4 col-lg-5 mt-4 mt-lg-0">
                                <div class="overflow-hidden" id="shopping-cart-wrapper">
                                    <div class="card card-cart overflow-hidden">
                                        <div class="card-header py-2 px-3">
                                            <div class="card-title mb-0 d-flex align-items-center justify-content-between">
                                                Giỏ hàng
                                                <span><i class="fas fa-shopping-cart"></i></span>
                                            </div>
                                        </div>
                                        <div class="card-body p-3 bg-white">

                                            <div class="text-danger py-2 h6 text-center mb-0">
                                                Giỏ hàng đang trống
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div hidden="" id="cart2" class="col-xl-4 col-lg-5 mt-4 mt-lg-0">
                                <div class="overflow-hidden" id="shopping-cart-wrapper">
                                    <div class="card card-cart overflow-hidden">
                                        <div class="card-header py-2 px-3">
                                            <div
                                                class="card-title mb-0 d-flex align-items-center justify-content-between"
                                                >
                                                Giỏ hàng
                                                <span><i class="fas fa-shopping-cart"></i>  </span>
                                            </div>
                                        </div>
                                        <div class="card-body p-3 bg-white">
                                            <div class="form-m1">
                                                <form
                                                    action="buy"
                                                    method="post"
                                                    accept-charset="UTF-8"
                                                    
                                                    class="section-cart"
                                                    >
                                                    <input hidden="" id="nop" name="mathe"/>
                                                    <input hidden="" id="gia" name="price"/>
                                                    <input hidden="" name="tenthecao" id="namethe"/>
                                                    <input
                                                        name="_token"
                                                        type="hidden"
                                                        value="11k5hzS4HEudqoA1P3fadDeB8HxmqQ77qQr4gtNR"
                                                        />
                                                    <div class="item">
                                                        <div class="row row5 rowmb3 align-items-center">
                                                            <div class="col-md-6 col-6">
                                                                <div
                                                                    class="cart-name d-flex align-items-center"
                                                                    >
                                                                    <div id="tenthe" class="cart-price text-left">
                                                                        Viettel 10.000đ
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-md-6 col-6">
                                                                <div
                                                                    class="d-flex align-items-center justify-content-end"
                                                                    >
                                                                    <div class="cart-quantity">
                                                                        <input
                                                                            type="text"
                                                                            class="input-quantity quantity shopping-cart-qty"
                                                                            min="1"
                                                                            max="10"
                                                                            name="qty-32"
                                                                            id="quantity"
                                                                            value="1"
                                                                            data-row="fc95b449cc9037e78cb1d65120c6cd4c"
                                                                            data-id=""
                                                                            />
                                                                        <button
                                                                            class="button-quantity button-quantity_up btn-number"
                                                                            type="button"
                                                                            aria-hidden="true"
                                                                            data-type="plus"
                                                                            data-field="qty-32"
                                                                            >
                                                                            <i class="fas fa-arrow-up"></i>
                                                                        </button>
                                                                        <button
                                                                            class="button-quantity button-quantity_down btn-number"

                                                                            data-type="minus"
                                                                            type="button"
                                                                            data-field="qty-32"
                                                                            >
                                                                            <i class="fas fa-arrow-down"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="giathe" class="cart-price ml-4"></div>
                                                                    <a
                                                                        href="javascript:void(0)"
                                                                        class="cart-delete flex-shrink-0 ml-2 delCart cell-delete-item"
                                                                        data-sku="32"
                                                                        data-row="fc95b449cc9037e78cb1d65120c6cd4c"
                                                                        >
                                                                        <i class="fas fa-times"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="total">
                                                        <div class="row row5 rowmb3 align-items-center">
                                                            <div class="col-6">
                                                                <div class="cart-total">Tổng:</div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div id="tonggia" class="card-total-value"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="info">
                                                        <div class="info-item">
                                                            <label
                                                                for=""
                                                                class="col-form-label font-weight-bold"
                                                                >
                                                                Email
                                                            </label>
                                                            <%if(u == null){%>
                                                               <input
                                                                type="email"
                                                                class="form-control"
                                                                name="email"
                                                                placeholder="Email"
                                                                value=""
                                                                required=""
                                                                />
                                                            <%}else{
        %>
                                                            <input
                                                                type="email"
                                                                class="form-control"
                                                                name="email"
                                                                placeholder="Email"
                                                                value="<%=u.getEmail()%>"
                                                                required=""
                                                                /> <%}%>
                                                        </div>
                                                        <div class="info-item">
                                                            <style>
                                                                #mtopup-form ul {
                                                                    padding: 0;
                                                                }
                                                                #mtopup-form li {
                                                                    padding: 5px;
                                                                    list-style: none;
                                                                    display: inline-block;
                                                                }
                                                                #mtopup-form img {
                                                                    width: 100%;
                                                                }

                                                                #mtopup-form li.active {
                                                                    border: 3px solid #42b873;
                                                                    padding: 0px;
                                                                }
                                                                #mtopup-form input[type="radio"] {
                                                                    display: none;
                                                                }
                                                            </style>
                                                            <div
                                                                class="clearfix"
                                                                style="margin-bottom: 15px"
                                                                ></div>
                                                            <div class="text-left">
                                                                <label
                                                                    class="template-1-label text-dark font-weight-bold"
                                                                    >Phương thức thanh toán:</label
                                                                >
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <select
                                                                            name="paygate_code"
                                                                            class="form-control"
                                                                            >
                                                                            <option value="vnpay">
                                                                                Thanh toán qua VNpay
                                                                            </option>
                                                                                                                        <%if(u == null){}else{
        %>
                                                                            <option value="coin">
                                                                                Thanh toán bằng ví của bạn(coins)
                                                                            </option>
                                                                            <%}%>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br />
                                                        </div>
                                                        <div class="info-item text-small">
                                                            <span class="text-muted"
                                                                  >Bằng việc chọn 'Thanh toán', bạn đồng ý
                                                                với</span
                                                            >
                                                            chính sách cung cấp, hủy và hoàn trả dịch vụ
                                                        </div>
                                                        <div class="info-item">
                                                            <button
                                                                type="submit"
                                                                class=" btn-large btn-success w-100"
                                                                style="height: 44px;
                                                                line-height: 32px;
                                                                font-size: 16px;
                                                                padding-left: 14px;
                                                                padding-right: 14px;"
                                                                
                                                                >
                                                                Thanh toán
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer id="footer" class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 my-4">
                        <div class="footer-item">
                            <strong class="text-bold"> CARDVIP.VN </strong>
                            <div class="footer-item_text">
                                Đại lý thẻ điện thoại, thẻ game trực tuyến, nạp tiền điện thoại.
                                Thanh toán tự động, nhanh chóng và uy tín.
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-6 my-4">
                        <div class="footer-item">
                            <div class="footer-item_title">Dịch vụ</div>
                            <div class="footer-item_list">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="/card">Mua mã thẻ</a>
                                    </li>
                                    <li>
                                        <a href="/doithecao">Đổi thẻ cào</a>
                                    </li>
                                    <li>
                                        <a href="/recharge">Nạp topup</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6 my-4">
                        <div class="footer-item">
                            <div class="footer-item_title">Thông tin</div>
                            <div class="footer-item_list">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="/news">Tin tức</a>
                                    </li>
                                    <li>
                                        <a href="/merchant/list">Kết nối API</a>
                                    </li>
                                    <li>
                                        <a href="/account/security">Bảo mật</a>
                                    </li>
                                    <li>
                                        <a href="news">Tin tức</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 my-4">
                        <div class="footer-item">
                            <div class="footer-item_title">Liên hệ</div>
                            <div class="footer-item_info">
                                <p>
                                    <i class="fas fa-phone-alt"></i>
                                    0859060060
                                </p>
                                <p>
                                    <i class="fas fa-envelope"></i>
                                    cskh@cardvip.vn
                                </p>

                                <p
                                    class="mt-3 d-flex align-items-center justify-content-start footer-item_info__social"
                                    >
                                    <a href="https://www.facebook.com/">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="https://www.youtube.com/watch?v=neCmEbI2VWg">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                    <a href="fb.com/admin">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-bottom">
                <div class="container">
                    <div class="row row-5 align-items-center">
                        <div class="col-6">
                            <a href="#" class="footer-bottom_copyright"
                               >© Software by Nencer Jsc.,</a
                            >
                        </div>
                        <div class="col-6">
                            <div
                                class="footer-dropdown d-flex align-items-center justify-content-end"
                                ></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script
            async=""
            src="https://www.googletagmanager.com/gtag/js?id=U321312323"
        ></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag("js", new Date());

            gtag("config", "U321312323");
        </script>
        <!-- Messenger Plugin chat Code -->
        <div id="fb-root" class="fb_reset">
            <div style="position: absolute; top: -10000px; width: 0px; height: 0px">
                <div></div>
            </div>
            <div
                class="fb_iframe_widget fb_invisible_flow"
                fb-iframe-plugin-query="app_id=&amp;attribution=biz_inbox&amp;container_width=1519&amp;current_url=https%3A%2F%2Fcardvip.vn%2Fcard&amp;is_loaded_by_facade=true&amp;local_state=%7B%22v%22%3A1%2C%22path%22%3A2%2C%22chatState%22%3A1%2C%22visibility%22%3A%22hidden%22%2C%22showUpgradePrompt%22%3A%22not_shown%22%2C%22greetingVisibility%22%3A%22hidden%22%2C%22shouldShowLoginPage%22%3Afalse%7D&amp;locale=vi_VN&amp;log_id=0e838323-b570-489d-859c-9047413913ea&amp;page_id=114331340324966&amp;request_time=1685765192382&amp;sdk=joey"
                >
                <span style="vertical-align: bottom; width: 1000px; height: 0px"
                      ><iframe
                        name="f39ca65724e7b58"
                        width="1000px"
                        height="1000px"
                        data-testid="dialog_iframe"
                        title=""
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customerchat.php?app_id=&amp;attribution=biz_inbox&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df2a5c6d0ece852%26domain%3Dcardvip.vn%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fcardvip.vn%252Ff29420c811dfecc%26relation%3Dparent.parent&amp;container_width=1519&amp;current_url=https%3A%2F%2Fcardvip.vn%2Fcard&amp;is_loaded_by_facade=true&amp;local_state=%7B%22v%22%3A1%2C%22path%22%3A2%2C%22chatState%22%3A1%2C%22visibility%22%3A%22hidden%22%2C%22showUpgradePrompt%22%3A%22not_shown%22%2C%22greetingVisibility%22%3A%22hidden%22%2C%22shouldShowLoginPage%22%3Afalse%7D&amp;locale=vi_VN&amp;log_id=0e838323-b570-489d-859c-9047413913ea&amp;page_id=114331340324966&amp;request_time=1685765192382&amp;sdk=joey"
                        style="
                        padding: 0px;
                        position: fixed;
                        z-index: 2147483646;
                        border-radius: 16px;
                        top: auto;
                        background: none;
                        width: 399px;
                        bottom: 84px;
                        max-height: 0px;
                        right: 16px;
                        visibility: visible;
                        height: 373px;
                        "
                        class=""
                        ></iframe
                    ></span>
            </div>
            <div
                class="fb_iframe_widget fb_invisible_flow"
                fb-iframe-plugin-query="app_id=&amp;attribution=biz_inbox&amp;container_width=1519&amp;current_url=https%3A%2F%2Fcardvip.vn%2Fcard&amp;is_loaded_by_facade=true&amp;local_state=%7B%22v%22%3A1%2C%22path%22%3A2%2C%22chatState%22%3A1%2C%22visibility%22%3A%22hidden%22%2C%22showUpgradePrompt%22%3A%22not_shown%22%2C%22greetingVisibility%22%3A%22hidden%22%2C%22shouldShowLoginPage%22%3Afalse%7D&amp;locale=vi_VN&amp;log_id=d1abab41-1421-43f4-a96f-b61577228348&amp;page_id=114331340324966&amp;request_time=1685765192551&amp;sdk=joey"
                >
                <span style="vertical-align: bottom; width: 1000px; height: 0px"
                      ><iframe
                        name="f177d1eac5d8e5c"
                        width="1000px"
                        height="1000px"
                        data-testid="dialog_iframe"
                        title=""
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customerchat.php?app_id=&amp;attribution=biz_inbox&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df3899bc561f7bdc%26domain%3Dcardvip.vn%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fcardvip.vn%252Ff29420c811dfecc%26relation%3Dparent.parent&amp;container_width=1519&amp;current_url=https%3A%2F%2Fcardvip.vn%2Fcard&amp;is_loaded_by_facade=true&amp;local_state=%7B%22v%22%3A1%2C%22path%22%3A2%2C%22chatState%22%3A1%2C%22visibility%22%3A%22hidden%22%2C%22showUpgradePrompt%22%3A%22not_shown%22%2C%22greetingVisibility%22%3A%22hidden%22%2C%22shouldShowLoginPage%22%3Afalse%7D&amp;locale=vi_VN&amp;log_id=d1abab41-1421-43f4-a96f-b61577228348&amp;page_id=114331340324966&amp;request_time=1685765192551&amp;sdk=joey"
                        style="
                        padding: 0px;
                        position: fixed;
                        z-index: 2147483646;
                        border-radius: 16px;
                        top: auto;
                        background: none;
                        width: 399px;
                        bottom: 84px;
                        max-height: 0px;
                        right: 16px;
                        visibility: visible;
                        height: 373px;
                        "
                        class=""
                        ></iframe
                    ></span>
            </div>
            <div
                class="fb_dialog fb_dialog_advanced"
                alignment="right"
                desktop_bottom_spacing="24"
                >
                <div class="fb_dialog_content">
                    <iframe
                        name="blank_f39ca65724e7b58"
                        width="60px"
                        tabindex="-1"
                        data-testid="bubble_iframe"
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customer_chat/bubble"
                        style="
                        background: none;
                        border-radius: 60px;
                        bottom: 24px;
                        box-shadow: rgba(0, 0, 0, 0.15) 0px 4px 12px 0px;
                        display: block;
                        height: 60px;
                        margin: 0px 12px;
                        overflow: visible;
                        padding: 0px;
                        position: fixed;
                        right: 12px;
                        top: auto;
                        width: 60px;
                        z-index: 2147483644;
                        "
                        ></iframe
                    ><iframe
                        name="availabilityStatus_f39ca65724e7b58"
                        tabindex="-1"
                        data-testid="availabilityStatus_iframe"
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customer_chat/bubble"
                        style="
                        border-radius: 50%;
                        bottom: 21.5px;
                        height: 15px;
                        position: fixed;
                        right: 24px;
                        width: 15px;
                        z-index: 2147483646;
                        "
                        ></iframe
                    ><iframe
                        name="unread_f39ca65724e7b58"
                        tabindex="-1"
                        data-testid="unread_iframe"
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customer_chat/bubble"
                        style="
                        background: none;
                        border-radius: 4pt;
                        bottom: 68px;
                        height: 24px;
                        position: fixed;
                        right: 22px;
                        width: 20px;
                        z-index: 2147483645;
                        "
                        ></iframe
                    ><iframe
                        name="greeting_f39ca65724e7b58"
                        tabindex="-1"
                        data-testid="greeting_iframe"
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customer_chat/bubble"
                        style="border: none; max-height: 0px; min-height: 0px"
                        ></iframe>
                </div>
            </div>
            <div
                class="fb_dialog fb_dialog_advanced"
                alignment="right"
                desktop_bottom_spacing="24"
                >
                <div class="fb_dialog_content">
                    <iframe
                        name="blank_f177d1eac5d8e5c"
                        width="60px"
                        tabindex="-1"
                        data-testid="bubble_iframe"
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customer_chat/bubble"
                        style="
                        background: none;
                        border-radius: 60px;
                        bottom: 24px;
                        box-shadow: rgba(0, 0, 0, 0.15) 0px 4px 12px 0px;
                        display: block;
                        height: 60px;
                        margin: 0px 12px;
                        overflow: visible;
                        padding: 0px;
                        position: fixed;
                        right: 12px;
                        top: auto;
                        width: 60px;
                        z-index: 2147483644;
                        "
                        ></iframe
                    ><iframe
                        name="availabilityStatus_f177d1eac5d8e5c"
                        tabindex="-1"
                        data-testid="availabilityStatus_iframe"
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customer_chat/bubble"
                        style="
                        border-radius: 50%;
                        bottom: 21.5px;
                        height: 15px;
                        position: fixed;
                        right: 24px;
                        width: 15px;
                        z-index: 2147483646;
                        "
                        ></iframe
                    ><iframe
                        name="unread_f177d1eac5d8e5c"
                        tabindex="-1"
                        data-testid="unread_iframe"
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customer_chat/bubble"
                        style="
                        background: none;
                        border-radius: 4pt;
                        bottom: 68px;
                        height: 24px;
                        position: fixed;
                        right: 22px;
                        width: 20px;
                        z-index: 2147483645;
                        "
                        ></iframe
                    ><iframe
                        name="greeting_f177d1eac5d8e5c"
                        tabindex="-1"
                        data-testid="greeting_iframe"
                        frameborder="0"
                        allowtransparency="true"
                        allowfullscreen="true"
                        scrolling="no"
                        allow="encrypted-media"
                        src="https://www.facebook.com/v13.0/plugins/customer_chat/bubble"
                        style="border: none; max-height: 0px; min-height: 0px"
                        ></iframe>
                </div>
            </div>
        </div>

        <!-- Your Plugin chat code -->

        <script
            async=""
            defer=""
            crossorigin="anonymous"
            src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&amp;version=v14.0"
            nonce="e2pbvvE8"
        ></script>
        <script>
            var chatbox = document.getElementById("fb-customer-chat");
            chatbox.setAttribute("page_id", "114331340324966");
            chatbox.setAttribute("attribution", "biz_inbox");
        </script>

        <!-- Your SDK code -->
        <script>
            window.fbAsyncInit = function () {
                FB.init({
                    xfbml: true,
                    version: "v13.0",
                });
            };

            (function (d, s, id) {
                var js,
                        fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js";
                fjs.parentNode.insertBefore(js, fjs);
            })(document, "script", "facebook-jssdk");
        </script>

        <script
            type=" text/javascript"
            src="https://cardvip.vn/assets/default/plugins/jquery.min.js"
        ></script>
        <script
            type="text/javascript"
            src="https://cardvip.vn/assets/default/plugins/bootstrap/js/bootstrap.bundle.min.js"
        ></script>
        <script
            type="text/javascript"
            src="https://cardvip.vn/assets/default/plugins/bootstrap-flatpickr/bootstrap-flatpickr.js"
        ></script>
        <script
            type="text/javascript"
            src="https://cardvip.vn/assets/default/plugins/bootstrap-flatpickr/bootstrap-flatpickr-vn.js"
        ></script>
        <script
            type="text/javascript"
            src="https://cardvip.vn/assets/default/plugins/select2/js/select2.min.js"
        ></script>
        <script
            type="text/javascript"
            src="https://cardvip.vn/assets/default/plugins/swiper/swiper-bundle.min.js"
        ></script>
        <script
            type="text/javascript"
            src="https://cardvip.vn/assets/default/plugins/fancybox/fancybox.min.js"
        ></script>
        <script
            type="text/javascript"
            src="https://cardvip.vn/assets/default/js/app.js"
        ></script>
        <script>
            function showErrorToast(error) {
                toast({
                    title: "Thông báo",
                    message: error,
                    type: "error",
                    duration: 5000
                });
            }
            const error = document.getElementById("error").value;
            console.log(error);
            if (!error === "") {
                console.log(error);
                const myTimeout = setTimeout(showErrorToast(error), 1000);
            }
        </script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
            });
            $("body").on("click", ".softcard-btn", function (e) {
                if (!$(this).hasClass("checked")) {
                    $(".softcard-btn").removeClass("checked");
                    $(this).addClass("checked");
                } else {
                }
            });

            $("body").on("click", ".sc-item-btn", function (e) {
                var ele = $(this);
                var itemSku = ele.data("sku");
                var itemId = ele.data("id");
                var tittle = ele.data("title");
                var price = ele.data("pricee");
                let p = +price;
                console.log(tittle);
                var row = "";
                var type = "";
                console.log("itemSku:" + itemSku);
                console.log("itemId:" + itemId);
                if (!ele.hasClass("active")) {
                    $(".sc-item-btn").removeClass("active");
                    ele.addClass("active");
                    type = "add";
                    var cart1 = document.getElementById("cart1");
                    cart1.setAttribute("hidden", "");
                    var cart2 = document.getElementById("cart2");
                    const tenthe = document.getElementById("tenthe");
                    var giathe = document.getElementById("giathe");
                    var tongtien = document.getElementById("tonggia");
                    var inputquan = document.getElementById("quantity").value;
                    var nop = document.getElementById("nop");
                    var gia = document.getElementById("gia");
                    var namethe1 = document.getElementById("namethe");
                    namethe.setAttribute("value",tittle);
                    gia.setAttribute("value", p);
                    nop.setAttribute("value", itemId);
                    giathe.innerHTML = p;
                    tongtien.innerHTML = price * inputquan;
                    console.log(tenthe);
                    tenthe.innerHTML = tittle;
                    cart2.removeAttribute("hidden");
//                    $.ajax({
//                        url: "https://cardvip.vn/ajax/mua-the",
//                        method: "POST",
//                        data: {type: type, id: itemId, row: row},
//                        beforeSend: function () {
//                            // $('.overlay').show();
//                        },
//                        success: function (data) {
//                            data = $.parseJSON(data);
//                            ele.data("row", data.row);
//                            $("#shopping-cart-wrapper").html(data.shopping_cart);
//                            // $('.overlay').fadeOut();
//                        },
//                    });
                }

                // Tiếp tục xử lý logic khác tại đây nếu cần

                // Nếu bạn muốn ngăn chặn sự kiện click tiếp tục lan ra các phần tử khác
                e.stopPropagation();
            });


            $("body").on("change", ".shopping-cart-qty", function (e) {
                if (checkInputQty($(this))) {
                    rowId = $(this).data("row");
                    qty = $(this).val();
                    itemId = $(this).data("id");
                    console.log(qty);
                    console.log(itemId);
                    console.log($(this));
                    var price = document.getElementById("gia").value;
                    console.log(price);
                    var p = price * qty;
                    let t = +p;

                    var tongtien = document.getElementById("tonggia");
                    tongtien.innerHTML = p;
                    var inputquan = document.getElementById("quantity");

                    inputquan.setAttribute("value", qty);
//                    $.ajax({
//                        url: "https://cardvip.vn/ajax/mua-the",
//                        method: "POST",
//                        data: {type: "update", row: rowId, qty: qty},
//                        beforeSend: function () {
//                            // $('.overlay').show();
//                        },
//                        success: function (data) {
//                            data = $.parseJSON(data);
//                            $("#shopping-cart-wrapper").html(data.shopping_cart);
//                            // $('.overlay').fadeOut();
//                        },
//                    });
                }
            });

            $("body").on("click", ".delete-cart", function (e) {
                $.ajax({
                    url: "https://cardvip.vn/ajax/mua-the",
                    method: "POST",
                    data: {type: "delete"},
                    beforeSend: function () {
                        // $('.overlay').show();
                    },
                    success: function (data) {
                        data = $.parseJSON(data);
                        $(".sc-item-btn").removeClass("active");
                        $("#shopping-cart-wrapper").html(data.shopping_cart);
                        // $('.overlay').fadeOut();
                    },
                });
            });

            $("body").on("click", ".cell-delete-item", function (e) {
                var cart1 = document.getElementById("cart1");
                cart1.removeAttribute("hidden");
                var cart2 = document.getElementById("cart2");
                cart2.setAttribute("hidden", "");
                sku = $(this).data("sku");
                row = $(this).data("row");
                console.log(sku);
                console.log(row);
                var inputquan = document.getElementById("quantity");
                $(".sc-item-btn").removeClass("active");
//                $.ajax({
//                    url: "https://cardvip.vn/ajax/mua-the",
//                    method: "POST",
//                    data: {type: "remove", row: row},
//                    beforeSend: function () {
//                        // $('.overlay').show();
//                    },
//                    success: function (data) {
//                        data = $.parseJSON(data);
//                        $("#item-" + sku).removeClass("active");
//                        $("#shopping-cart-wrapper").html(data.shopping_cart);
//                        // $('.overlay').fadeOut();
//                    },
//                });
            });

            $("body").on("click", ".btn-number", function (e) {
                e.preventDefault();

                fieldName = $(this).attr("data-field");
                type = $(this).attr("data-type");
                var input = $("input[name='" + fieldName + "']");
                var currentVal = parseInt(input.val());
                if (!isNaN(currentVal)) {
                    if (type == "minus") {
                        if (currentVal > input.attr("min")) {
                            input.val(currentVal - 1).change();
                        }
                        if (parseInt(input.val()) == input.attr("min")) {
                            $(this).addClass("disabled");
                        }
                    } else if (type == "plus") {
                        if (currentVal < input.attr("max")) {
                            input.val(currentVal + 1).change();
                        }
                        if (parseInt(input.val()) == input.attr("max")) {
                            $(this).addClass("disabled");
                        }
                    }
                } else {
                    input.val(0);
                }
            });
            $("body").on("focusin", ".input-number", function () {
                $(this).data("oldValue", $(this).val());
                var inputquan = document.getElementById("quantity");

                inputquan.setAttribute("value", $(this).val());
            });
            $("body").on("keydown", ".input-number", function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if (
                        $.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                        // Allow: Ctrl+A
                                (e.keyCode == 65 && e.ctrlKey === true) ||
                                // Allow: home, end, left, right
                                        (e.keyCode >= 35 && e.keyCode <= 39)
                                        ) {
                            // let it happen, don't do anything
                            return;
                        }
                        // Ensure that it is a number and stop the keypress
                        if (
                                (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) &&
                                (e.keyCode < 96 || e.keyCode > 105)
                                ) {
                            e.preventDefault();
                        }
                    });

            function checkInputQty(ele) {
                // $('body').on('change','.input-number',function() {
                // ele = $(this);
                minValue = parseInt(ele.attr("min"));
                maxValue = parseInt(ele.attr("max"));
                valueCurrent = parseInt(ele.val());
                var inputquan5 = document.getElementById("quantity");
                namea = ele.attr("name");
                if (valueCurrent >= minValue) {
                    $(
                            ".btn-number[data-type='minus'][data-field='" + namea + "']"
                            ).removeClass("disabled");
                } else {
                    alert("Vui lòng nhập số lượng thẻ từ 1 đến 10");
                    ele.val(ele.data("oldValue"));
                    
                    inputquan5.value = 1;
                    inputquan5.setAttribute("value",1);
                    return false;
                }
                if (valueCurrent <= maxValue) {
                    $(
                            ".btn-number[data-type='plus'][data-field='" + namea + "']"
                            ).removeClass("disabled");
                } else {
                    alert("Số lượng thẻ mua đã vượt quá giới hạn mua trong 1 lần (10 chiếc) hoặc số lượng thẻ trong kho hiện không đủ.");
                    ele.val(ele.data("oldValue"));
                    inputquan5.value = 1;
                    inputquan5.setAttribute("value",1);
                    return false;
                }
                return true;
                // });
            }
        </script>
        <script>
            const handleRowspan = function () {
                let prevCell;
                let prevCellVal = "";
                let prevCellButton = "";
                let rowSpan = 1;

                // Gắn thêm id table-rowspan vào table
                $("#table-rowspan tbody tr").each(function () {
                    let row = $(this);
                    let firstCell = row.find("td.dong"); // Lấy ra td có class dong;
                    let firstCellButton = row.find("td.dong-button"); // Lấy ra td có class dong-button (phụ thuộc với class dòng);
                    let firstCellVal = firstCell.text();

                    if (firstCellVal === prevCellVal) {
                        rowSpan++;
                        if (prevCell) {
                            prevCell.attr("rowspan", rowSpan); // Gán số lượng rowspan vào td tạm class dong;
                            firstCell.remove(); // Xóa bỏ html của td class dong được khai báo ban đầu;

                            prevCellButton.attr("rowspan", rowSpan); // Gán số lượng rowspan vào td tạm class dong-button;
                            firstCellButton.remove(); // Xóa bỏ html của td class dong-button được khai báo ban đầu;
                        }
                    } else {
                        prevCell = firstCell; // Gán td dong được khai báo mặc định bên html;
                        prevCellVal = firstCellVal; // Gán value của td dong vào biến tạm, dùng để so sánh giá trị;
                        prevCellButton = firstCellButton; // Gán td dong-button được khai báo mặc định bên html;
                        rowSpan = 1;
                    }
                });
            };

            $(function () {
                handleRowspan();
            });
        </script>

    </body>
</html>
