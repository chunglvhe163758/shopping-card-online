<%-- 
    Document   : error
    Created on : May 30, 2023, 2:51:51 AM
    Author     : hieub
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body {
    background: #2E95EF;
    background-image: -moz-radial-gradient(center 45deg,circle cover, #9BD1FF, #2E95EF);
    background-image: -webkit-gradient(radial, 50% 50%, 0, 50% 50%,800, from(#9BD1FF), to(#2E95EF));
    padding-top: 10%;
    padding-left: 42%;
}
.img-error{
width:30%;
height: 30%;
}
        </style>
    </head>
    <body>
      <div class="container bootstrap snippets bootdey">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right" style="margin-top:10px;">
                <div class="col-md-10 col-md-offset-1 pull-right">
                    <img class="img-error" src="https://bootdey.com/img/Content/fdfadfadsfadoh.png">
                    <h1>404 Not Found</h1>
                    <p>Xin lỗi đường dẫn này không tồn tại. Vui lòng  trở về trang chủ.</p>
                    <div class="error-actions">
                        <a href="/autothecao/client/login.jsp" class="btn btn-primary btn-lg">
                            <span class="glyphicon glyphicon-arrow-left"></span>
                            Trở về
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    </body>
</html>
