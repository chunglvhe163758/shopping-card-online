<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="ud" class="dao.UserDAO"/>
<%-- 
    Document   : order
    Created on : Jun 21, 2023, 8:25:54 PM
    Author     : Admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/order.css">

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manager Order</title>
    </head>
    <body>
        <div class="container-xl" style="display: flex">
            <%@include file="sidebar.jsp"%>
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4">
                                <h2>Order <b>Manager</b></h2>
                            </div>
                            <div class="col-sm-8">						
                                <a href="managerorder" class="btn btn-primary"><i class="material-icons"></i> <span>Refresh List</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="table-filter">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="show-entries">
                                    <span>Show</span>
                                    <select class="form-control">
                                        <option>5</option>
                                        <option>10</option>
                                        <option>15</option>
                                        <option>20</option>
                                    </select>
                                    <span>entries</span>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <form action="managerorder" method="POST">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>                   
                                    <div class="filter-group">
                                        <label>Name</label>
                                        <input type="text" oninput="searchByName(this)" value="${txtS}" name="txt" class="form-control" placeholder="Search Customer...">
                                    </div>   

                                    <div class="filter-group">
                                        <label>Supplier</label>
                                        <select class="form-control" name="SupId" style="width: 60%" onchange="this.form.submit()">
                                            <option value="">All</option>
                                            <c:forEach var="s" items="${suppliers}">
                                                <option ${SupId == s.name ?"selected":""} value="${s.name}">${s.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="filter-group">
                                        <label>Type</label>
                                        <select class="form-control" name="ProId" onchange="this.form.submit()">
                                            <option value="">All</option>
                                            <c:forEach var="p" items="${products}">
                                                <option ${ProId == p.name ?"selected":""} value="${p.name}">${p.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </form>

                                <span class="filter-icon"><i class="fa fa-filter"></i></span>
                            </div>
                        </div>
                    </div>

                    <c:if test="${order.isEmpty()}">
                        <h1 class="text-center text-warning my-5">
                            Not Found....
                        </h1>
                    </c:if>

                    <c:if test="${!order.isEmpty()}">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Customer</th>
                                    <th>Product</th>
                                    <th>Order Date</th>						
                                    <th>Status</th>						
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="o" items="${order}">     
                                    <c:set var="user" value="${ud.getUserByID(o.user)}"/>
                                    <tr>
                                        <td>${o.id}</td>
                                        <td><a href="#">${user.name}</a></td>
                                        <td>${o.name}</td>
                                        <td>${o.createdAt}</td>                        
                                        <td>
                                            <c:if test="${o.status == 'fail'}"><span class="status text-danger">•</span> Failed</c:if>
                                            <c:if test="${o.status == 'success'}"><span class="status text-success">•</span> Success</c:if>
                                            </td>
                                            <td>${o.total}</td>
                                        <td><a href="orderdetail?oid=${o.id}" class="view" title="View Details" data-toggle="tooltip"><i class="material-icons"></i></a></td>
                                    </tr> 
                                </c:forEach>                            
                            </tbody>
                        </table>
                        <div class="hint-text">Showing <b>8</b> out of <b>${ocount}</b> entries</div>
                        <nav aria-label="...">
                            <ul class="pagination">
                                <c:if test="${tag > 1}">
                                    <li class="page-item">
                                        <a class="page-link" href="managerorder?index=${tag-1}${ProId != null && ProId != '' ? '&ProId=' : ''}${ProId != null && ProId != '' ? ProId : ''}${SupId != null && SupId != '' ? '&SupId=' : ''}${SupId != null && SupId != '' ? SupId : ''}${txtS != null && txtS != '' ? '&txt=' : ''}${txtS != null && txtS != '' ? txtS : ''}">Previous</a>
                                    </li>
                                </c:if>
                                <c:forEach begin="1" end="${endP}" var="i">
                                    <li class="page-item ${tag==i?"active":""}">
                                        <a class="page-link" href="managerorder?index=${i}${ProId != null && ProId != '' ? '&ProId=' : ''}${ProId != null && ProId != '' ? ProId : ''}${SupId != null && SupId != '' ? '&SupId=' : ''}${SupId != null && SupId != '' ? SupId : ''}${txtS != null && txtS != '' ? '&txt=' : ''}${txtS != null && txtS != '' ? txtS : ''}">${i}</a>
                                    </li>
                                </c:forEach>
                                <c:if test="${tag < endP}">
                                    <li class="page-item">
                                        <a class="page-link" href="managerorder?index=${tag+1}${ProId != null && ProId != '' ? '&ProId=' : ''}${ProId != null && ProId != '' ? ProId : ''}${SupId != null && SupId != '' ? '&SupId=' : ''}${SupId != null && SupId != '' ? SupId : ''}${txtS != null && txtS != '' ? '&txt=' : ''}${txtS != null && txtS != '' ? txtS : ''}">Next</a>
                                    </li>
                                </c:if>
                            </ul>
                        </nav>
                    </c:if>
                </div>
            </div> 
        </div> 
    </body>
</html>
