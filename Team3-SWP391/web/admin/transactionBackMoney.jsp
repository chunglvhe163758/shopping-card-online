<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : order
    Created on : Jun 21, 2023, 8:25:54 PM
    Author     : Admin
--%>
<jsp:useBean id="pk" class="dao.TransactionDAO"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/order.css">

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manager Order</title>
    </head>
    <body>
           <script>
            function updateStatus(button){
                var id = button.value;
                console.log(id);
            $.ajax({
            url: "/autothecao/UpdateForTransaction",
                    type: "POST", //send it through get method
                    data: {
                    id: button.value
                    },
                    success: function (data) {
                         var list = document.getElementById("listTransaction");
                         list.innerHTML = data;
                    
                    }

            ,
                    error: function (xhr) {
                    document.window.alert("hieuuu");
                    //Do Something to handle error
                    }
            });
        }
        </script>
        <div class="container-xl" style="display: flex">
            <%@include file="sidebar.jsp"%>
            <div class="table-responsive">
                <div class="table-wrapper">


                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Mã GD</th>                              
                                <th>Tên GD</th>
                                <th>Ngày tạo</th>	
                                <th>Số tiền</th> 
                                <th>Trạng thái</th>						
                                <th>Xử lý</th>
                            </tr>
                        </thead>
                        <tbody id="listTransaction">
                        <c:set var="lst" value="${pk.getListTransNo()}"></c:set>
                        <c:if test="${lst.size() == 0}">
                              
                            <tr><td>Không còn giao dịch nào cần được xử lý</td></tr>                           
                        
                        </c:if>
                        <c:if test="${lst.size() != 0}">
                              
                            <c:forEach var="l" items="${lst}">

                                <tr>
                                    <td>${l.id}</td>
                            
                            <td>${l.name}</td>
                            <td>${l.createdAt}</td>                        
                            <td>
                                ${l.total}
                            </td>
                            <td>Chờ xử lý</td>
                            <td><button value="${l.id}" onclick="updateStatus(this)">Đã xử lý</button></td>
                            </tr> 
                        </c:forEach>                            
                        
                        </c:if>
                        </tbody>
                    </table>


                </div>
            </div> 
        </div> 
     
    </body>
</html>
