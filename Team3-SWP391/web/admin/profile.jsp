<%-- 
    Document   : profileadmin
    Created on : Jun 20, 2023, 9:59:33 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link
            rel="stylesheet"

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change Profile With Admin </title>
        <style>
            body {
                font-family: Arial, sans-serif;
                font-size: 14px;
            }
            .container {
                max-width: 800px;
                margin: 0 auto;
                padding: 20px;
            }
            .form-group.invalid .form-message {
                display: block;
                margin-left: 10px;
                color: #f33a58;
            }
            .card {
                margin-bottom: 20px;
                border-radius: 4px;
                box-shadow: 0 0 4px rgba(0, 0, 0, 0.1);
            }

            .form-group {
                display: flex;
                align-items: center;
                margin-bottom: 10px;
            }

            .form-group label {
                flex-basis: 30%;
                margin-right: 10px;
                font-weight: bold;
            }

            .form-group input,
            .form-group textarea {
                flex-basis: 70%;
                padding: 10px;
                border-radius: 4px;
                border: 1px solid #ccc;
            }

            .btn {
                cursor: pointer;
                border-radius: 4px;
                padding: 10px 20px;
                border: none;
            }

            .btn-success {
                background-color: #28a745;
                color: #fff;
            }
            #toast {
                position: fixed;
                top: 32px;
                right: 32px;
                z-index: 999999;
            }

            .toast {
                display: flex;
                align-items: center;
                background-color: #fff;
                border-radius: 2px;
                padding: 20px 0;
                min-width: 400px;
                max-width: 450px;
                border-left: 4px solid;
                box-shadow: 0 5px 8px rgba(0, 0, 0, 0.08);
                transition: all linear 0.3s;
            }

            @keyframes slideInLeft {
                from {
                    opacity: 0;
                    transform: translateX(calc(100% + 32px));
                }
                to {
                    opacity: 1;
                    transform: translateX(0);
                }
            }

            @keyframes fadeOut {
                to {
                    opacity: 0;
                }
            }

            .toast--success {
                border-color: #47d864;
            }

            .toast--success .toast__icon {
                color: #47d864;
            }

            .toast--info {
                border-color: #2f86eb;
            }

            .toast--info .toast__icon {
                color: #2f86eb;
            }

            .toast--warning {
                border-color: #ffc021;
            }

            .toast--warning .toast__icon {
                color: #ffc021;
            }

            .toast--error {
                border-color: #ff623d;
            }

            .toast--error .toast__icon {
                color: #ff623d;
            }

            .toast + .toast {
                margin-top: 24px;
            }

            .toast__icon {
                font-size: 24px;
            }

            .toast__icon,
            .toast__close {
                padding: 0 16px;
            }

            .toast__body {
                flex-grow: 1;
            }

            .toast__title {
                font-size: 16px;
                font-weight: 600;
                color: #333;
            }

            .toast__msg {
                font-size: 14px;
                color: #888;
                margin-top: 6px;
                line-height: 1.5;
            }

            .toast__close {
                font-size: 20px;
                color: rgba(0, 0, 0, 0.3);
                cursor: pointer;
            }

        </style>
    </head>
    <body>
        <div id="toast"></div>
        <% 
String erorr = (String) request.getAttribute("erorr")+"";
if(!erorr.trim().equals("null")){
        %>
        <input hidden="" id="error"  value="<%=erorr%>"/>
        <%}%>
    <c:if test ="${sessionScope.account!=null}">
        <div class="container-x1" style="display: flex">
            <%@include file="sidebar.jsp"%>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">

                        <form id="form-1" action="changeprofile" method="post">
                            <div class="card-body" style="width: 1000px">
                                <h2>Account Information</h2>
                                <input hidden="" name="action" value="infor"/>
                                <div class="form-group">
                                    <label for="account">ID</label>
                                    <input
                                        disabled
                                        placeholder="Input number"
                                        type="text"
                                        id="account"
                                        value="${sessionScope.account.id}"

                                        />
                                </div>
                                <div class="form-group">
                                    <label for="fullName">Name</label>
                                    <input
                                        placeholder=""
                                        type="text"
                                        id="fullName"
                                        value="${sessionScope.account.fullName}"
                                        name="fullname"
                                        />
                                </div>
                                <div class="form-group">
                                    <label for="phoneNumber">Phone Number</label>
                                    <input
                                        id="phoneNumber"
                                        name="phone"
                                        type="text"
                                        placeholder="Phone number"
                                        required=""
                                        class="form-control"
                                        value="${sessionScope.account.phone}"
                                        />
                                    <span class="form-message"></span>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email (*)</label>
                                    <input
                                        disabled
                                        placeholder=""
                                        type="text"
                                        id="email"
                                        name="email"
                                        value="${sessionScope.account.email}"
                                        />
                                </div>

                                <div style="display: flex; justify-content: center">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-check"></i> Update
                                    </button>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>

                <form id="form-2" action="changeprofile" method="post">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body" style="width: 1000px">
                                <h2>Change Password</h2>
                                <input hidden="" name="action" value="pass"/>
                                <div class="form-group">
                                    <label for="account">Account*</label>
                                    <input
                                        disabled
                                        placeholder="Account"
                                        required
                                        type="text"
                                        id="account"
                                        value="${sessionScope.account.name}"
                                        />
                                </div>
                                <div class="form-group">
                                    <label for="oldPassword">Old password*</label>
                                    <input
                                        id="oldpassword"
                                        name="oldpass"
                                        type="password"
                                        placeholder="Password"
                                        required=""
                                        class="form-control"
                                        />
                                    <span class="form-message"></span>
                                </div>
                                <div class="form-group">
                                    <label for="newPassword">New password*</label>
                                    <input
                                        id="password"
                                        name="password"
                                        type="password"
                                        placeholder="Conform Password"
                                        required=""
                                        class="form-control"
                                        />
                                    <span class="form-message"></span>
                                </div>
                                <div style="display: flex; justify-content: center">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-check"></i> Change password
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </c:if>
</body>
<script src="message.js"></script>
<script>
    function Validator(options) {
        var formElement = document.querySelector(options.form);
        var validate = function (inputElement, rule) {
            var errorElement =
                    inputElement.parentElement.querySelector(".form-message");

            var errorMessage = rule.test(inputElement.value);
            if (errorMessage) {
                errorElement.innerText = errorMessage;
                inputElement.parentElement.classList.add("invalid");
                console.log(inputElement.parentElement.classList);
            } else {
                errorElement.innerText = "";
                inputElement.parentElement.classList.remove("invalid");
            }
        };
        if (formElement) {
            options.rules.forEach(function (rule) {
                var inputElement = formElement.querySelector(rule.selector);
                if (inputElement) {
                    inputElement.onblur = function () {
                        validate(inputElement, rule);
                    };
                    inputElement.oninput = function () {
                        var errorElement =
                                inputElement.parentElement.querySelector(".form-message");
                        errorElement.innerText = "";
                        inputElement.parentElement.classList.remove("invalid");
                    };
                }
            });
        }
    }

    Validator.isRequired = function (selector) {
        return {
            selector: selector,
            test: function (value) {
                var regex = /^(\+84|0)(9|8)\d{8}$/;
                if (!regex.test(value)) {
                    return "Vui lòng nhập số điện thoại";
                }
            }
        };
    };

    Validator.isPassword = function (selector) {
        return {
            selector: selector,
            test: function (value) {
                var regex =
                        /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()])[A-Za-z\d!@#$%^&*()]{8,}$/;
                if (!regex.test(value)) {
                    return "Mật khẩu phải có ít nhất 8 ký tự, chữ cái đầu viết hoa và ít nhất 1 ký tự đặc biệt";
                }
                return undefined;
            }
        };
    };
    Validator({
        form: "#form-1",
        errrorSelector: ".form-message",
        rules: [Validator.isRequired("#phoneNumber")]
    });
    Validator({
        form: "#form-2",
        errrorSelector: ".form-message",
        rules: [
            Validator.isPassword("#password"),
            Validator.isPassword("#oldpassword")
        ]
    });
    function showErrorToast(error) {
        toast({
            title: "Thông báo",
            message: error,
            type: "error",
            duration: 5000
        });
    }
    const error = document.getElementById("error").value;
    console.log(error);
    if (!error == "") {
        console.log(error);
        const myTimeout = setTimeout(showErrorToast(error), 1000)
    }
</script>
</html>
