<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : user
    Created on : Jun 21, 2023, 8:35:25 PM
    Author     : Admin
--%>
<jsp:useBean id="ud" class="dao.ProductDAO"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/storage.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <title>Manager Stock</title>
        <style>
            a.active{
                font-weight: bold;
                color: white;
            }
        </style>
    </head>
    <body>
        <div class="container-xl" style="display: flex"> 
            <%@include file="sidebar.jsp"%>
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-5">
                                <h2>Stock <b>Management</b></h2>
                            </div>
                            <div class="col-sm-7">
                                <a href="#addEmployeeModal" class="btn btn-secondary" data-toggle="modal"><i class="material-icons"></i><span>Add New Stock</span></a>
                                <form method="post" action="${pageContext.request.contextPath}/uploadexcel" enctype="multipart/form-data">
                                    <a href="#uploadexcel" class="btn btn-secondary" data-toggle="modal"><i class="material-icons"></i> <span>Export to Excel</span></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>CODE NUMBER</th>						
                                <th>SERI NUMBER</th>						
                                <th>DATE CREATED</th>
                                <th>STATUS</th>
                                <th>EXPIRY</th>
                                <th>TYPE</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="storage" items="${storage}">
                                <c:set var="product" value="${ud.getProductById(storage.product)}"/>
                                <tr>
                                    <td>${storage.id}</td>
                                    <td>${storage.code}</td>
                                    <td>${storage.seri}</td>                        
                                    <td>${storage.createdAt}</td>
                                    <td>
                                        <c:if test="${storage.status == 'false'}"><span class="status text-danger">•</span> Out Of Stock</c:if>
                                        <c:if test="${storage.status == 'true'}"><span class="status text-success">•</span> Available</c:if>
                                        </td>
                                        <td>${storage.exp}</td>
                                    <td>${product.name}</td>
                                    <td>
                                        <a href="#editEmployeeModal" class="edit" data-toggle="modal"><i data-toggle="tooltip" title="Edit" class="material-icons"></i></a>
                                        <a href="#deleteEmployeeModal" class="delete" data-toggle="modal"><i onclick="setId(${storage.id})" title="Delete" class="material-icons"></i></a>
                                    </td>
                                </tr>       
                            </c:forEach>            
                        </tbody>
                    </table>
                    <div class="hint-text">Showing <b>50</b> out of <b>${scount}</b> entries</div>
                    <nav aria-label="...">
                        <ul class="pagination">
                            <c:if test="${tag > 1}">
                                <li class="page-item">
                                    <a class="page-link" href="managerstorage?index=${tag-1}">Previous</a>
                                </li>
                            </c:if>
                            <c:forEach begin="1" end="${endP}" var="i">
                                <li class="page-item ${tag==i?"active":""}">
                                    <a class="page-link" href="managerstorage?index=${i}">${i}</a>
                                </li>
                            </c:forEach>
                            <c:if test="${tag < endP}">
                                <li class="page-item">
                                    <a class="page-link" href="managerstorage?index=${tag+1}">Next</a>
                                </li>
                            </c:if>
                        </ul>
                    </nav>
                </div>
            </div>  
        </div> 
        <div id="addEmployeeModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="addstock" method="post">
                        <div class="modal-header">						
                            <h5 class="modal-title">Add Stock</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">					
                            <div class="form-group">
                                <label>ID</label>
                                <input name="id" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>CODE</label>
                                <input name="code" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>SERI</label>
                                <input name="seri" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>DATE CREATED</label>
                                <input name="date" type="date" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>STATUS</label>
                                <input type="text" name="status" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>EXPIRY</label>
                                <input name="expiry" type="date" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Products</label>
                                <select name="product" class="form-select" aria-label="Default select example">
                                    <c:forEach var="p" items="${list}">
                                        <option value="${p.id}">${p.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-danger" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-success" value="Add">
                        </div>
                    </form>
                </div>
            </div>
        </div>        

        <div id="deleteEmployeeModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="stock" method="post">
                        <input hidden="" name="action" value="delete"/>
                        <input hidden="" name="id" id="id"/>
                        <div class="modal-header">						
                            <h5 class="modal-title">Delete Product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">					
                            <p>Are you sure you want to delete?</p>
                            <p class="text-warning"><small>This action cannot be undone</small></p>
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id = "uploadexcel" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="${pageContext.request.contextPath}/admin/uploadexcel" enctype="multipart/form-data">
                        Select file to upload:
                        <br />
                        <input type="file" name="file" />
                        <br />
                        Name:
                        <br />
                        <input type="text" name="name" size="62.5" />
                        <br />
                        <br />
                        <input type="submit" value="Upload" />
                    </form>
                </div>
            </div>
        </div>
        <script>
            const setId = (id) => {
                $('#id').val(id);
            };
        </script>
    </body>

</html>
