/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.websocket.Session;
import java.io.IOException;
import model.User;
import org.apache.catalina.manager.util.SessionUtils;


/**
 *
 * @author hieub
 */
public class AuthorizationFilter implements Filter{
    
    private ServletContext  context;
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.context = filterConfig.getServletContext();
        
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) sr;
        HttpServletResponse response = (HttpServletResponse) sr1;
        String url = request.getRequestURI();
        if(url.startsWith(request.getContextPath()+"/admin")){
            HttpSession ss = request.getSession();
            User x = null;
            try {
                x = (User) ss.getAttribute("account");
            } catch (Exception e) {
            }
          
          if(x!= null){
              if(x.getRole().equals("admin")){
                  fc.doFilter(sr, sr1);
              }else if(x.getRole().equals("customer")){
                  response.sendRedirect("/login");
              }else{
                   response.sendRedirect(request.getContextPath()+"/client/login");  
              }
          }
        }else{
            response.sendRedirect(request.getContextPath()+"/client/login");
        }
    }

    @Override
    public void destroy() {
        Filter.super.destroy(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
}
