/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.User;

/**
 *
 * @author hieub
 */
public class Cookiee extends HttpServlet {

    public Cookiee() {
    }

    private User user;

    public Cookiee(User user) {
        this.user = user;
    }

    public void addCookie(HttpServletRequest request, HttpServletResponse response, String user, String pass) {
        jakarta.servlet.http.Cookie arr[] = request.getCookies();
        String txt = "";
        if (arr != null) {
            for (jakarta.servlet.http.Cookie o : arr) {
                if (o.getName().equals("user")) {

                    o.setMaxAge(0);
                    response.addCookie(o);
                }
            }
        }
        if (txt.isEmpty()) {
            txt = user + ":" + pass;
        } else {
            txt = txt + "/" + user + ":" + pass;
        }
        jakarta.servlet.http.Cookie c = new jakarta.servlet.http.Cookie("user", txt);
        c.setMaxAge(60 * 60 * 24);
        response.addCookie(c);
    }

    public User getCookie(HttpServletRequest request, HttpServletResponse response) {
        User x = null;
        jakarta.servlet.http.Cookie arr[] = request.getCookies();
          String txt = "";
        if (arr != null) {
            for (jakarta.servlet.http.Cookie o : arr) {
                if (o.getName().equals("user")) {
                    txt=txt+o.getValue();
                }
            }
        }
        try {
            if (txt != null && txt.length() != 0) {

                String[] n = txt.split(":", 0);
                String name = n[0];
                String pass = n[1];
                x.setName(name);
                x.setPassword(pass);
            }
        } catch (Exception e) {
        }
        return x;
    }

}
