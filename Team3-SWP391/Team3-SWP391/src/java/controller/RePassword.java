/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import utils.Encrypt;

/**
 *
 * @author hieub
 */
@WebServlet(name="rePassword", urlPatterns={"/client/repass"})
public class RePassword extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet rePassword</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet rePassword at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession ss = request.getSession();
        ss.invalidate();
        request.setAttribute("erorr", "Bạn không có quyền truy cập đường dẫn này!");
        request.getRequestDispatcher("login.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String pass = request.getParameter("password");
        String email = request.getParameter("email");
        HttpSession ss= request.getSession();
        String active = (String) ss.getAttribute("active")+"";
//        try {
//         String active = ss.getAttribute("active").toString();
//         ss.removeAttribute("active");
//        } catch (Exception e) {
//            request.setAttribute("erorr", "Bạn không thể thực hiện hành động này. Vui lòng thử lại !");
//            request.getRequestDispatcher("login.jsp").forward(request, response);
//        }
        
        if(active.equals("ok")){
        UserDAO u = new UserDAO();
        Encrypt ec = new Encrypt();
        String password = ec.toSHA1(pass);
        u.updateUserPass(email, password);
        ss.invalidate();
        request.setAttribute("erorr", "Mật khẩu đã được thay đổi vui lòng đăng nhập lại .");
        request.getRequestDispatcher("login.jsp").forward(request, response);
        }else{
        request.setAttribute("erorr", "Đường dẫn đã hết hạn hoặc bạn không có quyền truy cập!");
        request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
