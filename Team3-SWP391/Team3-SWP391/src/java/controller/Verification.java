/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import utils.Encrypt;
import utils.SendEmail;

/**
 *
 * @author hieub
 */
@WebServlet(name = "verification", urlPatterns = {"/client/verification"})
public class Verification extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet verification</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet verification at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("error", "Bạn không có quyền truy cập đường dẫn này.");
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "", email = "";
        PrintWriter out = response.getWriter();
        HttpSession ss = request.getSession();
        String otp = "ok";
        try {
            otp = (String) ss.getAttribute("otp") + "";
        } catch (Exception e) {
        }
        out.print("otp la '" + otp + "'");
        otp.trim();
        if (otp.equals("null")) {
            request.setAttribute("erorr", "Mã otp của bạn đã hết hạn, vui lòng thử gửi lại .");
            request.getRequestDispatcher("login.jsp").forward(request, response);
            out.print("qua day2");
        }else{
        email = request.getParameter("email");
        String status = request.getParameter("status");
        request.setAttribute("status", status);
        request.setAttribute("email", email);
        action = request.getParameter("action");

        if (action.equals("submit")) {

            String num1 = request.getParameter("num1").trim();
            String num2 = request.getParameter("num2").trim();
            String num3 = request.getParameter("num3").trim();
            String num4 = request.getParameter("num4").trim();
            String s = num1 + num2 + num3 + num4;

            if (s.equals(otp)) {
                if (status.equals("forget")) {
                    request.removeAttribute("email");

                    try {
                        ss.removeAttribute("otp");
                    } catch (Exception e) {
                    }

                    request.setAttribute("email", email);
                    ss.setAttribute("active", "ok");
                    request.getRequestDispatcher("rePassword.jsp").forward(request, response);
                } else {
                    UserDAO u = new UserDAO();
                    u.setStatus(email);
                    ss.invalidate();
                    request.setAttribute("erorr", "Xác thực tài khoản thành công. Vui lòng đăng nhập lại.");
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                }
            } else {
                request.setAttribute("erorr", "Mã OTP không chính xác, vui lòng thử lại.");
                request.getRequestDispatcher("verification.jsp").forward(request, response);
            }
        } else if (action.equals("reOtp")) {
            request.setAttribute("status", status);

            out.print("da chay den day 110");
            String captcha = request.getParameter("captcha");
            String captchas = "";
            try {

                captchas = ss.getAttribute("captchas").toString();
                ss.removeAttribute("captchas");
                out.print("da chay den day 117");
            } catch (Exception e) {

                request.setAttribute("erorr", "Mã captcha bị lỗi vui lòng thử lại. Xin lỗi vì sự bất tiện này.");
                request.getRequestDispatcher("login.jsp").forward(request, response);
                out.print("da chay den day 121");
            }
            if (captcha.equals(captchas)) {

                out.print("da chay den day 125");
                try {

                    ss.removeAttribute("otp");
                } catch (Exception e) {
                }
                Encrypt e = new Encrypt();

                otp = e.generateOtp(4);
                ss.setAttribute("otp", otp);
                SendEmail sd = new SendEmail();
                request.setAttribute("status", status);
                if(status.equals("forget")){
                    sd.threadMail(email, "OTP lấy lại mật khẩu tại Autothecao!", otp);
                }else{
                sd.threadMail(email, "Xác thực tài khoản tại Autothecao!", otp);
                }
                //sd.sendEmail("fpt.autothecao@gmail.com", "gyiyhediihgarcvt", email, "Xác thực tài khoản tại Autothecao!", otp);
                request.setAttribute("erorr", "Mail đã được gửi. Vui lòng kiểm tra hòm thư của bạn.");
                request.getRequestDispatcher("verification.jsp").forward(request, response);
            } else {
                out.print("da chay den day 139");
                request.setAttribute("status", status);
                request.setAttribute("email", email);
                request.setAttribute("erorr", "Mã captcha không chính xác, vui lòng nhập lại!");
                request.getRequestDispatcher("verification.jsp").forward(request, response);
            }
        } else {

            request.setAttribute("erorr", "Mã otp của bạn đã hết hạn, vui lòng thử gửi lại .");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
