/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author hieub
 */
public class Product extends StatusObj{
    private String createdBy;
    private String key;
    private String name;
    private String image;
    private String describe;
    private double price;
    private int profit;
    private int supplier;
    private int quantity;
    private int id;

    public Product() {
    }

    public Product(boolean isDeleted,String createdBy, Timestamp createdAt, Timestamp updatedAt, String updatedBy, Timestamp deletedAt, String deletedBy,String key, String name, String image, String describe, double price, int profit, int supplier, int quantity, int id) {
        super(isDeleted, createdAt, updatedAt, updatedBy, deletedAt, deletedBy);
        this.key = key;
        this.name = name;
        this.image = image;
        this.describe = describe;
        this.price = price;
        this.profit = profit;
        this.supplier = supplier;
        this.quantity = quantity;
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

    public int getSupplier() {
        return supplier;
    }

    public void setSupplier(int supplier) {
        this.supplier = supplier;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
}
