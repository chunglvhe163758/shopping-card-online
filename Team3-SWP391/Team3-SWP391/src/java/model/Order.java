/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author hieub
 */
public class Order extends StatusObj{
    private int product;
    private double price;
    private double  total;
    private String name;
    private int quantity;
    private String status;
    private String data;
    private int transaction;
    private String email;
    private int user;
    private int id;
    private String createdBy;
    public Order() {
    }

    public Order( boolean isDeleted,String createdBy, Timestamp createdAt, Timestamp updatedAt, String updatedBy, Timestamp deletedAt, String deletedBy,int product, double price, double total, String name, int quantity, String status, String data, int transaction,String email, int user, int id) {
        super(isDeleted, createdAt, updatedAt, updatedBy, deletedAt, deletedBy);
        this.product = product;
        this.price = price;
        this.total = total;
        this.name = name;
        this.quantity = quantity;
        this.status = status;
        this.data = data;
        this.transaction = transaction;
        this.user = user;
        this.id = id;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

   

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getTransaction() {
        return transaction;
    }

    public void setTransaction(int transaction) {
        this.transaction = transaction;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
}
