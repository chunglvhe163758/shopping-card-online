/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author hieub
 */
public class User extends StatusObj{
   
   private String name;
   private String email;
   private String password;
   private boolean status;
   private String role;
   private double wallet;
   private String fullName;
   private String phone;
   private String avatar;
   private int id;

    public User() {
    }

    public User(boolean isDeleted, Timestamp createdAt, Timestamp updatedAt, String updatedBy, Timestamp deletedAt, String deletedBy,String name, String email, String password, boolean status, String role, double wallet, String fullName, String phone, String avatar, int id) {
        super(isDeleted, createdAt, updatedAt, updatedBy, deletedAt, deletedBy);
        this.name = name;
        this.email = email;
        this.password = password;
        this.status = status;
        this.role = role;
        this.wallet = wallet;
        this.fullName = fullName;
        this.phone = phone;
        this.avatar = avatar;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public double getWallet() {
        return wallet;
    }

    public void setWallet(double wallet) {
        this.wallet = wallet;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
   
    
}
