/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Order;

/**
 *
 * @author hieub
 */
public class OrderDAO extends MyDAO{
    public int countOrderByID() {
        xSql = "SELECT COUNT(id) FROM `order`;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int countOrderByTotal() {
        xSql = "SELECT SUM(total) FROM `order`";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Order> getAllOrderByFilter(String search, String SupId) {
        List<Order> list = new ArrayList<>();
        xSql = "select * from `order` o , supplier s\n"
                + "where o.id = s.id and o.name like ?";
        if (!SupId.isEmpty()) {
            xSql = xSql + " and  o.id = " + SupId;
        }
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setCreatedAt(rs.getTimestamp("createdAt"));
                order.setStatus(rs.getString("status"));
                order.setTotal(rs.getDouble("total"));
                order.setData(rs.getString("data"));
                list.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> getAllOrder() {
        xSql = "SELECT * FROM `order`";
        List<Order> list = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setCreatedAt(rs.getTimestamp("createdAt"));
                order.setStatus(rs.getString("status"));
                order.setTotal(rs.getDouble("total"));
                order.setData(rs.getString("data"));
                list.add(order);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    
    // delete order
    public void deleteOrder(String id){
        xSql = "DELETE  FROM `order` WHERE id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//
//    public List<Order> pagingOrder(int index) {
//        List<Order> list = new ArrayList<>();
//        xSql = "SELECT * FROM `order` ORDER BY createdAt DESC LIMIT 6 OFFSET ?;";
//        try {
//            ps = con.prepareStatement(xSql);
//            ps.setInt(1, (index - 1) * 6);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                Order order = new Order();
//                order.setId(rs.getInt("id"));
//                order.setName(rs.getString("name"));
//                order.setIsDeleted(rs.getBoolean("isDeleted"));
//                order.setQuantity(rs.getInt("quantity"));
//                order.setTotal(rs.getDouble("total"));
//                order.setStatus(rs.getString("status"));
//                list.add(order);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return list;
//    }

    public ArrayList<Order> getAllOrderByName(String query, int index) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            xSql = "SELECT * FROM `order` WHERE (? is null or name like ?) ORDER BY createdAt DESC LIMIT 6 OFFSET ?;";
            ps = con.prepareStatement(xSql);
            ps.setString(1, query);
            ps.setString(2, "%" + query + "%");
            ps.setInt(3, (index - 1) * 6);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setIsDeleted(rs.getBoolean("isDeleted"));
                order.setQuantity(rs.getInt("quantity"));
                order.setTotal(rs.getDouble("total"));
                order.setStatus(rs.getString("status"));
                order.setData(rs.getString("data"));
                list.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    public Order getOrderById(int id){
        xSql = "select isDeleted,createdAt,product,price,total,name,quantity,data,id from `order` where id = ?";
        Order x = new Order();
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {                
                x.setIsDeleted(rs.getBoolean(1));
                x.setCreatedAt(rs.getTimestamp(2));
                x.setProduct(rs.getInt(3));
                x.setPrice(rs.getDouble(4));
                x.setTotal(rs.getDouble(5));
                x.setName(rs.getString(6));
                x.setQuantity(rs.getInt(7));
                x.setData(rs.getString(8));
                x.setId(rs.getInt(9));
                ps.close();
            }
        } catch (Exception e) {
        }
        return x;
    }
        public Order getOrderByTran(int id){
        xSql = "select isDeleted,createdAt,product,price,total,name,quantity,data,id from `order` where transaction = ?";
        Order x = new Order();
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {                
                x.setIsDeleted(rs.getBoolean(1));
                x.setCreatedAt(rs.getTimestamp(2));
                x.setProduct(rs.getInt(3));
                x.setPrice(rs.getDouble(4));
                x.setTotal(rs.getDouble(5));
                x.setName(rs.getString(6));
                x.setQuantity(rs.getInt(7));
                x.setData(rs.getString(8));
                x.setId(rs.getInt(9));
                ps.close();
            }
        } catch (Exception e) {
        }
        return x;
    }
    
    public void insertOrder(Order x){
        try {
            xSql = "insert into `order`(isDeleted,createdAt,product,price,total,name,quantity,status,transaction,user) values (?,?,?,?,?,?,?,?,?,?) ";
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getTime());
            ps.setInt(3, x.getId());
            ps.setDouble(4, x.getPrice());
            ps.setDouble(5, x.getTotal());
            ps.setString(6, x.getName());
            ps.setInt(7, x.getQuantity());
            ps.setString(8, "fail");
            ps.setInt(9, x.getTransaction());
            ps.setInt(10, x.getUser());
            ps.executeUpdate();


            System.out.println("tao order");
            ps.close();
        } catch (Exception e) {
        }
    }
}
