/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import model.Product;
import model.Supplier;

/**
 *
 * @author hieub
 */
public class ProductDAO extends MyDAO {
public int countProductById() {
        xSql = "SELECT COUNT(id) FROM product";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void insertProduct(String name, double price, String describe, int supplier) {
        xSql = "INSERT INTO product (name, price, `describe`, supplier)\n"
                + "VALUES (?,?,?,?);";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            ps.setString(1, name);
            ps.setDouble(2, price);
            ps.setString(3, describe);
            ps.setInt(4, supplier);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public List<Product> getAllProduct() {
        List<Product> lst = new ArrayList<>();
        xSql = "select * from product where isDeleted = 0";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product products = new Product();
                products.setId(rs.getInt("id"));
                products.setName(rs.getString("name"));
//                products.setImg(rs.getString("img"));
                products.setPrice(rs.getDouble("price"));
                lst.add(products);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Collections.sort(lst, (o1, o2) -> {
            int p1 = (int) o1.getPrice();
            int p2 = (int) o2.getPrice();
            if (p1 > p2) {
                return 1;
            } else if (p1 < p2) {
                return -1;
            } else {
                return 0;
            } // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/LambdaBody
        });
        return lst;
    }
    ///delete product

    public void deleteProduct(String pid) {
        Product x = new Product();
        xSql = "UPDATE product\n"
                + "SET isDeleted = 1,updatedAt = '" + x.getTime() + "'"
                + "WHERE id = " + pid;
        try {
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }

    public void editProduct(String name, String image, Double price,
            String title, String describe, int supplier) {
        Product x = null;
        xSql = "update product\n"
                + "set [name] = ?,\n"
                + "[image] = ?,\n"
                + "price = ?,\n"
                + "title = ?,\n"
                + "[describe] = ?,\n"
                + "supplierID = ?\n"
                + "where id = ?";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            ps.setBoolean(1, false);
            ps.setString(2, "admin");
            ps.setTimestamp(3, x.getTime());
            ps.setString(4, name);
            ps.setString(5, image);
            ps.setDouble(6, price);
            ps.setString(7, title);
            ps.setString(8, describe);
            ps.setInt(9, supplier);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //count by product
    public List<Product> viettelQuantityList() {
        List<Product> list = new ArrayList<>();
        xSql = "SELECT * FROM `product` WHERE name LIKE 'viettel%'";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setName(rs.getString("name"));
                product.setQuantity(rs.getInt("quantity"));
                list.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    //search products manager
    public List<Product> searchByName(String txtSearch) {
        List<Product> lst = new ArrayList<>();
        xSql = "select * from product\n"
                + "where name like ?;";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + txtSearch + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setPrice(rs.getDouble("price"));
                lst.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lst;
    }
    public List<Product> getProductBySup(int id) {
        List<Product> lst = new ArrayList<>();
        xSql = "select * from product where supplier = " + id +" and quantity > 0 and isDeleted = 0";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getBoolean(1),rs.getString(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getTimestamp(6), rs.getString(7),rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getDouble(12), rs.getInt(13), rs.getInt(14), rs.getInt(15), rs.getInt(16));
                System.out.println(p.getId());
                lst.add(p);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }

        Collections.sort(lst, (o1, o2) -> {
            int p1 = (int) o1.getPrice();
            int p2 = (int) o2.getPrice();
            if (p1 > p2) {
                return 1;
            } else if (p1 < p2) {
                return -1;
            } else {
                return 0;
            } // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/LambdaBody
        });
        return lst;
    }
    
    public Product getProductById(int id){
        Product x = null;
        try {
            xSql ="select * from product where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {                
                x = new Product(rs.getBoolean(1),rs.getString(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getTimestamp(6), rs.getString(7),rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getDouble(12), rs.getInt(13), rs.getInt(14), rs.getInt(15), rs.getInt(16));
                
            }
            ps.close();
        } catch (Exception e) {
        }
        return x;
    }
 
    public List<Supplier> getAllSupplier(){
        xSql = "select * from supplier";
        List<Supplier> lst = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while(rs.next()){
                Supplier x = new Supplier(rs.getBoolean(1), rs.getString(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getTimestamp(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getBoolean(11), rs.getInt(12));
                lst.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return lst;
    }
 
}
