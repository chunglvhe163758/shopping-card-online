/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import com.google.gson.JsonArray;
import com.mysql.cj.xdevapi.PreparableStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import model.Product;
import model.Storage;
import com.google.gson.JsonObject;
import model.Order;

/**
 *
 * @author hieub
 */
public class StorageDAO extends MyDAO {
public List<Storage> getAllStorage() {
        xSql = "SELECT * FROM `storage` Limit 5;"; 
        List<Storage> list = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while(rs.next()){
                Storage storage = new Storage();
                storage.setId(rs.getInt("id"));
                storage.setCode(rs.getString("code"));
                storage.setExp(rs.getTimestamp("exp"));
                storage.setStatus(rs.getBoolean("status"));
                storage.setSeri(rs.getString("seri"));
                storage.setCreatedAt(rs.getTimestamp("createdAt"));
                list.add(storage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    public void addStorage(Storage x) {
        xSql = "insert into storage(isDeleted,createdBy,createdAt,seri,code,exp,status,product) values (?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setString(2, "admin");
            ps.setTimestamp(3, x.getTime());
            ps.setString(4, x.getSeri());
            ps.setString(5, x.getCode());
            ps.setTimestamp(6, x.getExp());
            ps.setBoolean(7, x.isStatus());
            ps.setInt(8, x.getProduct());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }

    }
    
    public void buyCardVnpay(int id){
        OrderDAO od = new OrderDAO();
        List<Storage> list = new ArrayList<>();
        Order x = od.getOrderByTran(id);
                try {
            con.setAutoCommit(false);
            xSql = "Update product SET quantity = quantity - ? Where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getQuantity());
            ps.setInt(2, x.getProduct());
            ps.executeUpdate();
            ps.close();

        } catch (Exception e) {
        }

        try {
            xSql = "SELECT * FROM storage\n"
                    + "WHERE status = 1 and product = ?\n"
                    + "LIMIT ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getProduct());
            ps.setInt(2, x.getQuantity());
            rs = ps.executeQuery();
            while (rs.next()) {
                Storage y = new Storage();
                y.setCode(rs.getString("code"));
                y.setSeri(rs.getString("seri"));
                y.setExp(rs.getTimestamp("exp"));
                list.add(y);
            }
            System.out.println(list.toString());
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        //convert sang json
        JsonArray jsonArray = new JsonArray();
        for (Storage s : list) {
            JsonObject o = new JsonObject();
            o.addProperty("seri", s.getSeri());
            o.addProperty("code", s.getCode());
            String time = s.getExp().toString();
            o.addProperty("exp", time);
            jsonArray.add(o);
        }
        String result = jsonArray.toString();
        try {
            xSql = "Update order SET data = ?,status='success' where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setString(1, result);
            ps.setInt(2, x.getId());
            ps.executeUpdate();
        } catch (Exception e) {
        }
                try {
            xSql = "UPDATE storage\n"
                    + "SET status = 0,order_id = ?\n"
                    + "WHERE status = 1 and product = ?\n"
                    + "LIMIT ?;";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getId());
            ps.setInt(2, x.getProduct());
            ps.setInt(3, x.getQuantity());
            ps.executeUpdate();
            ps.close();
            con.commit();
        } catch (Exception e) {
        }
    }

    public void buyCard(int quantity, Product x, int transaction, String email,int user, double total) {
        //tru so luong the hien co trong kho
        List<Storage> list = new ArrayList<>();
        int order = 0;
        try {
            con.setAutoCommit(false);
            xSql = "Update product SET quantity = quantity - ? Where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, quantity);
            ps.setInt(2, x.getId());
            ps.executeUpdate();
            ps.close();

        } catch (Exception e) {
        }

        try {
            xSql = "SELECT * FROM storage\n"
                    + "WHERE status = 1 and product = ?\n"
                    + "LIMIT ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getId());
            ps.setInt(2, quantity);
            rs = ps.executeQuery();
            while (rs.next()) {
                Storage y = new Storage();
                y.setCode(rs.getString("code"));
                y.setSeri(rs.getString("seri"));
                y.setExp(rs.getTimestamp("exp"));
                list.add(y);
            }
            System.out.println(list.toString());
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        //convert sang json
        JsonArray jsonArray = new JsonArray();
        for (Storage s : list) {
            JsonObject o = new JsonObject();
            o.addProperty("seri", s.getSeri());
            o.addProperty("code", s.getCode());
            String time = s.getExp().toString();
            o.addProperty("exp", time);
            jsonArray.add(o);
        }
        String result = jsonArray.toString();
        System.out.println(result);
        // tao mot order moi
        try {
            xSql = "insert into `order`(isDeleted,createdAt,product,price,total,name,quantity,status,data,transaction,email,user) values (?,?,?,?,?,?,?,?,?,?,?,?) ";
            ps = con.prepareStatement(xSql, Statement.RETURN_GENERATED_KEYS);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getTime());
            ps.setInt(3, x.getId());
            ps.setDouble(4, x.getPrice());
            ps.setDouble(5, total);
            ps.setString(6, x.getName());
            ps.setInt(7, quantity);
            ps.setString(8, "success");
            ps.setString(9, result);
            ps.setInt(10, transaction);
            ps.setString(11, email);
            ps.setInt(12, user);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                order = rs.getInt(1);
            }
            System.out.println("tao order");
            ps.close();
        } catch (Exception e) {
        }
        System.out.println("tao xong order");
        //cap nhat the da ban
        try {
            xSql = "UPDATE storage\n"
                    + "SET status = 0,order_id = ?\n"
                    + "WHERE status = 1 and product = ?\n"
                    + "LIMIT ?;";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, order);
            ps.setInt(2, x.getId());
            ps.setInt(3, quantity);
            ps.executeUpdate();
            ps.close();
            con.commit();
        } catch (Exception e) {
        }
    }
    //lay ra ma the

    //    public List<Storage> buyCard(int product, int id, int quantity) {
    //        List<Storage> lst = new ArrayList<>();
    //        xSql = "BEGIN;\n"
    //                + "SELECT * FROM storage\n"
    //                + "WHERE status = 1 and product =" + product + "\n"
    //                + "LIMIT " + quantity + "\n"
    //                + "FOR UPDATE;\n"
    //                + "\n"
    //                + "UPDATE storage\n"
    //                + "SET status = 0,order_id = " + id + "\n"
    //                + "WHERE status = 1 and product =" + product + "\n"
    //                + "LIMIT " + quantity + ";\n"
    //                + "COMMIT ;";
    //        try {
    //            ps = con.prepareStatement(xSql);
    //            rs = ps.executeQuery();           
    //            while (rs.next()) {
    //                Storage x = new Storage();
    //                x.setSeri(rs.getString("seri"));
    //                x.setCode(rs.getString("code"));
    //                x.setExp(rs.getTimestamp("exp"));
    //                lst.add(x);
    //            }
    //            rs.close();
    //            ps.close();
    //        } catch (Exception e) {
    //        }
    //        return lst;
    //    }
//        public static void main(String[] args) {
//            Random random = new Random();
//            StorageDAO sd = new StorageDAO();
//            for (int j = 26; j < 37; j++) {
//                            for (int i = 0; i < 30; i++) {
//                int randomNumber = random.nextInt(900000000) + 100000000;
//            String num = String.valueOf(randomNumber);
//            int randomNumber1 = random.nextInt(900000000) + 100000000;
//            String num1 = String.valueOf(randomNumber1);
//            int id = random.nextInt(25) + 1;
//            Timestamp time =  Timestamp.valueOf("2029-06-13 18:47:58");
//                System.out.println(time);
//                
//            Storage y = new Storage();
//                System.out.println(y.getTime());
//                System.out.println(num +"::::"+num1);
//                System.out.println(id);
//                System.out.println(randomNumber1 +"::::"+randomNumber);
//           Storage x = new Storage(false, "hieu", y.getTime(), y.getTime(), "hieu", y.getTime(), "hieu", num1, num, time, true,0, j, 0);
//           sd.addStorage(x);
//            }
//            }
//
//           
//        }
    public static void main(String[] args) {
        StorageDAO sd = new StorageDAO();
        List<Storage> lst = new ArrayList<>();
        lst = sd.getAllStorage();
        for (int i = 0; i < lst.size(); i++) {
            System.out.println(lst.get(i).isStatus());
        }
    }
}
