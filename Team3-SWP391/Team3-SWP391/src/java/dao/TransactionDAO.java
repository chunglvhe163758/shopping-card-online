/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Transaction;
import vnpay.Config;

/**
 *
 * @author hieub
 */
public class TransactionDAO extends MyDAO {

    public void insertTransaction(Transaction x) {
        xSql = "insert into transaction(isDeleted,createdAt,name,type,total,status,content,user,code) values(?,?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getCreatedAt());
            ps.setString(3, x.getName());
            ps.setBoolean(4, x.isType());
            ps.setDouble(5, x.getTotal() / 100);
            ps.setString(6, x.getStatus());
            ps.setString(7, x.getContent());
            ps.setInt(8, x.getUser());
            ps.setString(9, x.getCode());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }

    public int insertTransaction1(Transaction x) {
        xSql = "insert into transaction(isDeleted,createdAt,name,type,total,status,content,user,code) values(?,?,?,?,?,?,?,?,?)";
        int order = 0;
        try {
            ps = con.prepareStatement(xSql, Statement.RETURN_GENERATED_KEYS);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getCreatedAt());
            ps.setString(3, x.getName());
            ps.setBoolean(4, x.isType());
            ps.setDouble(5, x.getTotal() / 100);
            ps.setString(6, x.getStatus());
            ps.setString(7, x.getContent());
            ps.setInt(8, x.getUser());
            ps.setString(9, x.getCode());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                order = rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {

        }
        return order;
    }

    public List<Transaction> getTransactionByUser(int id, int limit, int off) {
        List<Transaction> lst = new ArrayList<>();
        xSql = "SELECT * FROM `transaction` WHERE user = " + id + "\n"
                + "ORDER BY createdAt DESC\n"
                + "LIMIT " + limit + " OFFSET " + off + "";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                boolean isDeleted = rs.getBoolean(1);
                Timestamp created = rs.getTimestamp(2);
                Timestamp updatedAt = rs.getTimestamp(3);
                String updatedBy = rs.getString(4);
                Timestamp deletedAt = rs.getTimestamp(5);
                String deletedBy = rs.getString(6);
                String name = rs.getString(7);
                boolean type = rs.getBoolean(8);
                Double total = rs.getDouble(9);
                String status = rs.getString(10);
                String content = rs.getString(11);
                int user = rs.getInt(12);
                String code = rs.getString(13);
                int id1 = rs.getInt(14);
                Transaction x = new Transaction(name, type, total, status, content, user, code, id, isDeleted, created, updatedAt, updatedBy, deletedAt, deletedBy);
                lst.add(x);
            }
        } catch (Exception e) {
        }
        return lst;
    }

    public void setStatus(String status, String code, int ammount) {
        Transaction x = new Transaction();
        xSql = "UPDATE transaction\n"
                + "SET status = '" + status + "' , updatedAt = '" + x.getTime() + "'\n"
                + "WHERE id = (Select id from transaction where code = '" + code + "'\n"
                + "order by createdAt desc\n"
                + "Limit 1)";
        try {
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        if (status.equals("success")) {
            xSql = "UPDATE user SET wallet = wallet + " + ammount + " WHERE id = (select user from transaction where code = '" + code + "')";
            try {
                ps = con.prepareStatement(xSql);
                ps.executeUpdate();
                ps.close();
            } catch (Exception e) {
                System.out.println("loi database");
            }
        }
    }

    public int countTransaction(int id) {
        xSql = "select COUNT(id) as counttrans from transaction where user =" + id;
        int count = 0;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return count;
    }

    public int buyCardTransaction(int id, double ammount) {
        int order = 0;
        Transaction x = new Transaction();
        String s = Config.getRandomNumber(6);
        String str = "Thanh toán đơn hàng mã " + s;
        xSql = "insert into transaction(isDeleted,createdAt,name,type,total,status,content,user,code) values(?,?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql, Statement.RETURN_GENERATED_KEYS);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getTime());
            ps.setString(3, str);
            ps.setBoolean(4, false);
            ps.setDouble(5, ammount);
            ps.setString(6, "success");
            ps.setString(7, "mua the");
            ps.setInt(8, id);
            ps.setString(9, s);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                order = rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
        }
        try {
            xSql = "UPDATE user SET wallet = wallet - " + ammount + " WHERE id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        return order;
    }
}
