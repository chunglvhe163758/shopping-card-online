/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 *
 * @author hieub
 */
public class UserDAO extends MyDAO {

    public void insertUser(User x) {
        Timestamp time = x.getTime();
        xSql = "insert into User (isDeleted,createdAt,name,email,password,status,role,wallet) values (?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, time);
            ps.setString(3, x.getName());
            ps.setString(4, x.getEmail());
            ps.setString(5, x.getPassword());
            ps.setBoolean(6, false);
            ps.setString(7, "customer");
            ps.setDouble(8, 0);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }
    
    public String getEmailByName(String x){
         xSql = "select email from User where name like '" + x + "'";

      String s = "";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                s = rs.getString(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return s;
    }
    
    public boolean isUserByName(String x) {

        xSql = "select id from User where name like '" + x + "'";

        boolean check = false;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                check = true;
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return check;
    }

    public boolean isUserByEmail(String x) {

        xSql = "select id from User where email like '" + x + "'";

        boolean check = false;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                check = true;
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return check;
    }

    public void setStatus(String email) {
        User x = new User();
        xSql = "UPDATE user\n"
                + "SET status = 1 , updatedAt = '"+x.getTime()+"'\n"
                + "WHERE email like \"" + email + "\"";
        try {
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }

    public User checkPassword(String name, String pass) {

        xSql = "select name,email,password,status,role,wallet,id,fullname,phone from User where name like \"" + name + "\" and password like '%"+pass+"%'";

        User x = new User();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x.setName(rs.getString(1));
                x.setEmail(rs.getString(2));
                x.setPassword(rs.getString(3));
                x.setStatus(rs.getBoolean(4));
                x.setRole(rs.getString(5));
                x.setWallet(rs.getDouble(6));
                x.setId(rs.getInt(7));
                x.setFullName(rs.getString(8));
                x.setPhone(rs.getString(9));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            x.setId(0);
        }
        return x;
    }
    public User getUserByID(int id){
         xSql = "select name,email,password,status,role,wallet,id,fullname,phone from User where id ="+id;

        User x = new User();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x.setName(rs.getString(1));
                x.setEmail(rs.getString(2));
                x.setPassword(rs.getString(3));
                x.setStatus(rs.getBoolean(4));
                x.setRole(rs.getString(5));
                x.setWallet(rs.getDouble(6));
                x.setId(rs.getInt(7));
                x.setFullName(rs.getString(8));
                x.setPhone(rs.getString(9));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            x.setId(0);
        }
        return x;
    }
    public String getNameByEmail(String x){
             xSql = "select name from User where email like '" + x + "'";

      String s = "";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                s = rs.getString(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return s;
    }
    
    public void updateUserPass(String email,String pass){
          User x = new User();
        xSql = "UPDATE user\n"
                + "SET password = '"+pass+"' , updatedAt = '"+x.getTime()+"'\n"
                + "WHERE email like '" + email + "'";
        try {
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }
    
    public void updateUser(User x){
           
        xSql = "UPDATE user\n"
                + "SET fullname = '"+x.getFullName()+"' , updatedAt = '"+x.getTime()+"', phone = '"+x.getPhone()+"'\n"
                + "WHERE id = " + x.getId() ;
        try {
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }
    public String checkPassById(int id){
                 xSql = "select password from User where id = " + id;

    String s = "";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
             s = rs.getString(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return s;
    }
  public int countUserById() {
        xSql = "SELECT COUNT(id) FROM user";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    //manager user
    public List<User> showAllUser() {
        List<User> list = new ArrayList<>();
        xSql = "SELECT name,email,password,status,role,wallet,id,fullname,phone FROM user";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                User x = new User();
                x.setName(rs.getString("name"));
                x.setEmail(rs.getString("email"));
                x.setPassword(rs.getString("password"));
                x.setStatus(rs.getBoolean("status"));
                x.setRole(rs.getString("role"));
                x.setWallet(rs.getDouble("wallet"));
                x.setId(rs.getInt("id"));
                x.setFullName(rs.getString("fullname"));
                x.setPhone(rs.getString("phone"));
                list.add(x);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<User> showCategoriesUser() {
        List<User> list = new ArrayList<>();
        xSql = "SELECT role FROM user group BY role";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                User x = new User();
                x.setRole(rs.getString("role"));
                list.add(x);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
