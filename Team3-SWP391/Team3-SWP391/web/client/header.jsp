<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.User" %>
<!DOCTYPE html>
<!-- Coding By CodingNepal - codingnepalweb.com -->
<html lang="en">

    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Sidebar Menu | Side Navigation Bar</title>
        <!-- CSS -->
        <link rel="stylesheet" href="css/style.css" />
        <!-- Boxicons CSS -->
        <link rel="stylesheet" href="CSS/font-awesome.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css" rel="stylesheet" />
        <link href="../client/loading.css" rel="stylesheet" />
    </head>
    <style>
        /* Google Fonts - Poppins */
        @import url("https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap");

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: "Poppins", sans-serif;
        }

        header {
            position: absolute;
            background: #e3f2fd;
            z-index: 1;
        }



        nav {
            position: fixed;
            top: 0;
            left: 0;
            height: 70px;
            width: 100%;
            display: flex;
            align-items: center;
            background: #2E95EF;
            box-shadow: 0 0 1px rgba(0, 0, 0, 0.1);
        }

        nav .logo {
            display: flex;
            align-items: center;
            margin: 0 24px;
        }

        .logo .menu-icon {
            color: #333;
            font-size: 24px;
            margin-right: 14px;
            cursor: pointer;
        }

        .logo .logo-name {
            color: #333;
            font-size: 22px;
            font-weight: 500;
        }

        nav .sidebar {
            position: fixed;
            top: 0;
            left: -100%;
            height: 100%;
            width: 260px;
            padding: 20px 0;
            background-color: #fff;
            box-shadow: 0 5px 1px rgba(0, 0, 0, 0.1);
            transition: all 0.4s ease;
        }

        nav.open .sidebar {
            left: 0;
        }

        .sidebar .sidebar-content {
            display: flex;
            height: 100%;
            flex-direction: column;
            justify-content: space-between;
            padding: 30px 16px;
        }

        .sidebar-content .list {
            list-style: none;
        }

        .list .nav-link {
            display: flex;
            align-items: center;
            margin: 8px 0;
            padding: 14px 12px;
            border-radius: 8px;
            text-decoration: none;
        }

        .lists .nav-link:hover {
            background-color: #4070f4;
        }

        .nav-link .icon {
            margin-right: 14px;
            font-size: 20px;
            color: #707070;
        }

        .nav-link .link {
            font-size: 16px;
            color: #707070;
            font-weight: 400;
        }

        .lists .nav-link:hover .icon,
        .lists .nav-link:hover .link {
            color: #fff;
        }

        .overlay1 {
            position: fixed;
            top: 0;
            left: -100%;
            height: 1000vh;
            width: 200%;
            opacity: 0;
            pointer-events: none;
            transition: all 0.4s ease;

        }

        nav.open~.overlay1 {
            opacity: 1;
            left: 260px;
            pointer-events: auto;
        }
        nav {
            background-color: #F0F8FF;
            background-image: linear-gradient(180deg, #F0F8FF 0%, #E6FAFF 100%);
        }

        nav .logo-name {
            color: #333;
        }

        nav .menu-icon {
            color: #333;
        }

        nav .nav-link {
            color: #333;
        }

        nav .nav-link:hover {
            background-color: #333;
            color: #F0F8FF;
        }

        nav.open .sidebar {
            background-color: #F0F8FF;
        }

        nav.open .nav-link:hover {
            background-color: #333;
            color: #F0F8FF;
        }

    </style>
    <%Object obj = session.getAttribute("account");
     User u = null;
     if(obj !=null) u = (User)obj;
    %>
    <header>
        <!--        <div  id="loader-wrapper">
                    <div id="loader"></div>
        
                    <div class="loader-section section-left"></div>
                    <div class="loader-section section-right"></div>
        
                </div>-->

        <div style="display: flex;">
            <div>
                <nav>
                    <div class="logo" style="size: 238px 290px;">
                        <i class="bx bx-menu menu-icon"></i>
                        <span class="logo-name">Autothecao</span>
                    </div>
                    <div style="margin-right: 50px;" class="logo">
                        <i class="bx bx-menu menu-icon"></i>
                        <span class="logo-name">Autothecao</span>
                    </div>
                    <div class="sidebar">
                        <div class="logo">
                            <i class="bx bx-menu menu-icon"></i>
                            <span class="logo-name">Autothecao</span>
                        </div>

                        <div class="sidebar-content">
                            <ul class="lists">
                                <li class="list">
                                    <a href="home.jsp" class="nav-link">
                                        <i class="bx bx-home-alt icon"></i>
                                        <span class="link">Trang Chủ</span>
                                    </a>
                                </li>
                                <li class="list">
                                    <a href="#" class="nav-link">
                                        <i class="bx bx-bar-chart-alt-2 icon"></i>
                                        <span class="link">Giới Thiệu</span>
                                    </a>
                                </li>
                                <%if(u == null){}else{
                                %>
                                <li class="list">
                                    <a href="vnpay_pay.jsp" class="nav-link">
                                        <i class="bx bx-money icon"></i>
                                        <span class="link">Nạp Tiền</span>
                                    </a>
                                </li>
                                <li class="list">
                                    <a href="historyTransaction.jsp" class="nav-link">
                                        <i class="bx bx-history icon"></i>
                                        <span class="link">Lịch Sử Giao Dịch</span>
                                    </a>
                                </li>

                                <%}%>
                            </ul>

                            <div class="bottom-cotent">
                                <%if(u == null){%>
                                <li class="list">
                 
                                    <a href="login" class="nav-link">
                                        <i class="bx bx-log-out icon"></i>
                                        <span class="link">Login</span>
                                    </a>
                                </li>
                                <%}%><%else{
                                %>
                                <li class="list">
                                    <a href="#" class="nav-link">
                                        <i class="bx bx-cog icon"></i>
                                        <span class="link">Settings</span>
                                    </a>
                                </li>
                                <li class="list">
                                    <a href="login" class="nav-link">
                                        <i class="bx bx-log-out icon"></i>
                                        <span class="link">Logout</span>
                                    </a>
                                </li>
                                <%}%>
                            </div>
                        </div>
                    </div>
                </nav>
                <nav>
                    <div class="logo">
                        <i class="bx bx-menu menu-icon"></i>
                        <span class="logo-name"><img style="width: 200px;height: 170px; margin-left: -13%;margin-top: 5%;" src="../img/pageLogo.png" alt=""/></span>
                    </div>
                    <%if(u == null){
                    %>
                    <div style="size: 80%; margin-left: 70%; border: 1px solid #62a2b2;" class="logo">                      
                        <a href="login.jsp">Đăng nhập/Đăng ký</a>
                    </div>
                    <%}else{%>
                    <div style="size: 80%; margin-left: 70%; border: 1px solid #62a2b2;" class="logo">                      
                        <a href="profile.jsp">Xin chào <%=u.getName()%>. Wallet:<%=(int)u.getWallet()%> vnd</a>
                    </div>
                    <%}%>

                    <div class="sidebar">
                        <div class="logo">
                            <i class="fa-sharp fa-light fa-bars fa-2xs menu-icon"></i>
                            <span class="logo-name"><img src="../img/pageLogo.png" alt=""/></span>
                        </div>

                        <div class="sidebar-content">
                            <ul class="lists">
                                <li class="list">
                                    <a href="#" class="nav-link">
                                        <i class="bx bx-home-alt icon"></i>
                                        <span class="link">Trang Chủ</span>
                                    </a>
                                </li>
                                <li class="list">
                                    <a href="#" class="nav-link">
                                        <i class="bx bx-bar-chart-alt-2 icon"></i>
                                        <span class="link">Giới Thiệu</span>
                                    </a>
                                </li>
                                <li class="list">
                                    <a href="#" class="nav-link">
                                        <i class="bx bx-bar-chart-alt-2 icon"></i>
                                        <span class="link">Nạp Tiền</span>
                                    </a>
                                </li>
                                <li class="list">
                                    <a href="#" class="nav-link">
                                        <i class="bx bx-bar-chart-alt-2 icon"></i>
                                        <span class="link">Lịch Sử Giao Dịch</span>
                                    </a>
                                </li>




                            </ul>

                            <div class="bottom-cotent">
                                <li class="list">
                                    <a href="#" class="nav-link">
                                        <i class="bx bx-cog icon"></i>
                                        <span class="link">Settings</span>
                                    </a>
                                </li>
                                <li class="list">
                                    <a href="#" class="nav-link">
                                        <i class="bx bx-log-out icon"></i>
                                        <span class="link">Logout</span>
                                    </a>
                                </li>
                            </div>
                        </div>
                    </div>
                </nav>


                <section class="overlay1"></section>
            </div>

        </div>
        <script src="script.js"></script>
        <script>const navBar = document.querySelector("nav"),
                    menuBtns = document.querySelectorAll(".menu-icon"),
                    overlay = document.querySelector(".overlay1");

            menuBtns.forEach((menuBtn) => {
                menuBtn.addEventListener("click", () => {
                    navBar.classList.toggle("open");
                });
            });

            overlay.addEventListener("click", () => {
                navBar.classList.remove("open");
            });</script>
        <script>
            console.log("hieuuuuuuuuuuuuuuuuuuuuuu");

            console.log("hieuuuuuuuuuuuuuuuuuuuuuu");
            // Fakes the loading setting a timeout
            setTimeout(function () {
                const loaded = document.getElementById("loader-wrapper");
                console.log(loaded);
                loaded.style.visibility = "hidden";
                loaded.style.transform = "translateY(-100%)";
                loaded.style.transition = "all 0.3s 1s ease-out";

            }, 1000);



        </script>

    </header>
</html>