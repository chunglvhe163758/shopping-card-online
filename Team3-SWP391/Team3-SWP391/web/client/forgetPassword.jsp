<%-- 
    Document   : newjsp
    Created on : May 26, 2023, 8:30:52 PM
    Author     : hieub
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link
                rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
                />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
        <style>
            @import url("https://fonts.googleapis.com/css2?family=Poppins");

            * {
                    box-sizing: border-box;
            }

            body {
                    display: flex;
                    background-color: #f6f5f7;
                    justify-content: center;
                    align-items: center;
                    flex-direction: column;
                    font-family: "Poppins", sans-serif;
                    overflow: hidden;
                    height: 100vh;
            }

            h1 {
                    font-weight: 700;
                    letter-spacing: -1.5px;
                    margin: 0;
                    margin-bottom: 15px;
            }

            h1.title {
                    font-size: 45px;
                    line-height: 45px;
                    margin: 0;
                    text-shadow: 0 0 10px rgba(16, 64, 74, 0.5);
            }

            p {
                    font-size: 14px;
                    font-weight: 100;
                    line-height: 20px;
                    letter-spacing: 0.5px;
                    margin: 20px 0 30px;
                    text-shadow: 0 0 10px rgba(16, 64, 74, 0.5);
            }

            span {
                    font-size: 14px;
                    margin-top: 25px;
            }

            a {
                    color: #333;
                    font-size: 14px;
                    text-decoration: none;
                    margin: 15px 0;
                    transition: 0.3s ease-in-out;
            }

            a:hover {
                    color: #4bb6b7;
            }

            .content {
                    display: flex;
                    width: 100%;
                    height: 50px;
                    align-items: center;
                    justify-content: space-around;
            }

            .content .checkbox {
                    display: flex;
                    align-items: center;
                    justify-content: center;
            }

            .content input {
                    accent-color: #333;
                    width: 12px;
                    height: 12px;
            }

            .content label {
                    font-size: 14px;
                    user-select: none;
                    padding-left: 5px;
            }

            button {
                    position: relative;
                    border-radius: 20px;
                    border: 1px solid #4bb6b7;
                    background-color: #4bb6b7;
                    color: #fff;
                    font-size: 15px;
                    font-weight: 700;
                    margin: 10px;
                    padding: 12px 80px;
                    letter-spacing: 1px;
                    text-transform: capitalize;
                    transition: 0.3s ease-in-out;
            }

            button:hover {
                    letter-spacing: 3px;
            }

            button:active {
                    transform: scale(0.95);
            }

            button:focus {
                    outline: none;
            }

            button.ghost {
                    background-color: rgba(225, 225, 225, 0.2);
                    border: 2px solid #fff;
                    color: #fff;
            }

            button.ghost i{
                    position: absolute;
                    opacity: 0;
                    transition: 0.3s ease-in-out;
            }

            button.ghost i.register{
                    right: 70px;
            }

            button.ghost i.login{
                    left: 70px;
            }

            button.ghost:hover i.register{
                    right: 40px;
                    opacity: 1;
            }

            button.ghost:hover i.login{
                    left: 40px;
                    opacity: 1;
            }

            form {
                    background-color: #fff;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    flex-direction: column;
                    padding: 0 50px;
                    height: 100%;
                    text-align: center;
            }

            input {
                    background-color: #eee;
                    border-radius: 10px;
                    border: none;
                    padding: 12px 15px;
                    margin: 8px 0;
                    width: 100%;
            }

            .container {
                    background-color: #fff;
                    border-radius: 25px;
                    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
                    position: relative;
                    overflow: hidden;
                    width: 768px;
                    max-width: 100%;
                    min-height: 500px;
            }

            .form-container {
                    position: absolute;
                    top: 0;
                    height: 100%;
                    transition: all 0.6s ease-in-out;
            }

            .login-container {
                    left: 0;
                    width: 50%;
                    z-index: 2;
            }

            .container.right-panel-active .login-container {
                    transform: translateX(100%);
            }

            .register-container {
                    left: 0;
                    width: 50%;
                    opacity: 0;
                    z-index: 1;
            }

            .container.right-panel-active .register-container {
                    transform: translateX(100%);
                    opacity: 1;
                    z-index: 5;
                    animation: show 0.6s;
            }

            @keyframes show {
                    0%,
                    49.99% {
                            opacity: 0;
                            z-index: 1;
                    }

                    50%,
                    100% {
                            opacity: 1;
                            z-index: 5;
                    }
            }

            .overlay-container {
                    position: absolute;
                    top: 0;
                    left: 50%;
                    width: 50%;
                    height: 100%;
                    overflow: hidden;
                    transition: transform 0.6s ease-in-out;
                    z-index: 100;
            }

            .container.right-panel-active .overlay-container {
                    transform: translate(-100%);
            }

            .overlay {
                    background-image: url('image/image.gif');
                    background-repeat: no-repeat;
                    background-size: cover;
                    background-position: 0 0;
                    color: #fff;
                    position: relative;
                    left: -100%;
                    height: 100%;
                    width: 200%;
                    transform: translateX(0);
                    transition: transform 0.6s ease-in-out;
            }

            .overlay::before {
                    content: "";
                    position: absolute;
                    left: 0;
                    right: 0;
                    top: 0;
                    bottom: 0;
                    background: linear-gradient(
                            to top,
                            rgba(46, 94, 109, 0.4) 40%,
                            rgba(46, 94, 109, 0)
                            );
            }

            .container.right-panel-active .overlay {
                    transform: translateX(50%);
            }

            .overlay-panel {
                    position: absolute;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    flex-direction: column;
                    padding: 0 40px;
                    text-align: center;
                    top: 0;
                    height: 100%;
                    width: 50%;
                    transform: translateX(0);
                    transition: transform 0.6s ease-in-out;
            }

            .overlay-left {
                    transform: translateX(-20%);
            }

            .container.right-panel-active .overlay-left {
                    transform: translateX(0);
            }

            .overlay-right {
                    right: 0;
                    transform: translateX(0);
            }

            .container.right-panel-active .overlay-right {
                    transform: translateX(20%);
            }

            .social-container {
                    margin: 20px 0;
            }

            .social-container a {
                    border: 1px solid #dddddd;
                    border-radius: 50%;
                    display: inline-flex;
                    justify-content: center;
                    align-items: center;
                    margin: 0 5px;
                    height: 40px;
                    width: 40px;
                    transition: 0.3s ease-in-out;
            }

            .social-container a:hover {
                    border: 1px solid #4bb6b7;
            }
            .form-group.invalid .form-control {
                    border-color: #f33a58;
            }

            .form-group.invalid .form-message {
                    color: #f33a58;
            }
            * {
                    padding: 0;
                    margin: 0;
                    box-sizing: border-box;
            }

            body {
                    height: 100vh;
                    display: flex;
                    flex-direction: column;
                    font-family: "Helvetica Neue";
                    background-color: #f4f4f5;
            }

            body > div {
                    margin: auto;
            }

            /* ======= Buttons ======== */

            /* Block */
            .btn {
                    display: inline-block;
                    text-decoration: none;
                    background-color: transparent;
                    border: none;
                    outline: none;
                    color: #fff;
                    padding: 12px 48px;
                    border-radius: 50px;
                    cursor: pointer;
                    min-width: 120px;
                    transition: opacity 0.2s ease;
            }

            /* Modifier */
            .btn--size-l {
                    padding: 16px 56px;
            }

            .btn--size-s {
                    padding: 8px 32px;
            }

            .btn:hover {
                    opacity: 0.8;
            }

            .btn + .btn {
                    margin-left: 16px;
            }

            .btn--success {
                    background-color: #71be34;
            }

            .btn--warn {
                    background-color: #ffb702;
            }

            .btn--danger {
                    background-color: #ff623d;
            }

            .btn--disabled {
                    opacity: 0.5 !important;
                    cursor: default;
            }

            /* ======= Toast message ======== */

            #toast {
                    position: fixed;
                    top: 32px;
                    right: 32px;
                    z-index: 999999;
            }

            .toast {
                    display: flex;
                    align-items: center;
                    background-color: #fff;
                    border-radius: 2px;
                    padding: 20px 0;
                    min-width: 400px;
                    max-width: 450px;
                    border-left: 4px solid;
                    box-shadow: 0 5px 8px rgba(0, 0, 0, 0.08);
                    transition: all linear 0.3s;
            }

            @keyframes slideInLeft {
                    from {
                            opacity: 0;
                            transform: translateX(calc(100% + 32px));
                    }
                    to {
                            opacity: 1;
                            transform: translateX(0);
                    }
            }

            @keyframes fadeOut {
                    to {
                            opacity: 0;
                    }
            }

            .toast--success {
                    border-color: #47d864;
            }

            .toast--success .toast__icon {
                    color: #47d864;
            }

            .toast--info {
                    border-color: #2f86eb;
            }

            .toast--info .toast__icon {
                    color: #2f86eb;
            }

            .toast--warning {
                    border-color: #ffc021;
            }

            .toast--warning .toast__icon {
                    color: #ffc021;
            }

            .toast--error {
                    border-color: #ff623d;
            }

            .toast--error .toast__icon {
                    color: #ff623d;
            }

            .toast + .toast {
                    margin-top: 24px;
            }

            .toast__icon {
                    font-size: 24px;
            }

            .toast__icon,
            .toast__close {
                    padding: 0 16px;
            }

            .toast__body {
                    flex-grow: 1;
            }

            .toast__title {
                    font-size: 16px;
                    font-weight: 600;
                    color: #333;
            }

            .toast__msg {
                    font-size: 14px;
                    color: #888;
                    margin-top: 6px;
                    line-height: 1.5;
            }

            .toast__close {
                    font-size: 20px;
                    color: rgba(0, 0, 0, 0.3);
                    cursor: pointer;
            }

        </style>
    </head>
    <body>
        <div id="toast"></div>
        <%
        String erorr = (String) request.getAttribute("erorr")+"";
        String action = (String) request.getAttribute("action")+"";
        if(!erorr.trim().equals("null")){
        %>
        <input hidden=""  id="error"  value="<%=erorr%>"/>
        <%}%>
        <div class="container <%if(action.trim().equals("register")){%> right-panel-active <%}%>" id="container">

            <div class="form-container register-container">
                <form id="form-1" class="form" action="forget" method="post">
                    <h1>Quên tài khoản
                        <input hidden="" name="action" value="register" />

                        <div class="form-group">
                            <input id="email" name="email" type="email" placeholder="Email" required="" class="form-control">
                            <span class="form-message"></span>
                        </div><!-- comment -->

                        <div class="form-group">
                            <div style="display: flex" class="Captcha dang-ky-wrapper">
                                <div class="column" style="margin-top: 4%; margin-left: 15%;">
                                    <img style="border-radius: 10px;" id="captcha-image1" src="Captcha" alt="Captcha Image">
                                </div>
                                <button id="refresh-captcha1" style=" position: inherit;
                                        border-radius: 0px;
                                        border: 0px solid #4bb6b7;
                                        background-color: #4bb6b7;
                                        color: #fff;
                                        font-size: 24px;
                                        size: 20%;
                                        font-weight: 0;
                                        margin: 0px;
                                        padding: 0px 0px;
                                        letter-spacing: 0px;
                                        text-transform: capitalize;
                                        transition: 0.3s ease-in-out;
                                        background-color: white;
                                        border: 0px solid #fff;
                                        margin-left: 6px;
                                        color: black;
                                        margin-right: 6px;
                                        " class="refresh-captcha"><i class="fas fa-sync-alt"></i></button>
                                <div style="size: 50%;"  class="column dang-ky">
                                    <input style="font-size: 14px; padding-right: 2px;"  name="captcha" id="captcha-input" type="text" placeholder="Nhập Captcha" class="form-control"
                                           required="">
                                </div>

                            </div>
                        </div>
                        <button>Lấy Lại Tài Khoản</button>


                </form>
            </div>

            <div class="form-container login-container ">
                <form id="form-2" class="form" action="forget" method="post">
                    <input hidden="" name="action" value="login" />
                    <h1>Quên mật khẩu.</h1>
                    <div class="form-group">
                        <input id="username" name="name" type="text" placeholder="Tên đăng nhập" required="" class="form-control">
                        <span class="form-message"></span>
                    </div><!-- comment -->

                    <div class="form-group">
                        <div style="display: flex" class="Captcha dang-ky-wrapper">
                            <div class="column" style="margin-top: 4%; margin-left: 15%;">
                                <img style="border-radius: 10px;" id="captcha-image" src="Captcha" alt="Captcha Image">
                            </div>
                            <button id="refresh-captcha" style=" position: inherit;
                                    border-radius: 0px;
                                    border: 0px solid #4bb6b7;
                                    background-color: #4bb6b7;
                                    color: #fff;
                                    font-size: 24px;
                                    size: 20%;
                                    font-weight: 0;
                                    margin: 0px;
                                    padding: 0px 0px;
                                    letter-spacing: 0px;
                                    text-transform: capitalize;
                                    transition: 0.3s ease-in-out;
                                    background-color: white;
                                    border: 0px solid #fff;
                                    margin-left: 6px;
                                    color: black;
                                    margin-right: 6px;
                                    " class="refresh-captcha"><i class="fas fa-sync-alt"></i></button>
                            <div style="size: 50%;"  class="column dang-ky">
                                <input style="font-size: 14px; padding-right: 2px;"  name="captcha" id="captcha-input" type="text" placeholder="Nhập Captcha" class="form-control"
                                       required="">
                            </div>

                        </div>
                    </div>
                    <div class="content">
                        <div class="pass-link">
                            <a href="login?action=register">Đăng Ký</a>
                        </div>
                        <div class="pass-link">
                            <a href="login.jsp">Đăng nhập?</a>
                        </div>
                    </div>
                    <button>Lấy Lại Mật Khẩu</button>
                    <a href="login.jsp"> <span>hoặc bấm vào <strong><u >đây</u></strong> để tạo tài khoản mới</span> </a>


                </form>
            </div>

            <div class="overlay-container">
                <div class="overlay">
                    <div class="overlay-panel overlay-left">
                        <h1 class="title">Xin chào</h1>
                        <p>Nếu bạn quên mật khẩu, vui lòng bấm quên mật khẩu tại đây.</p>
                        <button class="ghost" id="login">Quên Mật Khẩu
                            <i class="lni lni-arrow-left login"></i>
                        </button>
                    </div>
                    <div class="overlay-panel overlay-right">
                        <h1 class="title">Xin chào<h1>
                                <p>Nếu bạn quên tài khoản, vui lòng bấm quên tài khoản tại đây</p>
                                <button class="ghost" id="register">Quên Tài Khoản
                                    <i class="lni lni-arrow-right register"></i>
                                </button>
                                </div>
                                </div>
                                </div>

                                </div>
                                <script src="login.js"></script>
                                <script src="message.js"></script>
                                <script>


                                    Validator({
                                            form: "#form-1",
                                            errrorSelector: ".form-message",
                                            rules: [
                                                    Validator.isRequired("#username"),
                                                    Validator.isEmail("#email"),
                                                    Validator.isPassword('#password'),
                                            ]
                                    });
                                    Validator({
                                            form: "#form-2",
                                            errrorSelector: ".form-message",
                                            rules: [

                                                    Validator.isRequired("#username"),
                                                    Validator.isPassword('#password')
                                            ]
                                    });
                                    const registerButton = document.getElementById("register");
                                    const loginButton = document.getElementById("login");
                                    const container = document.getElementById("container");

                                    registerButton.addEventListener("click", () => {
                                            container.classList.add("right-panel-active");
                                    });

                                    loginButton.addEventListener("click", () => {
                                            container.classList.remove("right-panel-active");
                                    });
                                    function showErrorToast(error) {
                                            toast({
                                                    title: "Thông báo",
                                                    message: error,
                                                    type: "error",
                                                    duration: 5000
                                            });
                                    }
                                    const error = document.getElementById("error").value;
                                    console.log(error);
                                    if (!error == "") {
                                            console.log(error);
                                            const myTimeout = setTimeout(showErrorToast(error), 1000)
                                    }
                                </script>

                                </body>
                                </html>
