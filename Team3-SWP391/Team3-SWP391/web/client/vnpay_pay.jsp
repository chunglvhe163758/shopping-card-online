<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.User" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Tạo mới đơn hàng</title>
        <!-- Bootstrap core CSS -->
        <link href="/vnpay_jsp/assets/bootstrap.min.css" rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link href="/vnpay_jsp/assets/jumbotron-narrow.css" rel="stylesheet">      
        <script src="/vnpay_jsp/assets/jquery-1.11.3.min.js"></script>
    </head>
     <jsp:include page="header.jsp"></jsp:include>
      <%Object obj = session.getAttribute("account");
    User u = null;
    if(obj !=null) u = (User)obj;
    %>
    <body>
       
         <div class="container">
           <div class="header clearfix">

                <h3 class="text-muted">VNPAY DEMO</h3>
            </div>
            <h3>Nạp tiền</h3>
            <div class="table-responsive">
                <form action="ajaxSeverlet" id="frmCreateOrder" method="post">    
                     <%if(u == null){
        %>
        <input hidden="" type="text" name="account" value="<%=0%>"/>
        <%}else{%>
        <input hidden="" type="text" name="account" value="<%=u.getId()%>"/>
        <%}%>
                    <div class="form-group">
                        <label for="amount">Số tiền</label>
                        <input class="form-control" data-val="true" data-val-number="The field Amount must be a number." data-val-required="The Amount field is required." id="amount" max="100000000" min="1" name="amount" type="number" value="10000" />
                    </div>
                    <button type="submit" class="btn btn-default" href>Nạp tiền</button>
                 
                        <h4>Ngân hàng hỗ trợ</h4>
                        <img style="size: 90%;" src="../img/bank.png" alt="alt"/>
                    
                    <div class="form-group">
                      
                        <input type="radio" Checked="True" id="bankCode" name="bankCode" value="">
                     
                       
                       
                        
                       
                       
                       
                       
                    </div>
                    <div class="form-group">
                        
                        <input hidden="" type="text" id="language" Checked="True" name="language" value="vn">
                         
                         
                    </div>
                    
                </form>
            </div>
            <p>
                &nbsp;
            </p>
            <footer class="footer">
                <p>&copy; VNPAY 2020</p>
            </footer>
        </div>
          
        <link href="https://pay.vnpay.vn/lib/vnpay/vnpay.css" rel="stylesheet" />
        <script src="https://pay.vnpay.vn/lib/vnpay/vnpay.min.js"></script>
        <script type="text/javascript">
            $("#frmCreateOrder").submit(function () {
                var postData = $("#frmCreateOrder").serialize();
                var submitUrl = $("#frmCreateOrder").attr("action");
                $.ajax({
                    type: "POST",
                    url: submitUrl,
                    data: postData,
                    dataType: 'JSON',
                    success: function (x) {
                        if (x.code === '00') {
                            if (window.vnpay) {
                                vnpay.open({width: 768, height: 600, url: x.data});
                            } else {
                                location.href = x.data;
                            }
                            return false;
                        } else {
                            alert(x.Message);
                        }
                    }
                });
                return false;
            });
        </script>       
    </body>
</html>