<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : user
    Created on : Jun 21, 2023, 8:35:25 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/storage.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Manager Stock</title>
    </head>
    <body>
        <div class="container-xl">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-5">
                                <h2>Stock <b>Management</b></h2>
                            </div>
                            <div class="col-sm-7">
                                <a href="#" class="btn btn-secondary"><i class="material-icons"></i> <span>Add New Supplier</span></a>
                                <a href="#" class="btn btn-secondary"><i class="material-icons"></i> <span>Export to Excel</span></a>						
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>CODE NUMBER</th>						
                                <th>SERI NUMBER</th>						
                                <th>DATE CREATED</th>
                                <th>STATUS</th>
                                <th>EXPIRY</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="s" items="${storage}">
                                <tr>
                                    <td>${s.id}</td>
                                    <td>${s.code}</td>
                                    <td>${s.seri}</td>                        
                                    <td>${s.createdAt}</td>
                                    <td>
                                        <c:if test="${s.isStatus() == false}"><span class="status text-danger">•</span> Failed</c:if>
                                        <c:if test="${s.isStatus() == true}"><span class="status text-success">•</span> Success</c:if>
                                    </td>
                                        <td>${s.exp}</td>
                                    <td>
                                        <a href="#" class="settings" title="Settings" data-toggle="tooltip"><i class="material-icons"></i></a>
                                        <a href="#" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons"></i></a>
                                    </td>
                                </tr>       
                            </c:forEach>            
                        </tbody>
                    </table>
                    <div class="clearfix">
                        <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                        <ul class="pagination">
                            <li class="page-item disabled"><a href="#">Previous</a></li>
                            <li class="page-item active"><a href="#" class="page-link">1</a></li>
                            <li class="page-item"><a href="#" class="page-link">2</a></li>
                            <li class="page-item"><a href="#" class="page-link">3</a></li>
                            <li class="page-item"><a href="#" class="page-link">4</a></li>
                            <li class="page-item"><a href="#" class="page-link">5</a></li>
                            <li class="page-item"><a href="#" class="page-link">Next</a></li>
                        </ul>
                    </div>
                </div>
                <a href="admin"><button type="button" class="btn btn-primary">Trang chủ</button>
            </div>
        </div> 
    </body>
</html>
