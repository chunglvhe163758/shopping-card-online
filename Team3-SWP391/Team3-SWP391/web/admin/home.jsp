<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : index
    Created on : May 27, 2023, 11:07:37 PM
    Author     : hieub
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Managements</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <style>
            .main-search-input {
                background: #fff;
                padding: 0 120px 0 0;
                border-radius: 1px;
                margin-top: -25px;
                box-shadow: 0px 0px 0px 6px rgba(255,255,255,0.3);
            }

            .fl-wrap {
                float: left;
                width: 100%;
                position: relative;
            }

            .main-search-input:before {
                content: '';
                position: absolute;
                bottom: -40px;
                width: 50px;
                height: 1px;
                background: rgba(255,255,255,0.41);
                left: 50%;
                margin-left: -25px;
            }

            .main-search-input-item {
                float: left;
                width: 100%;
                box-sizing: border-box;
                border-right: 1px solid #eee;
                height: 100%;
                position: relative;
            }

            .main-search-input-item input:first-child {
                border-radius: 100%;
            }

            .main-search-input-item input {
                float: left;
                border: none;
                width: 100%;
                height: 50px;
                padding-left: 20px;
            }

            .main-search-button{
                background: #009d63;
            }

            .main-search-button {
                position: absolute;
                right: 0px;
                height: 50px;
                width: 120px;
                color: #fff;
                top: 0;
                border: none;
                border-top-right-radius: 0px;
                border-bottom-right-radius: 0px;
                cursor: pointer;
            }

            .main-search-input-wrap {
                max-width: 500px;
                margin: 20px auto;
                position: relative;
            }

            :focus {
                outline: 0;
            }


            @media only screen and (max-width: 768px){
                .main-search-input {
                    background: rgba(255,255,255,0.2);
                    padding: 14px 20px 10px;
                    border-radius: 10px;
                    box-shadow: 0px 0px 0px 10px rgba(255,255,255,0.0);
                }

                .main-search-input-item {
                    width: 100%;
                    border: 1px solid #eee;
                    height: 50px;
                    border: none;
                    margin-bottom: 10px;
                }

                .main-search-input-item input {
                    border-radius: 6px !important;
                    background: #fff;
                }
                .main-search-button {
                    position: relative;
                    float: left;
                    width: 100%;
                    border-radius: 6px;
                }
            }
            a.active{
                color: black;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <div class="bg-white" id="sidebar-wrapper">
                <div class="sidebar-heading text-center py-4 primary-text fs-4 fw-bold text-uppercase border-bottom"><i
                        class="fas fa-user-secret me-2"></i>Admin</div>
                <div class="list-group list-group-flush my-3">
                    <a href="admin" class="list-group-item list-group-item-action bg-transparent second-text active"><i 
                            class="fa-solid fa-house me-2"></i>Trang Chủ</a>
                    <a href="manageruser" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i 
                            class="fa-sharp fa-solid fa-users-gear me-2"></i>Người dùng</a>
                    <a href="chartImport" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i 
                            class="fa-solid fa-chart-simple me-2"></i>Thông kê</a>
                    <a href="managerstorage" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i 
                            class="fa-solid fa-database me-2"></i>Kho hàng</a>
                    <a href="managerorder" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i
                            class="fas fa-shopping-cart me-2"></i>Ðơn hàng</a>
                    <a href="manager" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i
                            class="fas fa-gift me-2"></i>Sản phẩm</a>
                    <a href="#" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i 
                            class="fa-brands fa-cc-paypal me-2"></i>Thanh toán</a>
                    <a href="logout" class="list-group-item list-group-item-action bg-transparent text-danger fw-bold"><i
                            class="fas fa-power-off me-2"></i>Đăng xuất</a>
                </div>
            </div>
            <div id="page-content-wrapper">
                <nav class="navbar navbar-expand-lg navbar-light bg-transparent py-4 px-4">
                    <div class="d-flex align-items-center">
                        <i class="fa-solid fa-bars primary-text fs-4 me-3" id="menu-toggle"></i>
                    </div>

                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle second-text fw-bold" href="#" id="navbarDropdown"
                                   role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-user me-2"></i>Admin
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="changeprofile">Change Profile</a></li>
                                    <li><a class="dropdown-item" href="logout">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div class="container-fluid px-4">
                    <div class="row g-3 my-2">
                        <div class="col-md-3">
                            <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">                               
                                <div>
                                    <p class="fs-5">PRODUCTS ON SALE</p>
                                    <h6 class="fs-2">${pcount}</h6>
                                </div>
                                <i class="fas fa-gift fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                                <div>
                                    <p class="fs-5">INCOME</p>
                                    <h3 class="fs-2"><%= request.getAttribute("tcount") != null 
                                            ? String.format("%,d", request.getAttribute("tcount")) 
                                            : 0 %> $</h3>
                                </div>
                                <i
                                    class="fa-solid fa-sack-dollar fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                                <div>
                                    <p class="fs-5">ORDERS AMOUNT</p>
                                    <h3 class="fs-2">${ocount}</h3>
                                </div>
                                <i class="fa-solid fa-cart-plus fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                                <div>
                                    <p class="fs-5">USER USING</p>
                                    <h3 class="fs-2">${ucount}</h3>
                                </div>
                                <i class="fa-solid fa-users fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                            </div>
                        </div>
                    </div>
                    <div style="display: flex">        
                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        <div
                            id="myChart" style="width:100%; max-width:600px; height:500px;">
                        </div>
                        <script>
                            google.charts.load('current', {'packages': ['corechart']});
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                var data = google.visualization.arrayToDataTable([
                                    ['Card Type', 'Mhl'],
                                    ['Viettel', 54.8],
                                    ['Vinaphone', 48.6],
                                    ['Vietnamobile', 44.4],
                                    ['Mobifone', 23.9],
                                    ['GMoblie', 14.5]
                                ]);

                                var options = {
                                    title: 'Revenue Per Supplier By Day',
                                    is3D: true
                                };

                                var chart = new google.visualization.PieChart(document.getElementById('myChart'));
                                chart.draw(data, options);
                            }
                        </script>
                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        <div id="myChart1" style="width:100%; max-width:600px; height:500px; "></div>
                        <script>
                            google.charts.load('current', {'packages': ['corechart']});
                            google.charts.setOnLoadCallback(drawChart1);

                            function drawChart1() {
                                var data = google.visualization.arrayToDataTable([
                                    ['Card Type', 'number'],
                                    ['Viettel', 55],
                                    ['Vinaphone', 49],
                                    ['Vietnamobile', 44],
                                    ['Mobifone', 24],
                                    ['GMoblie', 15]
                                ]);

                                var options = {
                                    title: 'Number Of Products Sold Per Day'
                                };

                                var chart = new google.visualization.BarChart(document.getElementById('myChart1'));
                                chart.draw(data, options);
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
        <script>
                            var el = document.getElementById("wrapper");
                            var toggleButton = document.getElementById("menu-toggle");

                            toggleButton.onclick = function () {
                                el.classList.toggle("toggled");
                            };
        </script>
    </body>
</html>
