<%-- 
    Document   : statistical
    Created on : Jun 22, 2023, 2:17:43 AM
    Author     : Admin
--%>
<%@ page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "dao.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.google.gson.Gson"%>
<%@ page import="com.google.gson.JsonObject"%>
<%@ page import="jakarta.servlet.http.HttpServlet"%>
<%@ page import="jakarta.servlet.http.HttpServletRequest"%>
<%@ page import="jakarta.servlet.http.HttpServletResponse"%>
<%@ page import="jakarta.servlet.http.HttpSession"%>



<%
Gson gsonObj = new Gson();
Map<Object,Object> map = null;
Map<Object,Object> mapName = null;

List<Map<Object,Object>> list = new ArrayList<Map<Object,Object>>();
List listName = new ArrayList<>();


int min = 50; // Minimum value of range
int max = 100; // Maximum value of range

List<Product> list2 = (List) request.getAttribute("list");
 //
for(int i = 0; i <= list2.size()-1; i++){
        map = new HashMap<Object,Object>(); 
        map.put("x", i); 
        map.put("y", list2.get(i).getQuantity()); 
        list.add(map);
        
    }

String dataPoints = gsonObj.toJson(list);

for(int j = 0; j <= list2.size()-1; j++){
        listName.add(list2.get(j).getName());
    }
String dataPointsName = gsonObj.toJson(listName);

%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>Statistical Manager</title>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/statistical.css">
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script type="text/javascript">
            window.onload = function () {
                const ctx = document.getElementById('testChart');
                new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: <%out.print(dataPointsName);%>,
                        datasets: [{
                                label: '# of Quantity',
                                data: <%out.print(dataPoints);%>,
                                borderWidth: 1
                            }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
                //

            }
        </script>
    </head>
    <body>
        <div class="grid-container">
            <main class="main-container">

                <div class="charts">

                    <div class="charts-card">
                        <p class="chart-title">Product Sales</p>
                        <div id="bar-chart"></div>
                    </div>

                    <div class="charts-card">
                        <p class="chart-title">Purchase and Sales Orders</p>
                        <div id="area-chart"></div>
                    </div>

                    <div class="charts-card">
                        <p class="chart-title">Timeline</p>
                        <canvas id="testChart"></canvas>
                    </div>
                </div>

            </main>

        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/apexcharts/3.35.3/apexcharts.min.js"></script>
        <script src="js/statistical.js"></script>
    </body>
</html>