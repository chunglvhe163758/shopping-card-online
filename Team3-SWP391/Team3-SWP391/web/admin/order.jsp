<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : order
    Created on : Jun 21, 2023, 8:25:54 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/order.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manager Order</title>
    </head>
    <body>
        <div class="container-xl">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4">
                                <h2>Order <b>Manager</b></h2>
                            </div>
                            <div class="col-sm-8">						
                                <a href="managerorder" class="btn btn-primary"><i class="material-icons"></i> <span>Refresh List</span></a>
                                <a href="#" class="btn btn-secondary"><i class="material-icons"></i> <span>Export to Excel</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="table-filter">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="show-entries">
                                    <span>Show</span>
                                    <select class="form-control">
                                        <option>5</option>
                                        <option>10</option>
                                        <option>15</option>
                                        <option>20</option>
                                    </select>
                                    <span>entries</span>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <form action="managerorder" method="get">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>                   
                                    <div class="filter-group">
                                        <label>Name</label>
                                        <input type="text" name="search" value="${searchtxt}" class="form-control" placeholder="Search Customer...">
                                    </div>                                
                                    <div class="filter-group">
                                        <label>Supplier</label>
                                        <select class="form-control" name="SupId" style="width: 68%">
                                            <option value="">All</option>
                                            <c:forEach var="s" items="${suppliers}">
                                                <option ${SupId == s.id ?"selected":""} value="${s.id}">${s.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </form>
                                <span class="filter-icon"><i class="fa fa-filter"></i></span>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Data</th>
                                <th>Product</th>
                                <th>Order Date</th>						
                                <th>Status</th>						
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="order" items="${order}">
                                <tr>
                                    <td>${order.id}</td>
                                    <td>${order.data}</td>
                                    <td>${order.name}</td>
                                    <td>${order.createdAt}</td>                        
                                    <td>
                                        <c:if test="${order.status == 'failed'}"><span class="status text-danger">•</span> Failed</c:if>
                                        <c:if test="${order.status == 'success'}"><span class="status text-success">•</span> Success</c:if>
                                        </td>
                                        <td>${order.total}</td>
                                    <td><a href="orderdetail.jsp" class="view" title="View Details" data-toggle="tooltip"><i class="material-icons"></i></a></td>
                                </tr> 
                            </c:forEach>                            
                        </tbody>
                    </table>
                    <div class="clearfix">
                        <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                        <ul class="pagination">
                            <li class="page-item disabled"><a href="#">Previous</a></li>
                            <li class="page-item active"><a href="#" class="page-link">1</a></li>
                            <li class="page-item"><a href="#" class="page-link">2</a></li>
                            <li class="page-item"><a href="#" class="page-link">3</a></li>
                            <li class="page-item"><a href="#" class="page-link">4</a></li>
                            <li class="page-item"><a href="#" class="page-link">5</a></li>
                            <li class="page-item"><a href="#" class="page-link">6</a></li>
                            <li class="page-item"><a href="#" class="page-link">7</a></li>
                            <li class="page-item"><a href="#" class="page-link">Next</a></li>
                        </ul>
                    </div>
                </div>
                <a href="admin"><button type="button" class="btn btn-primary">Trang chủ</button>
            </div> 
        </div> 
    </body>
</html>
