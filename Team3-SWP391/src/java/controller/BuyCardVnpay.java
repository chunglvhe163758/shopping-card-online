/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.OrderDAO;
import dao.StorageDAO;
import dao.TransactionDAO;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import model.Order;
import model.User;
import utils.SendEmailProduct;
import vnpay.Config;

/**
 *
 * @author hieub
 */
public class BuyCardVnpay extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BuyCardVnpay</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BuyCardVnpay at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map fields = new HashMap();
        for (Enumeration params = request.getParameterNames(); params.hasMoreElements();) {
            String fieldName = URLEncoder.encode((String) params.nextElement(), StandardCharsets.US_ASCII.toString());
            String fieldValue = URLEncoder.encode(request.getParameter(fieldName), StandardCharsets.US_ASCII.toString());
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                fields.put(fieldName, fieldValue);
            }
        }

        String vnp_SecureHash = request.getParameter("vnp_SecureHash");
        if (fields.containsKey("vnp_SecureHashType")) {
            fields.remove("vnp_SecureHashType");
        }
        if (fields.containsKey("vnp_SecureHash")) {
            fields.remove("vnp_SecureHash");
        }
        String signValue = Config.hashAllFields(fields);
        int ammount = (int) Integer.valueOf(request.getParameter("vnp_Amount")) / 100;
        TransactionDAO td = new TransactionDAO();
        String code = request.getParameter("vnp_TxnRef");
        System.out.println(code);
        if (signValue.equals(vnp_SecureHash)) {

            if ("00".equals(request.getParameter("vnp_TransactionStatus"))) {
                td.setStatus1("success", code);
                StorageDAO sd = new StorageDAO();
                OrderDAO od = new OrderDAO();
                od.setSatatusOrder(Integer.valueOf(code), "process");
//                sd.buyCardVnpay(Integer.valueOf(code));
//                Order y = od.getOrderByTran(Integer.valueOf(code));
//                SendEmailProduct sdp = new SendEmailProduct();
//                sdp.threadMail(y.getEmail(), y);
//                System.out.println("da qua day");
                try {
                    HttpSession ss = request.getSession();
                    User x = (User) ss.getAttribute("account");
                    // ss.invalidate();
                    PrintWriter out = response.getWriter();
                    int id = x.getId();
                    UserDAO ud = new UserDAO();
                    User k = ud.getUserByID(id);
                    if(id!=0){
                        ss.setAttribute("account", k);
                    }else{
                        try {
                            ss.removeAttribute("account");
                        } catch (Exception e) {
                        }
                    }
                    
                } catch (Exception e) {
                }
                request.setAttribute("money", ammount);
                response.sendRedirect("home.jsp?error=" + "process");
                
            } else {
                td.setStatus("Failed", code);
                OrderDAO od = new OrderDAO();
                od.setSatatusOrder(Integer.valueOf(code), "fail");
                response.sendRedirect("home.jsp?error=" + "Failed");
            }

        } else {
            td.setStatus("invalid signature", code);
            //out.print("invalid signature");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
