/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.OrderDAO;
import dao.ProductDAO;
import dao.StorageDAO;
import dao.TransactionAddDAO;
import dao.TransactionDAO;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;
import model.Product;
import model.Transaction;
import model.User;
import utils.SendEmailProduct;

/**
 *
 * @author hieub
 */
public class BuyCard extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BuyCard</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BuyCard at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("home.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.print(request.getParameter("mathe"));
        String ten = request.getParameter("tenthecao");
        int id = Integer.valueOf(request.getParameter("mathe"));
        int quantity = Integer.valueOf(request.getParameter("qty-32"));
        int gia = Integer.valueOf(request.getParameter("price"));
        String typeTransaction = (String) request.getParameter("paygate_code");
        String email = (String) request.getParameter("email");
        int total = gia * quantity;
        out.print(total);
        Product y = new Product();
        y.setName(ten);
        y.setId(id);
        y.setPrice(gia);
        Order od = new Order();
        od.setName(ten);
        od.setEmail(email);
        od.setPrice(gia);
        od.setTotal(total);
        od.setQuantity(quantity);
        od.setProduct(id);
        User x = null;

        TransactionDAO td = new TransactionDAO();
        StorageDAO sd = new StorageDAO();
        UserDAO u = new UserDAO();
        try {
            HttpSession ss = request.getSession();
            Object obj = ss.getAttribute("account");
            if (obj != null) {
                x = (User) obj;
            }
        } catch (Exception e) {
        }
        ProductDAO pd = new ProductDAO();
        Product d = pd.getProductById(id);
        
        if (typeTransaction.equals("vnpay") || x == null) {
            //mua khi khong dang nhap
            response.sendRedirect("paymentCard.jsp?amount=" + total + "&nameProduct=" + d.getName() + "&giaProduct=" + gia + "&idProduct=" + id + "&quantityProduct=" + quantity + "&email=" + email);
        } else {
           

                
                
//            try {
//                tad = TransactionAddDAO.getInstance("jdbc:mysql://localhost:3306/team3", "root", "");
//                t = tad.insertTransaction(x.getId(), total);
//            } catch (SQLException ex) {
//                Logger.getLogger(BuyCard.class.getName()).log(Level.SEVERE, null, ex);
//            }
              
            Transaction t  =  td.addTransaction(x.getId(), total);
            UserDAO ud = new UserDAO();
            User k = ud.getUserByID(x.getId());
            HttpSession ss = request.getSession();
            ss.setAttribute("account", k);
            System.out.println("Transaction id ="+t.getId());
            System.out.println("User id ="+x.getId());
            od.setUser(x.getId());
            od.setTransaction(t.getId());
            String s = "";
            if (t.getStatus().equals("Success")) {
                od.setStatus("process");
                s+="process";
            } else {
                od.setStatus("fail");
                s+="failed";
            }
            OrderDAO oderDAO = new OrderDAO();
            oderDAO.insertOrder(od);
            response.sendRedirect("home.jsp?error=" + s);

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
//            if (x.getWallet() > total) {
//                int order = td.buyCardTransaction(x.getId(), total);
//                User m = u.getUserByID(x.getId());
//                try {
//                    HttpSession ss = request.getSession();
//                    ss.setAttribute("account", m);
//                } catch (Exception e) {
//                }
//                out.print("order =" + order);
//                System.out.println(email);
//                sd.buyCard(quantity, y, order, email, x.getId(), total);
//                OrderDAO od = new OrderDAO();
//                Order k = od.getOrderByTran(order);
//                SendEmailProduct smd = new SendEmailProduct();
//                smd.threadMail(email, k);
//                response.sendRedirect("home.jsp?");
//            } else {
//                response.sendRedirect("home.jsp?erorr=notmoney");
//                //tra lai khi tai khoan khong du tien
//            }
