/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import dao.TransactionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.text.NumberFormat;
import java.util.List;
import model.Transaction;

/**
 *
 * @author hieub
 */
public class HistoryTransaction extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HistoryTransaction</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HistoryTransaction at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int page = Integer.valueOf(request.getParameter("page"));
        int ghi = Integer.valueOf(request.getParameter("ghi"));
        int totalGhi = Integer.valueOf(request.getParameter("totalGhi"));
        String id = request.getParameter("id");
        String moneyFrom = (String) request.getParameter("moneyFrom");
        String moneyTo = (String) request.getParameter("moneyTo");
        String timeFrom = (String) request.getParameter("timeFrom");
        String timeTo = (String) request.getParameter("timeTo");
        String transac = (String) request.getParameter("transac");
        String detail = (String) request.getParameter("content");
        String method = (String) request.getParameter("status");
        String type = (String) request.getParameter("type");
        Gson gson = new Gson();
        TransactionDAO td = new TransactionDAO();
        int page1 = (page - 1) * ghi;
        List<Transaction> list = td.getTransactionByUser( ghi, page1,transac,timeFrom,timeTo,moneyFrom,moneyTo,detail,method,type,id);
        PrintWriter out = response.getWriter();
        String str = "";
        if(list.size()== 0){
            str = "<tr>Khong co giao dich nao</tr>";
        }else{
        int dem = page1;
        NumberFormat formatter = NumberFormat.getNumberInstance();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getId());
            dem += 1;
            String s = "";
            s.trim();
            if (list.get(i).getStatus().equalsIgnoreCase("success")) {
                s += "<tr>"
                        + "                                                <td>" + dem + "</td>\n"
                        + "                                                    <td colspan=\"2\">\n"
                        + "                                                        <h6>" + list.get(i).getId() + "</h6>\n"
                        + "                                                    </td>\n"
                        + "<td colspan=\"2\">" + formatter.format(list.get(i).getTotal()) + "</td>"
                        + "                                                <td colspan=\"2\">" + list.get(i).getCreatedAt() + "<br></td>\n"
                        + "<td>" + list.get(i).getContent() + "</td>"
                        + "                                                <td style=\"color: green\" class=\"font-weight-bold\">Thành công</td>\n"
                        + "\n"
                        + "                                            ";
                if (list.get(i).isType() == true) {
                    s += "<td>+</td> </tr>";
                } else {
                    s += "<td>-</td> </tr>";
                }
            } else {
                s += "<tr>\n"
                        + "                                                <td>" + dem + "</td>\n"
                        + "                                                    <td colspan=\"2\">\n"
                        + "                                                        <h6>" + list.get(i).getId() + "</h6>\n"
                        + "                                                    </td>\n"
                        + "<td colspan=\"2\">" + formatter.format(list.get(i).getTotal()) + "</td>"
                        + "                                                <td colspan=\"2\">" + list.get(i).getCreatedAt() + "<br></td>\n"
                        + "<td>" + list.get(i).getContent() + "</td>"
                        + "                                                <td class=\"font-weight-bold text-danger\">Thất bại</td>\n"
                        + "\n"
                        + "                                            ";
                if (list.get(i).isType() == true) {
                    s += "<td>+</td> </tr>";
                } else {
                    s += "<td>-</td> </tr>";
                }
            }
            str += s;
        }
    }
        
        out.println(str);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
