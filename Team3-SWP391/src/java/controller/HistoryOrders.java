/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.google.gson.Gson;
import dao.OrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Array;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import model.Order;

/**
 *
 * @author hieub
 */
public class HistoryOrders extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HistoryOrders</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HistoryOrders at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        int page = Integer.valueOf(request.getParameter("page"));
        int ghi = Integer.valueOf(request.getParameter("ghi"));
        int totalGhi = Integer.valueOf(request.getParameter("totalGhi"));
        String id = request.getParameter("id");
        String price = (String) request.getParameter("price");
         int page1 = (page - 1) * ghi;
        String timeFrom = (String) request.getParameter("timeFrom");
        String timeTo = (String) request.getParameter("timeTo");
        String name = (String) request.getParameter("name");
        String status = (String) request.getParameter("status");
        String user = (String) request.getParameter("user");
        Gson gson = new Gson();
        OrderDAO od = new OrderDAO();
        List<Order> lst = new ArrayList<>();
        lst = od.getListOrderPage(String.valueOf(ghi), String.valueOf(page1), id, price, timeFrom, timeTo, name, status, user);
        PrintWriter out = response.getWriter();
        String str = "";
        if(lst.size()== 0){
            str = "<tr>Khong co giao dich nao</tr>";
        }else{
        int dem = page1;
        NumberFormat formatter = NumberFormat.getNumberInstance();
            for (int i = 0; i < lst.size(); i++) {
                 dem++;
                str+="<tr>\n" +
"                                   <td>"+dem+"</td>\n" +
"                                    <td colspan=\"2\" style=\"width: 18%;\">"+lst.get(i).getId()+"</td>\n" +
"                                    <td style=\"width: 15%;\">"+formatter.format(lst.get(i).getPrice())+"</td>\n" +
"                                    <td colspan=\"2\" style=\"width: 10%;\">"+lst.get(i).getCreatedAt()+"</td>\n" +
"                                    <td>"+lst.get(i).getName()+"</td>\n" +
"                                    <td>"+lst.get(i).getStatus()+"</td>\n" +
"                                    <td>"+lst.get(i).getQuantity()+"</td>\n" +
"                                    <td><button value=\""+lst.get(i).getId()+"\" onclick=\"showValue(this)\">Chi tiết</button></td>\n" +
"                                </tr>";
               
            }
        }
        out.print(str);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
