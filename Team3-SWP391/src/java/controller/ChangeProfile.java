/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.User;
import utils.Encrypt;

/**
 *
 * @author hieub
 */
@WebServlet(name = "changeProfile", urlPatterns = {"/client/changeProfile"})
public class ChangeProfile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet changeProfile</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet changeProfile at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("profile.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        //kiem tra form doi thong tin hay doi mat khau
        if (action.equals("infor")) {
            String phone = request.getParameter("phone");
            String name = request.getParameter("fullname");
            boolean check = false;
            HttpSession ss = request.getSession();
            User x = new User();
            UserDAO ud = new UserDAO();
            User user = null;
            try {
                user = (User) ss.getAttribute("account");
                x.setId(user.getId());
                x.setFullName(name);
                x.setPhone(phone);
                
            } catch (Exception e) {
            }
            User u = new User();
            out.print(x.getFullName());
            if (!phone.equals(user.getPhone()) || !name.equals(user.getFullName())) {
                ud.updateUser(x);
                int id = x.getId();
                x = ud.getUserByID(id);
                out.print("hieuueuuu");
                ss.setAttribute("account", x);
                ss.setMaxInactiveInterval(60000);
                request.setAttribute("erorr", "Cập nhật thông tin thành công");
                request.getRequestDispatcher("profile.jsp").forward(request, response);
            } else {
                 request.setAttribute("erorr", "Vui lòng thay đổi thông tin khi cập nhật");
                out.println("okee");
                request.getRequestDispatcher("profile.jsp").forward(request, response);
            }
            //ket thuc doi thong tin
        } else if (action.equals("pass")) {
            String pass = request.getParameter("oldpass");
            String newpass = request.getParameter("password");
            Encrypt ec = new Encrypt();
            UserDAO ud = new UserDAO();
            HttpSession ss = request.getSession();
            String password = ec.toSHA1(pass); //ma hoa mat khau cu khi nguoi dung nhap
            String newpassEc = ec.toSHA1(newpass); //ma hoa mat khau moi
            int id = 0;
            String email = null;
            try {
                User user = (User) ss.getAttribute("account");
                id = user.getId();
                email = user.getEmail();
            } catch (Exception e) {
            }
            String passDb = ud.checkPassById(id);
            if (passDb.equals(password)) {
                ud.updateUserPass(email, newpassEc);
                request.setAttribute("erorr", "Cập nhật mật khẩu thành công");
                request.getRequestDispatcher("profile.jsp").forward(request, response);
            } else {
                request.setAttribute("erorr", "Mật khẩu cũ không đúng");

                request.getRequestDispatcher("profile.jsp").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
