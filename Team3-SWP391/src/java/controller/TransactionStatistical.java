/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package admin.controller;

import dao.TransactionDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import model.Transaction;

/**
 *
 * @author Admin
 */
@WebServlet(name = "TransactionStatistical", urlPatterns = {"/admin/transactionstatistical"})
public class TransactionStatistical extends HttpServlet {

    public Date getCurrentDate() {
        LocalDate curDate = java.time.LocalDate.now();
        return Date.valueOf(curDate.toString());
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        TransactionDAO transactionDAO = new TransactionDAO();
        List<Transaction> transactions = transactionDAO.getAllTotalList();
        int count1 = transactionDAO.countTypeTransactionByTotal();
        int count0 = transactionDAO.countTypeTransactionByTotal1();
        request.setAttribute("count1", count1);
        request.setAttribute("count0", count0);

        String date = request.getParameter("date");

        if (date == null) {
            date = getCurrentDate().toString();
        }

        request.setAttribute("date", date);

        double chartDonateSuccess = transactionDAO.getMoneyByTypeAnDateAndStatus(1, date, "success");
        double chartBuySuccess = transactionDAO.getMoneyByTypeAnDateAndStatus(0, date, "success");
        double chartDonateFaild = transactionDAO.getMoneyByTypeAnDateAndStatus(1, date, "Failed");
        double chartBuyFaild = transactionDAO.getMoneyByTypeAnDateAndStatus(0, date, "Failed");

        request.setAttribute("chartDonateSuccess", chartDonateSuccess);
        request.setAttribute("chartBuySuccess", chartBuySuccess);
        request.setAttribute("chartDonateFaild", chartDonateFaild);
        request.setAttribute("chartBuyFaild", chartBuyFaild);

        request.setAttribute("transactions", transactions);
        request.getRequestDispatcher("transactionstatistical.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
