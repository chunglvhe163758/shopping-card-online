/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.mysql.cj.xdevapi.Row;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Sheet;
/**
 *
 * @author hieub
 */
@WebServlet("/admin/UploadServlet")
public class UploadServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        InputStream inputStream = null; // Đối tượng để đọc dữ liệu từ tệp Excel tải lên
        Connection connection = null; // Đối tượng để kết nối tới cơ sở dữ liệu
        PreparedStatement statement = null; // Đối tượng để thực hiện truy vấn SQL

        // Lấy tệp Excel tải lên từ phần định danh "file"
        Part filePart = request.getPart("file");
        if (filePart != null) {
            try {
                // Tạo đối tượng InputStream để đọc dữ liệu từ tệp Excel tải lên
                inputStream = filePart.getInputStream();

                // Tạo một đối tượng Workbook từ InputStream
                Workbook workbook = WorkbookFactory.create(inputStream);

                // Lấy trang tính đầu tiên từ Workbook
                Sheet sheet = workbook.getSheetAt(0);

                // Lặp qua các hàng trong trang tính và chèn dữ liệu vào cơ sở dữ liệu
                for (org.apache.poi.ss.usermodel.Row row : sheet) {
                    // Bỏ qua hàng đầu tiên (tiêu đề)
                    if (row.getRowNum() == 0) {
                        continue;
                    }
                    
                    // Lấy dữ liệu từ các ô trong hàng
                    String name = row.getCell(0).getStringCellValue();
                    String phone = row.getCell(1).getStringCellValue();
                    System.out.println(name + phone);
                    // Kết nối tới cơ sở dữ liệu
                    connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/hieu", "root", "");

                    // Chuẩn bị câu truy vấn SQL để chèn dữ liệu vào bảng "mytable"
                    String sql = "INSERT INTO card (code, seri) VALUES (?, ?)";
                    statement= connection.prepareStatement(sql);
                    statement.setString(1, name);
                    statement.setString(2, phone);

                    // Thực hiện câu truy vấn SQL
                    statement.executeUpdate();

                    // Đóng kết nối và đối tượng PreparedStatement
                    statement.close();
                    connection.close();
                }

                // Đóng đối tượng Workbook và InputStream
                workbook.close();
                inputStream.close();

                // Trả về mã HTTP 200 để cho phép yêu cầu Ajax thành công
                response.setStatus(HttpServletResponse.SC_OK);
            } catch (Exception e) {
                // Nếu có lỗi xảy ra, trả về mã HTTP 500 để cho phép yêu cầu Ajax thất bại
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                e.printStackTrace();
            }
        }
    }

}



