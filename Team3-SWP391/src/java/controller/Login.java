/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.User;
import utils.Cookiee;
import utils.Encrypt;
import utils.SendEmail;

/**
 *
 * @author hieub
 */
@WebServlet(urlPatterns = {"/client/login"})
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific erorr occurs
     * @throws IOException if an I/O erorr occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific erorr occurs
     * @throws IOException if an I/O erorr occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession ss = request.getSession();
        try {
            ss.invalidate();
        } catch (Exception e) {
        }
        String action = "";
        try {
            action = request.getParameter("action");
        } catch (Exception e) {
        }
        
        request.setAttribute("action", action);
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific erorr occurs
     * @throws IOException if an I/O erorr occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String action = "", name = "", email = "", pass = "", captcha = "";
        try {
            action = request.getParameter("action");
        } catch (Exception e) {
            response.sendRedirect(request.getContextPath() + "/client/login.jsp");
        }
        //nh?n action neu action là register thì dang ký tkoan nguoc la se dang nhap
        name = request.getParameter("name");
        request.setAttribute("name", name);
        pass = request.getParameter("password");
        captcha = request.getParameter("captcha");
        try {
            email = request.getParameter("email");
            request.setAttribute("email", email);

        } catch (Exception e) {
        }
        SendEmail sd = new SendEmail();
        UserDAO u = new UserDAO();
        Encrypt ec = new Encrypt();
        //check captcha
        String captchas = "1";
        try {

            HttpSession ss = request.getSession();
            captchas = ss.getAttribute("captchas").toString();
            ss.removeAttribute("captchas");
        } catch (Exception e) {
            response.sendRedirect(request.getContextPath() + "/client/login.jsp");
        }
        //ham check captcha xem nhap captcha dung khong
        if (captcha.equals(captchas)) {
            //nhap dung captcha
            if (action.equals("register")) {
                //check xem dang nhap hay dang ky
                request.setAttribute("action", "register");
                email = request.getParameter("email");
                name = request.getParameter("name");
                if (u.isUserByEmail(email)) {
                    //check tai khoan da ton tai hay chua
                    request.setAttribute("erorr", "Email đã tồn tại.");
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                } else if (u.isUserByName(name)) {
                    //check ten dang nhap da ton tai hay chua
                    request.setAttribute("erorr", "Tên đăng nhập đã tồn tại.");
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                } else {
                    //tat ca deu oke bat dau di them tai khoan
                    User x = new User();
                    x.setName(name);
                    x.setEmail(email);
                    String password = ec.toSHA1(pass);
                    x.setPassword(password);
                    u.insertUser(x);
                    String otp = ec.generateOtp(4);
                    sd.threadMail(email, "Xác thực tài khoản tại Autothecao!", otp);
                    HttpSession ss = request.getSession();
                    try {
                        ss.removeAttribute("otp");
                    } catch (Exception e) {
                    }
                    //gui mail
                    //gui mail thanh cong chuyen den xac thuc cua Hieu
                    ss.setAttribute("otp", otp);
                    request.setAttribute("email", email);
                    request.setAttribute("erorr", "Vui lòng kiểm tra mã OTP được gửi về gmail của bạn!");
                    request.getRequestDispatcher("verification.jsp").forward(request, response);
                }
                //dang nhap
            } else if (action.equals("login")) {
                //neu khach hang chon dang nhap
                request.setAttribute("action", "login");
                //ten dang nhap khong ton tai
                out.print("code chay qua 137");
                String password = ec.toSHA1(pass);
                out.print(name + pass);
                Cookiee c = new Cookiee();
                c.addCookie(request, response, name, pass);
                User us = u.checkPassword(name, password);
                if (us.getId() == 0) {
                    //khach hang khong ton tai tra ve trang login
                    out.print("code chay qua 140");
                    request.setAttribute("erorr", "Vui lòng kiểm tra lại tên đăng nhập hoặc mật  khẩu.");
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                } else if (us.isStatus()) {
                    //khach hang chua kich hoat tai khoan chuyen sang trang kich hoat
                    out.print("code chay qua 143");
                    HttpSession ss = request.getSession();
                    ss.setAttribute("account", us);
                    ss.setMaxInactiveInterval(60000);
                    try {
                        ss.removeAttribute("otp");
                    } catch (Exception e) {
                    }
                    request.setAttribute("erorr", "xin chao quay lai");
                    response.sendRedirect("home.jsp");
                } else {
                    out.print("code chay qua 146");
                    String otp = ec.generateOtp(4);
                    sd.threadMail(us.getEmail(),  "Xác thực tài khoản tại Autothecao!", otp);
                    //String status = sd.sendEmail("fpt.autothecao@gmail.com", "gyiyhediihgarcvt", us.getEmail(), "Xác thực tài khoản tại Autothecao!", otp);
                    HttpSession ss = request.getSession();
                    try {

                        ss.removeAttribute("otp");
                    } catch (Exception e) {
                    }                    
                        ss.setAttribute("otp", otp);
                        request.setAttribute("email", us.getEmail());
                        request.setAttribute("erorr", "Vui lòng kiểm tra mã OTP được gửi về gmail của bạn!");
                        request.getRequestDispatcher("verification.jsp").forward(request, response);                  
                    }

                }
                //ten dang nhap ton tai nhung phai check mat khau va type           
        } else {          
            request.setAttribute("erorr", "Mã captcha không dúng");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
        //da check xong captcha tien hanh check dang ky va dang nhap

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
