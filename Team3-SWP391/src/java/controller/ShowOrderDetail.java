/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dao.OrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Data;
import model.Order;

/**
 *
 * @author hieub
 */
@WebServlet(name="ShowOrderDetail", urlPatterns={"/ShowOrderDetail"})
public class ShowOrderDetail extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShowOrderDetail</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShowOrderDetail at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String id = (String) request.getParameter("id");
        OrderDAO od = new OrderDAO();
        Order x = od.getOrderById(Integer.valueOf(id));
        String s = "";
        s+=" <div class=\"title\">Chi tiết đơn hàng</div>\n" +
"                    <div class=\"info\">\n" +
"                        <div class=\"row\">\n" +
"                            <div class=\"col-7\">\n" +
"                                <span id=\"heading\">Date</span>\n" +
"                                <span id=\"details\">"+x.getCreatedAt()+"</span>\n" +
"                            </div>\n" +
"                            <div class=\"col-5\">\n" +
"                                <span id=\"heading\">Mã đơn hàng</span>\n" +
"                                <span id=\"details\">"+x.getId()+"</span>\n" +
"                            </div>\n" +
"                        </div>\n" +
"                    </div>\n" +
"                    <div class=\"pricing\">\n" +
"                        <div class=\"row\">\n" +
"                            <div class=\"col-9\">\n" +
"                                <span >"+x.getName()+"</span>\n" +
"                            </div>\n" +
"                            <div class=\"col-3\">\n" +
"                                <span >"+x.getPrice()+"</span>\n" +
"                            </div>\n" +
"                        </div>\n" +
"                        <div class=\"row\">\n" +
"                            <div class=\"col-9\">\n" +
"                                <span >Số lượng</span>\n" +
"                            </div>\n" +
"                            <div class=\"col-3\">\n" +
"                                <span>"+x.getQuantity()+"</span>\n" +
"                            </div>\n" +
"                        </div>\n" +
"                    </div>\n" +
"                    <div class=\"total\">\n" +
"                        <div class=\"row\">\n" +
"                            <div class=\"col-9\"></div>\n" +
"                            <div class=\"col-3\"></div>\n" +
"                        </div>\n" +
"                        <div class=\"tracking\">\n" +
"                            <div class=\"title\">Chi tiết đơn hàng</div>\n" +
"                        </div>\n" +
"                        <br>\n" +
"                        <div class=\"progress-track\">\n" ;
        if(x.getStatus().equals("fail")){
            s+="<div>Không có sản phẩm</div>";
        }else{
            

                    Gson gson = new Gson();
        List<Data> data = gson.fromJson(x.getData(), new TypeToken<List<Data>>() {
        }.getType());
        String str = "";
        for (Data d : data) {
            s +=            "                            <div class=\"col-md-12\">\n" +
"                                <div class=\"row\">\n" +
"                                    <div class=\"col-md-6\">\n" +
"                                        <span >Code:</span>\n" +
"                                        <span >"+d.getCode()+"</span>\n" +
"                                    </div>\n" +
"                                    <div class=\"col-md-6\">\n" +
"                                        <span >Seri:</span>\n" +
"                                        <span >"+d.getSeri()+"</span>\n" +                                     
"                                    </div>\n" +
"                                </div>\n" +
"                            </div>\n" ;
        }
            

            
                    s+=
"\n" +
"                        </div>\n" +
"\n" +
"                    </div>";
        }
        PrintWriter out = response.getWriter();
        out.print(s);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
