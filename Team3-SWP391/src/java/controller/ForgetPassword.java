/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import utils.Encrypt;
import utils.SendEmail;

/**
 *
 * @author hieub
 */
@WebServlet(name = "forgetPassword", urlPatterns = {"/client/forget"})
public class ForgetPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific"erorr occurs
     * @throws IOException if an I/O"erorr occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet forgetPassword</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet forgetPassword at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific"erorr occurs
     * @throws IOException if an I/O"erorr occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("erorr", "B?n không có quy?n truy c?p du?ng d?n này");
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific"erorr occurs
     * @throws IOException if an I/O"erorr occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "", captcha = "", captchas = "";
        SendEmail sd = new SendEmail();
        UserDAO u = new UserDAO();
        action = request.getParameter("action");
        HttpSession ss = request.getSession();
        try {           
            captchas = (String) ss.getAttribute("captchas");
        } catch (Exception e) {
            request.setAttribute("erorr", "Bạn không có quyền truy cập đường dẫn này.");
            request.getRequestDispatcher("forgetPassword.jsp").forward(request, response);
        }
        captcha = request.getParameter("captcha");
        if (!captcha.equals(captchas)) {
            if(action.equals("login")){
                request.setAttribute("action", "ok");
            }
            request.setAttribute("erorr", "Mã captcha không chính xác, vui lòng nhập lại!");
            request.getRequestDispatcher("forgetPassword.jsp").forward(request, response);
        } else {
            String email = "", name = "";
            
            if (action.equals("register")) {
                email = request.getParameter("email");
                String user = u.getNameByEmail(email);
                if(!user.equals("")){
                    sd.threadMail(email, "Lấy lại tài khoản tại Autothecao!", user);
                //String status = sd.sendEmail("fpt.autothecao@gmail.com", "gyiyhediihgarcvt", email, "Xác thực tài khoản tại Autothecao!", user);
                request.setAttribute("erorr", "Mail đã được gửi. Vui lòng kiểm tra hòm thư của bạn.");
                
                ss.invalidate();
                request.getRequestDispatcher("login.jsp").forward(request, response);}
                else{
                    request.setAttribute("erorr", "Email của bạn không tồn tại.");
                    request.getRequestDispatcher("forgetPassword.jsp").forward(request, response);
                }
            } else if (action.equals("login")) {
                request.setAttribute("action", "ok");
                name = request.getParameter("name");
                email = u.getEmailByName(name);
                Encrypt ec = new Encrypt();
                String otp = ec.generateOtp(4);
                request.setAttribute("email", email);               
                //String status = sd.sendEmail("fpt.autothecao@gmail.com", "gyiyhediihgarcvt", email, "Xác thực tài khoản tại Autothecao!", otp);
              
                if(email.equals("")){
                    request.setAttribute("erorr", "Tên đăng nhập không tồn tại.");
                    request.getRequestDispatcher("forgetPassword.jsp").forward(request, response);
                }else{
                try {
                    ss.removeAttribute("otp");
                    request.removeAttribute("email");
                } catch (Exception e) {
                }            
                    sd.threadMail(email, "OTP lấy lại mật khẩu tại Autothecao!", otp);
                    ss.setAttribute("otp", otp);
                    request.setAttribute("status", "forget");                    
                    request.setAttribute("email", email);
                    request.setAttribute("erorr", "Mail đã được gửi. Vui lòng kiểm tra hòm thư của bạn.");
                    request.getRequestDispatcher("verification.jsp").forward(request, response);               
                }
//            request.setAttribute("erorr", "Ðã g?i thu d?n email c?a b?n vui lòng ki?m tra");
//            request.getRequestDispatcher("verification.jsp").forward(request, response);
            } else {
                request.setAttribute("erorr", "L?i vui lòng nh?p l?i");
                request.getRequestDispatcher("forgetPassword.jsp").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
