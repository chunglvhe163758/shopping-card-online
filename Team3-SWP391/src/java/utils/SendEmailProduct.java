/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dao.OrderDAO;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.Data;
import model.Order;
import model.Storage;

/**
 *
 * @author hieub
 */
public class SendEmailProduct {

    public String sendEmail(String from, String password, String to, Order o) {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        // String from = "";
        //create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }

        };
        String text = "";
        Session session = Session.getInstance(props, auth);

        //gui email
        // final String to = "hieubgjm@gmail.com";
        MimeMessage msg = new MimeMessage(session);
        String noidung = "<!DOCTYPE html>\n"
                + "<html lang=\"en\">\n"
                + "\n"
                + "<head>\n"
                + "    <meta charset=\"UTF-8\" />\n"
                + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n"
                + "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
                + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n"
                + "    <title>Document</title>\n"
                + "    <link href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" rel=\"stylesheet\" />\n"
                + "    <style>\n"
                + "        body {\n"
                + "            background: #ddd3;\n"
                + "            height: 100vh;\n"
                + "            vertical-align: middle;\n"
                + "            display: flex;\n"
                + "            font-family: Muli;\n"
                + "            font-size: 14px;\n"
                + "        }\n"
                + "\n"
                + "        .card {\n"
                + "            margin: auto;\n"
                + "            width: 38%;\n"
                + "            max-width: 600px;\n"
                + "            padding: 4vh 0;\n"
                + "            box-shadow: 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n"
                + "            border-top: 3px solid rgb(252, 103, 49);\n"
                + "            border-bottom: 3px solid rgb(252, 103, 49);\n"
                + "            border-left: none;\n"
                + "            border-right: none;\n"
                + "        }\n"
                + "\n"
                + "        @media (max-width: 768px) {\n"
                + "            .card {\n"
                + "                width: 90%;\n"
                + "            }\n"
                + "        }\n"
                + "\n"
                + "        .title {\n"
                + "            color: rgb(252, 103, 49);\n"
                + "            font-weight: 600;\n"
                + "            margin-bottom: 2vh;\n"
                + "            padding: 0 8%;\n"
                + "            font-size: initial;\n"
                + "        }\n"
                + "\n"
                + "        #details {\n"
                + "            font-weight: 400;\n"
                + "        }\n"
                + "\n"
                + "        .info {\n"
                + "            padding: 5% 8%;\n"
                + "        }\n"
                + "\n"
                + "        .info .col-5 {\n"
                + "            padding: 0;\n"
                + "        }\n"
                + "\n"
                + "        #heading {\n"
                + "            color: grey;\n"
                + "            line-height: 6vh;\n"
                + "        }\n"
                + "\n"
                + "        .pricing {\n"
                + "            background-color: #ddd3;\n"
                + "            padding: 2vh 8%;\n"
                + "            font-weight: 400;\n"
                + "            line-height: 2.5;\n"
                + "        }\n"
                + "\n"
                + "        .pricing .col-3 {\n"
                + "            padding: 0;\n"
                + "        }\n"
                + "\n"
                + "        .total {\n"
                + "            padding: 2vh 8%;\n"
                + "            color: rgb(252, 103, 49);\n"
                + "            font-weight: bold;\n"
                + "        }\n"
                + "\n"
                + "        .total .col-3 {\n"
                + "            padding: 0;\n"
                + "        }\n"
                + "\n"
                + "        .footer {\n"
                + "            padding: 0 8%;\n"
                + "            font-size: x-small;\n"
                + "            color: black;\n"
                + "        }\n"
                + "\n"
                + "        .footer img {\n"
                + "            height: 5vh;\n"
                + "            opacity: 0.2;\n"
                + "        }\n"
                + "\n"
                + "        .footer a {\n"
                + "            color: rgb(252, 103, 49);\n"
                + "        }\n"
                + "\n"
                + "        .footer .col-10,\n"
                + "        .col-2 {\n"
                + "            display: flex;\n"
                + "            padding: 3vh 0 0;\n"
                + "            align-items: center;\n"
                + "        }\n"
                + "\n"
                + "        .footer .row {\n"
                + "            margin: 0;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar {\n"
                + "            margin-bottom: 3vh;\n"
                + "            overflow: hidden;\n"
                + "            color: rgb(252, 103, 49);\n"
                + "            padding-left: 0px;\n"
                + "            margin-top: 3vh;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar li {\n"
                + "            list-style-type: none;\n"
                + "            font-size: x-small;\n"
                + "            width: 25%;\n"
                + "            float: left;\n"
                + "            position: relative;\n"
                + "            font-weight: 400;\n"
                + "            color: rgb(160, 159, 159);\n"
                + "        }\n"
                + "\n"
                + "        #progressbar #step1:before {\n"
                + "            content: \"\";\n"
                + "            color: rgb(252, 103, 49);\n"
                + "            width: 5px;\n"
                + "            height: 5px;\n"
                + "            margin-left: 0px !important;\n"
                + "            /* padding-left: 11px !important */\n"
                + "        }\n"
                + "\n"
                + "        #progressbar #step2:before {\n"
                + "            content: \"\";\n"
                + "            color: #fff;\n"
                + "            width: 5px;\n"
                + "            height: 5px;\n"
                + "            margin-left: 32%;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar #step3:before {\n"
                + "            content: \"\";\n"
                + "            color: #fff;\n"
                + "            width: 5px;\n"
                + "            height: 5px;\n"
                + "            margin-right: 32%;\n"
                + "            /* padding-right: 11px !important */\n"
                + "        }\n"
                + "\n"
                + "        #progressbar #step4:before {\n"
                + "            content: \"\";\n"
                + "            color: #fff;\n"
                + "            width: 5px;\n"
                + "            height: 5px;\n"
                + "            margin-right: 0px !important;\n"
                + "            /* padding-right: 11px !important */\n"
                + "        }\n"
                + "\n"
                + "        #progressbar li:before {\n"
                + "            line-height: 29px;\n"
                + "            display: block;\n"
                + "            font-size: 12px;\n"
                + "            background: #ddd;\n"
                + "            border-radius: 50%;\n"
                + "            margin: auto;\n"
                + "            z-index: -1;\n"
                + "            margin-bottom: 1vh;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar li:after {\n"
                + "            content: \"\";\n"
                + "            height: 2px;\n"
                + "            background: #ddd;\n"
                + "            position: absolute;\n"
                + "            left: 0%;\n"
                + "            right: 0%;\n"
                + "            margin-bottom: 2vh;\n"
                + "            top: 1px;\n"
                + "            z-index: 1;\n"
                + "        }\n"
                + "\n"
                + "        .progress-track {\n"
                + "            padding: 0 8%;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar li:nth-child(2):after {\n"
                + "            margin-right: auto;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar li:nth-child(1):after {\n"
                + "            margin: auto;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar li:nth-child(3):after {\n"
                + "            float: left;\n"
                + "            width: 68%;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar li:nth-child(4):after {\n"
                + "            margin-left: auto;\n"
                + "            width: 132%;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar li.active {\n"
                + "            color: black;\n"
                + "        }\n"
                + "\n"
                + "        #progressbar li.active:before,\n"
                + "        #progressbar li.active:after {\n"
                + "            background: rgb(252, 103, 49);\n"
                + "        }\n"
                + "    </style>\n"
                + "</head>\n"
                + "\n"
                + "<body>\n"
                + "    <div class=\"card\">\n"
                + "        <div class=\"title\">Chi tiết đơn hàng</div>\n"
                + "        <div class=\"info\">\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-7\">\n"
                + "                    <span id=\"heading\">Date</span><br />\n"
                + "                    <span id=\"details\">" + o.getCreatedAt() + "</span>\n"
                + "                </div>\n"
                + "                <div class=\"col-5 pull-right\">\n"
                + "                    <span id=\"heading\">Mã đơn hàng</span><br />\n"
                + "                    <span id=\"details\">" + o.getId() + "</span>\n"
                + "                </div>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "        <div class=\"pricing\">\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-9\">\n"
                + "                    <span id=\"name\">" + o.getName() + "</span>\n"
                + "                </div>\n"
                + "                <div class=\"col-3\">\n"
                + "                    <span id=\"price\">" + (int) o.getPrice() + "</span>\n"
                + "                </div>\n"
                + "            </div>\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-9\">\n"
                + "                    <span id=\"name\">Số lượng</span>\n"
                + "                </div>\n"
                + "                <div class=\"col-3\">\n"
                + "                    <span id=\"price\">" + o.getQuantity() + "</span>\n"
                + "                </div>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "        <div class=\"total\">\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-9\"></div>\n"
                + "                <div class=\"col-3\"><big>" + (int) o.getTotal() + " vnd</big></div>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "        <div class=\"tracking\">\n"
                + "            <div class=\"title\">Chi tiết mã thẻ</div>\n"
                + "        </div>\n"
                + "        <div class=\"progress-track\">\n"
                + "            <table border=\"1\" cellpadding=\"10\" style=\"width: 90vw; max-width: 370px;\">\n"
                + "            <thead>\n"
                + "              <tr>\n"
                + "                <th>Seri</th>\n"
                + "                <th>Code</th>\n"
                + "<th>Ngày hết hạn</th>"
                + "              </tr>\n"
                + "            </thead>\n"
                + "            <tbody>";

        Gson gson = new Gson();
        List<Data> data = gson.fromJson(o.getData(), new TypeToken<List<Data>>() {
        }.getType());
        String str = "";
        for (Data d : data) {
            str += "<tr>"
                    + "<td>" + d.getSeri() + "</td>"
                    + "<td>" + d.getCode() + "</td>"
                    + "<td>" + d.getExp() + "</td>"
                    + "</tr>";
        }

        String footer = "        </tbody> </table> </div>\n"
                + "\n"
                + "\n"
                + "        <div class=\"footer\">\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-2\">\n"
                + "                    <img class=\"img-fluid\" src=\"https://i.imgur.com/YBWc55P.png\" />\n"
                + "                </div>\n"
                + "                <div class=\"col-10\">\n"
                + "                    Want any help? Please &nbsp;<a> contact us</a>\n"
                + "                </div>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "    </div>\n"
                + "</body>\n"
                + "\n"
                + "</html>";
        noidung += str;
        noidung += footer;
        System.out.println(noidung);
        String content = "Đơn hàng số " + o.getId() + " tại Autothecao";
        try {
            // msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.setFrom(from);
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            msg.setSubject(content, "utf-8");
            msg.setContent(noidung, "text/HTML; charset=UTF-8");
            //gui email
            Transport.send(msg);
        } catch (Exception e) {
            return "fail";
        }
        return "success";
    }

    public void sendEmail(String from, String password, String to) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        // String from = "";
        //create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }

        };
        String text = "";
        Session session = Session.getInstance(props, auth);

        //gui email
        // final String to = "hieubgjm@gmail.com";
        MimeMessage msg = new MimeMessage(session);
        String noidung = "";

        try {
            // msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.setFrom(from);
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            msg.setSubject("Ðon hang 1990 tai Auto the cao", "utf-8");
            msg.setContent(noidung, "text/HTML; charset=UTF-8");
            //gui email
            Transport.send(msg);
        } catch (Exception e) {

        }
    }

    public void threadMail(String to, Order o) {
        Thread a = new Thread(new Runnable() {
            @Override
            public void run() {
                sendEmail("fpt.autothecao@gmail.com", "gyiyhediihgarcvt", to, o);
            }
        });
        a.start();
    }

//    public static void main(String[] args) {
//        OrderDAO od = new OrderDAO();
//        Order x = od.getOrderById(14);
//        SendEmailProduct sd = new SendEmailProduct();
//        sd.threadMail("hieubgjm@gmail.com", x);
//    }
}
