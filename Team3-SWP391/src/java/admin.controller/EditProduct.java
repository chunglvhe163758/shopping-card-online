package admin.controller;

import dao.ProductDAO;
import dao.SupplierDAO;
import dao.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Product;
import model.Supplier;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "EditProduct", urlPatterns = {"/admin/edit"})
public class EditProduct extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idP = Integer.parseInt(request.getParameter("pid"));
        Product product = new ProductDAO().getProductById(idP);
        List<Supplier> listSuppliers = new SupplierDAO().getAll();
        request.setAttribute("product", product);
        request.setAttribute("listSuppliers", listSuppliers);
        request.getRequestDispatcher("/admin/editproducts.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession ss = request.getSession();
        User user = null;
        // dùng để test
        if (ss.getAttribute("account") == null) {
            user = new UserDAO().getUserByID(4);
        } else {
            user = (User) ss.getAttribute("account");
        }
//        user = (User) ss.getAttribute("account");
        Product product = new Product();
        product.setUpdatedBy(user.getEmail());
        product.setId(Integer.parseInt(request.getParameter("id")));
        product.setKey(request.getParameter("key"));
        product.setName(request.getParameter("name"));
        product.setImage(request.getParameter("image"));
        product.setPrice(Double.parseDouble(
                request.getParameter("price")));
        product.setDescribe(request.getParameter("description"));
        product.setSupplier(Integer.parseInt(
                request.getParameter("supplier")));
        product.setProfit(Integer.parseInt(
                request.getParameter("profit")));
        product.setQuantity(Integer.parseInt(
                request.getParameter("quantity")));

        ProductDAO dao = new ProductDAO();

        dao.editProduct(product);
        response.sendRedirect("/autothecao/admin/manager");
    }

}
