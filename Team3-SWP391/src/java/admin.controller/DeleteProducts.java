/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package admin.controller;

import dao.ProductDAO;
import dao.UserDAO;
import java.io.IOException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Arrays;
import model.Product;
import model.Supplier;
import model.User;

@WebServlet(name = "DeleteProducts", urlPatterns = {"/deleteproducts"})
public class DeleteProducts extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("pid") != null) {
            String pid = request.getParameter("pid");
            ProductDAO pd = new ProductDAO();
            pd.deleteProduct(pid);

        }
        // reset laptop roi dung dong output cua apache minh moi debug dc
        if (request.getParameterValues("itemIds") != null) {
            String[] itemIds = request.getParameterValues("itemIds");

            if (itemIds != null && itemIds.length > 0 && request.getParameter("delete") != null) {
                // Chuyển đổi mảng chuỗi sang mảng số nguyên
                int[] ids = Arrays.stream(itemIds).mapToInt(Integer::parseInt).toArray();
                // Xóa các bản ghi được chọn từ cơ sở dữ liệu
                HttpSession ss = request.getSession();
                User user = null;

                if (ss.getAttribute("account") == null) {
                    user = new UserDAO().getUserByID(4);
                    System.out.println("4");
                } else {
                    user = (User) ss.getAttribute("account");
                    System.out.println("3");
                }
//                user = (User) ss.getAttribute("account");
                try {
                    System.out.println(user == null);
                    ProductDAO productDAO = new ProductDAO();
                    productDAO.deleteMultichoiceProducts(ids, user.getEmail());
                } catch (Exception e) {
                    // Xử lý lỗi
                    e.printStackTrace();
                    request.setAttribute("errorMessage", "An error occurred while deleting expenses.");
                    request.getRequestDispatcher("error.jsp").forward(request, response);
                }
            }
        } // Lấy danh sách các ID được chọn

        response.sendRedirect("/autothecao/admin/manager");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Chuyển hướng đến trang thành công
        //response.sendRedirect("/autothecao/admin/manager");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
