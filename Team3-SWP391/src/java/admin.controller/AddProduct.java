/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package admin.controller;

import dao.ProductDAO;
import dao.SupplierDAO;
import dao.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Product;
import model.Supplier;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "AddProduct", urlPatterns = {"/admin/addProduct"})
public class AddProduct extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Supplier> listSuppliers = new SupplierDAO().getAll();
        request.setAttribute("listSuppliers", listSuppliers);
        request.getRequestDispatcher("/admin/AddProduct.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession ss = request.getSession();
        User user = null;
        // dùng để test
        if (ss.getAttribute("account") == null) {
            user = new UserDAO().getUserByID(4);
        } else {
            user = (User) ss.getAttribute("account");
        }
        Product product = new Product();
        product.setCreatedBy(user.getEmail());
        product.setKey(request.getParameter("key"));
        product.setName(request.getParameter("name"));
        product.setImage(request.getParameter("image"));
        product.setDescribe(request.getParameter("describe"));
        product.setPrice(Double.parseDouble(request.getParameter("price")));
        product.setProfit(Integer.parseInt(request.getParameter("profit")));
        product.setSupplier(Integer.parseInt(request.getParameter("supplier")));
        product.setQuantity(Integer.parseInt(request.getParameter("quantity")));

        new ProductDAO().addProduct(product);
        response.sendRedirect("/autothecao/admin/manager");
    }

}