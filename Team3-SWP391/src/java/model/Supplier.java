/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author hieub
 */
public class Supplier extends StatusObj{
    private String name;
    private String logo;
    private String describe;
    private boolean status;
    private int id;
    private String createdBy;
    public Supplier() {
    }

    public Supplier(boolean isDeleted,String createdBy, Timestamp createdAt, Timestamp updatedAt, String updatedBy, Timestamp deletedAt, String deletedBy,String name, String logo, String describe, boolean status, int id) {
        super(isDeleted, createdAt, updatedAt, updatedBy, deletedAt, deletedBy);
        this.name = name;
        this.logo = logo;
        this.describe = describe;
        this.status = status;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
}
