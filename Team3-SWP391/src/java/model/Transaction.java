/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author hieub
 */
public class Transaction extends StatusObj{
    private String name;
    private boolean type;
    private double total;
    private String status;
    private String content;
    private int user;
    private boolean  process;
    private int id;

    public Transaction() {
    }

    public Transaction( boolean isDeleted, Timestamp createdAt, Timestamp updatedAt, String updatedBy, Timestamp deletedAt, String deletedBy,String name, boolean type, double total, String status, String content, int user, boolean process, int id) {
        super(isDeleted, createdAt, updatedAt, updatedBy, deletedAt, deletedBy);
        this.name = name;
        this.type = type;
        this.total = total;
        this.status = status;
        this.content = content;
        this.user = user;
        this.process = process;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public boolean isProcess() {
        return process;
    }

    public void setProcess(boolean process) {
        this.process = process;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    
}
