/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.util.ArrayList;
import java.util.List;
import model.Supplier;

public class SupplierDAO extends MyDAO {
    public int countTypeTransactionByTotal() {
        xSql = "SELECT SUM(total) FROM `transaction` WHERE type = 1;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int countTypeTransactionByTotal1() {
        xSql = "SELECT SUM(total) FROM `transaction` WHERE type = 0;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public List<Supplier> getAll() {
        List<Supplier> lst = new ArrayList<>();
        xSql = "SELECT * FROM supplier WHERE isDeleted = 0 ;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Supplier supplier = new Supplier();
                supplier.setName(rs.getString("name"));
                supplier.setId(rs.getInt("id"));
                lst.add(supplier);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lst;
    }
//    public static void main(String[] args) {
//        SupplierDAO sd = new SupplierDAO();
//        List<Supplier> sp = new ArrayList<>();
//        sp = sd.getAll();
//        System.out.println(sp.size());
//    }
            
}