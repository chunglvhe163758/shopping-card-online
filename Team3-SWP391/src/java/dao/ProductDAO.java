/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import model.Product;
import model.Supplier;

/**
 *
 * @author hieub
 */
public class ProductDAO extends MyDAO {
        public int countStorageById() {
        xSql = "SELECT COUNT(id) FROM `storage` WHERE status = 1;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public String getNameById(int id){
        String name = "";
        xSql = "select name from `product` where id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {                
                name = rs.getString(1);
            }
            rs.close();
                    ps.close();
        } catch (Exception e) {
        }

        return name;
    }
     public List<Product> viettelQuantityList() {
        List<Product> list = new ArrayList<>();
        xSql = "SELECT * FROM `product` WHERE name LIKE 'viettel%'";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setName(rs.getString("name"));
                product.setQuantity(rs.getInt("quantity"));
                list.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    //search products manager
    public List<Product> searchByName(String txtSearch) {
        List<Product> lst = new ArrayList<>();
        xSql = "select * from product\n"
                + "where name like ?;";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + txtSearch + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setPrice(rs.getDouble("price"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                lst.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lst;
    }

    //get quantities from different categories
    public List<Product> getAllQuantityList() {
        List<Product> list = new ArrayList<>();
        xSql = "SELECT SUM(product.quantity) as quantity, supplier.name as name\n"
                + "FROM product\n"
                + "INNER JOIN supplier ON product.supplier = supplier.id\n"
                + "GROUP BY supplier.name;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setName(rs.getString("name"));
                product.setQuantity(rs.getInt("quantity"));
                list.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    //payging product
    public List<Product> payging(int index) {
        List<Product> list = new ArrayList<>();
        xSql = "SELECT * FROM `product` where isDeleted = 0 ORDER BY id LIMIT 8 OFFSET ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, (index - 1) * 8);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setImage(rs.getString("image"));
                product.setKey(rs.getString("key"));
                product.setPrice(rs.getDouble("price"));
                product.setQuantity(rs.getInt("quantity"));
                list.add(product);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int countProductById() {
        xSql = "SELECT COUNT(id) FROM `product`;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    // crud product
    public void addProduct(Product product) {
        xSql = "INSERT INTO team3.`product`"
                + " (isDeleted, createdBy, createdAt,"
                + "  `key`, name, image, `describe`,"
                + " price, profit, supplier, quantity) VALUES\n"
                + "(0,?,now(),?,?,?,?,?,?,?,?) ";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, product.getCreatedBy() != null ? product.getCreatedBy() : "admin");
            ps.setString(2, product.getKey());
            ps.setString(3, product.getName());
            ps.setString(4, product.getImage());
            ps.setString(5, product.getDescribe());
            ps.setDouble(6, product.getPrice());
            ps.setInt(7, product.getProfit());
            ps.setInt(8, product.getSupplier());
            ps.setInt(9, product.getQuantity());
            ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println(ex);
        }
        System.out.println(xSql);
    }

    public void editProduct(Product product) {
        Product x = null;
        xSql = "UPDATE `product`\n"
                + "SET\n"
                + "updatedAt = now(),\n"
                + "updatedBy = ?,\n"
                + "key =?,\n"
                + "name =?,\n"
                + "image =?,\n"
                + "describe = ?,\n"
                + "price = ?,\n"
                + "profit = ?,\n"
                + "supplier =?,\n"
                + "quantity = ?\n"
                + "WHERE id = ?;";
        try {
            ps = con.prepareStatement(xSql);

            ps.setString(1, product.getUpdatedBy());
            ps.setString(2, product.getKey());
            ps.setString(3, product.getName());
            ps.setString(4, product.getImage());
            ps.setString(5, product.getDescribe());
            System.out.println(product.getDescribe());
            ps.setDouble(6, product.getPrice());
            ps.setInt(7, product.getProfit());
            ps.setInt(8, product.getSupplier());
            ps.setInt(9, product.getQuantity());
            ps.setInt(10, product.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
public void deleteMultichoiceProducts(int[] ids, String nameUser) {
        try {
//            // ví dụ mảng ids = [1,2,4,5] thì sau placeholders nó trả về 1,2,4,5
            String placeholders = Arrays.stream(ids)
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining(","));
//
//            String listIdOrder = "";
//
//            for (Integer integer : new OrderDAO().getIdOrderByProduct(ids)) {
//                listIdOrder += integer + ",";
//            }
//            String sql1 = " DELETE FROM team3.`order` WHERE product IN (" + placeholders + "); ";
//            String sql2 = " DELETE FROM team3.`storage` WHERE product IN (" + placeholders + "); ";
//            String sql3 = " DELETE FROM team3.`product` WHERE id IN (" + placeholders + "); ";
//            PreparedStatement ps1;
//            if (!listIdOrder.equals("")) {
//                listIdOrder = listIdOrder.substring(0, listIdOrder.length() - 1);
//                String sql = "DELETE FROM team3.`storage` WHERE order_id IN ( " + listIdOrder + " ); ";
//                ps1 = con.prepareStatement(sql);
//                ps1.executeUpdate();
//            }
//            ps1 = con.prepareStatement(sql1);
//            ps1.executeUpdate();
//            ps1 = con.prepareStatement(sql2);
//            ps1.executeUpdate();
//            ps1 = con.prepareStatement(sql3);
//            ps1.executeUpdate();

            String sql1 = "UPDATE team3.`product`\n"
                    + "SET\n"
                    + "deletedBy = ?,\n"
                    + "isDeleted = 1,\n"
                    + "deletedAt = now()\n"
                    + "WHERE id IN (" + placeholders + ");";
            PreparedStatement ps1 = con.prepareStatement(sql1);
            ps1.setString(1, nameUser);
            ps1.executeUpdate();
System.out.println(sql1);
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
           
        }
        
    }


    public void deleteProduct(int id) {
        try {
            String sql1 = "DELETE FROM team3.`storage` WHERE product = ?";
            String sql2 = "DELETE FROM team3.`product` WHERE id = ?";
            PreparedStatement ps1 = con.prepareStatement(sql1);
            PreparedStatement ps2 = con.prepareStatement(sql2);
            ps1.setInt(1, id);
            ps2.setInt(1, id);
            ps1.executeUpdate();
            ps2.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Product getProductById(int id) {
        Product x = null;
        try {
            xSql = "select * from product where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Product(rs.getBoolean("isDeleted"), rs.getString("createdBy"),
                        rs.getTimestamp("createdAt"), rs.getTimestamp("updatedAt"), rs.getString("updatedBy"),
                        rs.getTimestamp("deletedAt"), rs.getString("deletedBy"), rs.getString("key"), rs.getString("name"),
                        rs.getString("image"), rs.getString("describe"), rs.getDouble("price"), rs.getInt("profit"),
                        rs.getInt("supplier"), rs.getInt("quantity"), id);

            }
            ps.close();
        } catch (Exception e) {
        }
        return x;
    }
public List<Integer> getPriceOfProduct(){
    xSql = "SELECT DISTINCT price from product";
    List<Integer> numbers = new ArrayList<>();
    try {
       ps = con.prepareStatement(xSql);
       rs = ps.executeQuery();
        while (rs.next()) {  
          int i = rs.getInt(1);
          numbers.add(i);
           
        }
    } catch (Exception e) {
    }
    return numbers;
}    
    
public List<Supplier> getAllSuppliers(){
    List<Supplier> lst = new ArrayList<>();
    xSql = "Select name,id from supplier";
    try {
        ps = con.prepareStatement(xSql);
        rs = ps.executeQuery();
        while (rs.next()) {            
            Supplier x = new Supplier();
            x.setName(rs.getString(1));
            x.setId(rs.getInt(2));
            lst.add(x);
        }
        rs.close();
        ps.close();
    } catch (Exception e) {
    }
    return lst;
}


    public void insertProduct(String name, double price, String describe, int supplier) {
        xSql = "INSERT INTO product (name, price, `describe`, supplier)\n"
                + "VALUES (?,?,?,?);";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            ps.setString(1, name);
            ps.setDouble(2, price);
            ps.setString(3, describe);
            ps.setInt(4, supplier);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public List<Product> getAllProduct() {
        List<Product> lst = new ArrayList<>();
        xSql = "select * from product where isDeleted = 0";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product products = new Product();
                products.setId(rs.getInt("id"));
                products.setName(rs.getString("name"));
//                products.setImg(rs.getString("img"));
                products.setPrice(rs.getDouble("price"));
                lst.add(products);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Collections.sort(lst, (o1, o2) -> {
            int p1 = (int) o1.getPrice();
            int p2 = (int) o2.getPrice();
            if (p1 > p2) {
                return 1;
            } else if (p1 < p2) {
                return -1;
            } else {
                return 0;
            } // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/LambdaBody
        });
        return lst;
    }
    ///delete product

    public void deleteProduct(String pid) {
        Product x = new Product();
        xSql = "UPDATE product\n"
                + "SET isDeleted = 1,updatedAt = '" + x.getTime() + "'"
                + "WHERE id = " + pid;
        try {
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }

    public void editProduct(String name, String image, Double price,
            String title, String describe, int supplier) {
        Product x = null;
        xSql = "update product\n"
                + "set [name] = ?,\n"
                + "[image] = ?,\n"
                + "price = ?,\n"
                + "title = ?,\n"
                + "[describe] = ?,\n"
                + "supplierID = ?\n"
                + "where id = ?";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            ps.setBoolean(1, false);
            ps.setString(2, "admin");
            ps.setTimestamp(3, x.getTime());
            ps.setString(4, name);
            ps.setString(5, image);
            ps.setDouble(6, price);
            ps.setString(7, title);
            ps.setString(8, describe);
            ps.setInt(9, supplier);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //count by product
   
    public List<Product> getProductBySup(int id) {
        List<Product> lst = new ArrayList<>();
        xSql = "select * from product where supplier = " + id +" and quantity > 0 and isDeleted = 0";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getBoolean(1),rs.getString(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getTimestamp(6), rs.getString(7),rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getDouble(12), rs.getInt(13), rs.getInt(14), rs.getInt(15), rs.getInt(16));
                System.out.println(p.getId());
                lst.add(p);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }

        Collections.sort(lst, (o1, o2) -> {
            int p1 = (int) o1.getPrice();
            int p2 = (int) o2.getPrice();
            if (p1 > p2) {
                return 1;
            } else if (p1 < p2) {
                return -1;
            } else {
                return 0;
            } // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/LambdaBody
        });
        return lst;
    }
    
   
 
    public List<Supplier> getAllSupplier(){
        xSql = "select * from supplier";
        List<Supplier> lst = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while(rs.next()){
                Supplier x = new Supplier(rs.getBoolean(1), rs.getString(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getTimestamp(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getBoolean(11), rs.getInt(12));
                lst.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return lst;
    }
//    public static void main(String[] args) {
//        ProductDAO pd = new ProductDAO();
//        System.out.println(pd.getPriceOfProduct());
//    }
}
