/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import com.google.gson.JsonArray;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Product;
import model.Storage;
import com.google.gson.JsonObject;
import com.mysql.cj.xdevapi.Row;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import model.Order;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.sl.usermodel.Sheet;

/**
 *
 * @author hieub
 */
public class StorageDAO extends MyDAO {

    public void add(boolean isDeleted, String createdBy, Timestamp createdAt, String id, String code, String seri, String status, String exp, String product) {
        xSql = "INSERT INTO `storage` (isDeleted, createdBy,createdAt, id, code, seri,status,exp,product)\n"
                + "VALUES (?,?,?,?,?,?,?,?,?);";
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setString(2, "admin");
            ps.setTimestamp(3, createdAt);
            ps.setString(4, id);
            ps.setString(5, code);
            ps.setString(6, seri);
            ps.setString(7, status);
            ps.setString(8, exp);
            ps.setString(9, product);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(String id) {
        xSql = "DELETE FROM `storage` WHERE id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, id);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void edit(boolean isDeleted, Timestamp updatedAt, String updatedBy, String seri, String code, String exp, String status, String product, String id) {
        xSql = "UPDATE `storage`\n"
                + "SET isDeleted = ?,updatedAt = ?,updatedBy = ? ,"
                + "seri = ? , code = ? , exp = ? ,status = ?,product = ? \n"
                + "WHERE id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, updatedAt);
            ps.setString(3, "admin");
            ps.setString(4, seri);
            ps.setString(5, code);
            ps.setString(6, exp);
            ps.setString(7, status);
            ps.setString(8, product);
            ps.setString(9, id);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Storage getStoragebyId(String id) {
        xSql = "SELECT * FROM `storage` WHERE id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Storage storage = new Storage();
                storage.setIsDeleted(false);
                storage.setUpdatedAt(rs.getTimestamp("updatedAt"));
                storage.setUpdatedBy(rs.getString("updatedBy"));
                storage.setSeri(rs.getString("seri"));
                storage.setCode(rs.getString("code"));
                storage.setExp(rs.getTimestamp("exp"));
                storage.setStatus(rs.getBoolean("status"));
                storage.setProduct(rs.getInt("product"));
                return storage;
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //
    public int countStorageByID() {
        xSql = "SELECT COUNT(*) FROM `storage`;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Storage> payging(int index) {
        List<Storage> list = new ArrayList<>();
        xSql = "SELECT * FROM `storage` ORDER BY id LIMIT 50 OFFSET ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, (index - 1) * 50);
            rs = ps.executeQuery();
            while (rs.next()) {
                Storage storage = new Storage();
                storage.setId(rs.getInt("id"));
                storage.setSeri(rs.getString("seri"));
                storage.setCode(rs.getString("code"));
                storage.setExp(rs.getTimestamp("exp"));
                storage.setStatus(rs.getBoolean("status"));
                storage.setCreatedAt(rs.getTimestamp("createdAt"));
                list.add(storage);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    //Import excel
//    public void ImportExcel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        FileInputStream inp;
//        try {
//            inp = new FileInputStream("D://XNXX.xls");
//            HSSFWorkbook wb = new HSSFWorkbook(new POIFSFileSystem(inp));
//            HSSFSheet sheet = wb.getSheetAt(0);
//
//            for (int i = 1; i < sheet.getLastRowNum(); i++) {
//                HSSFRow row = sheet.getRow(i);
//                String seri = row.getCell(0).getStringCellValue();
//                String code = row.getCell(1).getStringCellValue();
//                Timestamp exp = (Timestamp) row.getCell(2).getDateCellValue();
//                boolean status = (boolean) row.getCell(3).getNumericCellValue();
//                int product = (int) row.getCell(4).getNumericCellValue();               
//                int id = (int) row.getCell(5).getNumericCellValue();
//                Storage storage = new Storage();
//                storage.setId(id);
//                storage.setSeri(seri);
//                storage.setCode(code);
//                storage.setExp(exp);
//                storage.setProduct(product);
//                storage.setStatus(status);
//                InsertData(request, storage);
//            }
//            wb.close();
//        } catch (FileNotFoundException ex) {
//            request.setAttribute("message", ex.getMessage());
//        } catch (IOException ex) {
//            request.setAttribute("message", ex.getMessage());
//        }
//        response.sendRedirect("managerstorage");
//    }
    public void InsertData(HttpServletRequest request, Storage storage) {
        Timestamp time = storage.getTime();
        xSql = "INSERT INTO `storage` (isDeleted,createdBy,createdAt,seri,code,exp,status) VALUES (?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setString(2, "admin");
            ps.setTimestamp(3, time);
            ps.setString(4, storage.getSeri());
            ps.setString(5, storage.getCode());
            ps.setTimestamp(6, storage.getExp());
            ps.setBoolean(7, storage.isStatus());
            int kq = ps.executeUpdate();
            if (kq != 0) {
                request.setAttribute("message", "Bu'");
            } else {
                request.setAttribute("message", "Ngu");
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<Storage> getAllStorage() {
        xSql = "SELECT * FROM `storage`;";
        List<Storage> list = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Storage storage = new Storage();
                storage.setId(rs.getInt("id"));
                storage.setCode(rs.getString("code"));
                storage.setExp(rs.getTimestamp("exp"));
                storage.setStatus(rs.getBoolean("status"));
                storage.setSeri(rs.getString("seri"));
                storage.setProduct(rs.getInt("product"));
                storage.setCreatedAt(rs.getTimestamp("createdAt"));
                list.add(storage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void addStorage(Storage x) {
        xSql = "insert into storage(isDeleted,createdBy,createdAt,seri,code,exp,status,product) values (?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setString(2, "admin");
            ps.setTimestamp(3, x.getTime());
            ps.setString(4, x.getSeri());
            ps.setString(5, x.getCode());
            ps.setTimestamp(6, x.getExp());
            ps.setBoolean(7, x.isStatus());
            ps.setInt(8, x.getProduct());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }

    }

    public void buyCardVnpay(int id) {
        OrderDAO od = new OrderDAO();
        List<Storage> list = new ArrayList<>();
        Order x = od.getOrderByTran(id);
        try {
            con.setAutoCommit(false);
            xSql = "Update product SET quantity = quantity - ? Where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getQuantity());
            ps.setInt(2, x.getProduct());
            ps.executeUpdate();
            ps.close();

        } catch (Exception e) {
        }

        try {
            xSql = "SELECT * FROM storage\n"
                    + "WHERE status = 1 and product = ?\n"
                    + "LIMIT ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getProduct());
            ps.setInt(2, x.getQuantity());
            rs = ps.executeQuery();
            while (rs.next()) {
                Storage y = new Storage();
                y.setCode(rs.getString("code"));
                y.setSeri(rs.getString("seri"));
                y.setExp(rs.getTimestamp("exp"));
                list.add(y);
            }
            System.out.println(list.toString());
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        //convert sang json
        JsonArray jsonArray = new JsonArray();
        for (Storage s : list) {
            JsonObject o = new JsonObject();
            o.addProperty("seri", s.getSeri());
            o.addProperty("code", s.getCode());
            String time = s.getExp().toString();
            o.addProperty("exp", time);
            jsonArray.add(o);
        }
        String result = jsonArray.toString();
        try {
            xSql = "Update `order` SET data = ?,status='success' where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setString(1, result);
            ps.setInt(2, x.getId());
            ps.executeUpdate();
        } catch (Exception e) {
        }
        try {
            xSql = "UPDATE storage\n"
                    + "SET status = 0,order_id = ?\n"
                    + "WHERE status = 1 and product = ?\n"
                    + "LIMIT ?;";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getId());
            ps.setInt(2, x.getProduct());
            ps.setInt(3, x.getQuantity());
            ps.executeUpdate();
            ps.close();
            con.commit();
        } catch (Exception e) {
        }
    }

    public void buyCard(int quantity, Product x, int transaction, String email, int user, double total) {
        //tru so luong the hien co trong kho
        String erorr = "ok";
        List<Storage> list = new ArrayList<>();
        int order = 0;
        int soluongthe;
        try {
            con.setAutoCommit(false);
            xSql = "Update product SET quantity = quantity - ? Where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, quantity);
            ps.setInt(2, x.getId());
            ps.executeUpdate();
            ps.close();

        } catch (Exception e) {
            erorr = "fail";
        }

        try {
            xSql = "SELECT * FROM storage\n"
                    + "WHERE status = 1 and product = ?\n"
                    + "LIMIT ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getId());
            ps.setInt(2, quantity);
            rs = ps.executeQuery();
            while (rs.next()) {
                Storage y = new Storage();
                y.setCode(rs.getString("code"));
                y.setSeri(rs.getString("seri"));
                y.setExp(rs.getTimestamp("exp"));
                list.add(y);
            }
            System.out.println(list.toString());
            rs.close();
            ps.close();
        } catch (Exception e) {
            erorr = "fail";
        }
        soluongthe = list.size();
        if (soluongthe != quantity) {
            erorr = "fail";
        }
        //convert sang json
        JsonArray jsonArray = new JsonArray();
        for (Storage s : list) {
            JsonObject o = new JsonObject();
            o.addProperty("seri", s.getSeri());
            o.addProperty("code", s.getCode());
            String time = s.getExp().toString();
            o.addProperty("exp", time);
            jsonArray.add(o);
        }
        String result = jsonArray.toString();
        System.out.println(result);
        // tao mot order moi
        try {
            xSql = "insert into `order`(isDeleted,createdAt,product,price,total,name,quantity,status,data,transaction,email,user) values (?,?,?,?,?,?,?,?,?,?,?,?) ";
            ps = con.prepareStatement(xSql, Statement.RETURN_GENERATED_KEYS);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getTime());
            ps.setInt(3, x.getId());
            ps.setDouble(4, x.getPrice());
            ps.setDouble(5, total);
            ps.setString(6, x.getName());
            ps.setInt(7, quantity);
            ps.setString(8, "success");
            ps.setString(9, result);
            ps.setInt(10, transaction);
            ps.setString(11, email);
            ps.setInt(12, user);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                order = rs.getInt(1);
            }
            System.out.println("tao order");
            ps.close();
        } catch (Exception e) {
            erorr = "fail";
        }
        try {
            xSql = "UPDATE user SET wallet = wallet - " + total + " WHERE id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, user);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            erorr = "fail";
        }
        System.out.println("tao xong order");
        //cap nhat the da ban
        try {
            xSql = "Update `transaction`"
                    + "SET status = 'success' where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, transaction);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            erorr = "fail";
        }
        try {
            xSql = "UPDATE storage\n"
                    + "SET status = 0,order_id = ?\n"
                    + "WHERE status = 1 and product = ?\n"
                    + "LIMIT ?;";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, order);
            ps.setInt(2, x.getId());
            ps.setInt(3, quantity);
            ps.executeUpdate();
            ps.close();

        } catch (Exception e) {
            erorr = "fail";
        }
        try {
            if (erorr.equals("fail")) {
                con.rollback();
            } else {
                con.commit();
                con.setAutoCommit(true);
            }
        } catch (Exception e) {
        }
    }
    //lay ra ma the

    //    public List<Storage> buyCard(int product, int id, int quantity) {
    //        List<Storage> lst = new ArrayList<>();
    //        xSql = "BEGIN;\n"
    //                + "SELECT * FROM storage\n"
    //                + "WHERE status = 1 and product =" + product + "\n"
    //                + "LIMIT " + quantity + "\n"
    //                + "FOR UPDATE;\n"
    //                + "\n"
    //                + "UPDATE storage\n"
    //                + "SET status = 0,order_id = " + id + "\n"
    //                + "WHERE status = 1 and product =" + product + "\n"
    //                + "LIMIT " + quantity + ";\n"
    //                + "COMMIT ;";
    //        try {
    //            ps = con.prepareStatement(xSql);
    //            rs = ps.executeQuery();           
    //            while (rs.next()) {
    //                Storage x = new Storage();
    //                x.setSeri(rs.getString("seri"));
    //                x.setCode(rs.getString("code"));
    //                x.setExp(rs.getTimestamp("exp"));
    //                lst.add(x);
    //            }
    //            rs.close();
    //            ps.close();
    //        } catch (Exception e) {
    //        }
    //        return lst;
    //    }
//        public static void main(String[] args) {
//            Random random = new Random();
//            StorageDAO sd = new StorageDAO();
//            for (int j = 26; j < 37; j++) {
//                            for (int i = 0; i < 30; i++) {
//                int randomNumber = random.nextInt(900000000) + 100000000;
//            String num = String.valueOf(randomNumber);
//            int randomNumber1 = random.nextInt(900000000) + 100000000;
//            String num1 = String.valueOf(randomNumber1);
//            int id = random.nextInt(25) + 1;
//            Timestamp time =  Timestamp.valueOf("2029-06-13 18:47:58");
//                System.out.println(time);
//                
//            Storage y = new Storage();
//                System.out.println(y.getTime());
//                System.out.println(num +"::::"+num1);
//                System.out.println(id);
//                System.out.println(randomNumber1 +"::::"+randomNumber);
//           Storage x = new Storage(false, "hieu", y.getTime(), y.getTime(), "hieu", y.getTime(), "hieu", num1, num, time, true,0, j, 0);
//           sd.addStorage(x);
//            }
//            }
//
//           
//        }
}
