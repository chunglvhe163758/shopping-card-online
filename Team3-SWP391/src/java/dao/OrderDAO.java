/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Order;
import model.Product;
import model.Supplier;
import model.User;

/**
 *
 * @author hieub
 */
public class OrderDAO extends MyDAO {
    
   

    public List<Order> getAllTotalList() {
        SupplierDAO sp = new SupplierDAO();
        List<Supplier> lst = sp.getAll();
        List<Order> list = new ArrayList<>();
        for (int i = 0; i <= lst.size() - 1; i++) {
            xSql = "Select Sum(total) from `order` where name like '%" + lst.get(i).getName() + "%'";
            try {
                ps = con.prepareStatement(xSql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Order x = new Order();
                    x.setTotal(rs.getDouble(1));
                    x.setName(lst.get(i).getName());
                    list.add(x);
                }
                rs.close();
                ps.close();
            } catch (Exception e) {
            }
        }
        return list;
    }
//    public Order getOrderById(int id){
//        Order x = new Order();
//        xSql = "Select * from `order` where id = ?";
//        try {
//            ps = con.prepareStatement(xSql);
//            ps.setInt(1, id);
//           rs = ps.executeQuery();
//            while (rs.next()) {                
//                x.setCreatedAt(rs.getTimestamp("createdAt"));
//                x.setId(rs.getInt("id"));
//                x.setName(rs.getString("name"));
//                x.setEmail(rs.getString("email"));
//                x.setPrice(rs.getDouble("price"));
//                x.setTotal(rs.getDouble("total"));
//                x.setQuantity(rs.getInt("quantity"));
//                x.setData(rs.getString("data"));
//                x.setTransaction(rs.getInt("transaction"));
//            }
//            rs.close();
//            ps.close();
//        } catch (Exception e) {
//        }
//        return x;
//    }

    public int countOrderPage(String id, String price, String dateFrom, String dateTo, String name, String status, String user) {
        int count = 0;
        String s = "";

        s += "Select count(id) from `order` where id like '%" + id + "%'" + " and createdAt between '" + dateFrom + "' and '" + dateTo + "' and name like '%" + name + "%'"
                + "and status like '%" + status + "%' and user = " + user;
        if (price.equals("all")) {

        } else {
            s += " and price = " + price;
        }
        xSql = s;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return count;
    }

    public List<Order> getListOrderPage(String limit, String off, String id, String price, String dateFrom, String dateTo, String name, String status, String user) {
        String s = "";
        List<Order> lst = new ArrayList<>();
        s += "Select * from `order` where id like '%" + id + "%'" + " and createdAt between '" + dateFrom + "' and '" + dateTo + "' and name like '%" + name + "%'"
                + "and status like '%" + status + "%' and user = " + user;
        if (price.equals("all")) {

        } else {
            s += " and price = " + price;
        }
        s += " ORDER BY createdAt DESC\n"
                + "LIMIT " + limit + " OFFSET " + off + "";
        xSql = s;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order x = new Order();
                x.setId(rs.getInt("id"));
                x.setName(rs.getString("name"));
                x.setQuantity(rs.getInt("quantity"));
                x.setPrice(rs.getDouble("price"));
                x.setCreatedAt(rs.getTimestamp("createdAt"));
                x.setStatus(rs.getString("status"));
                lst.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        System.out.println(xSql);
        return lst;
    }

    public int countOrderByTotal() {
        xSql = "SELECT SUM(total) FROM `order`";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Order> getAllOrderByFilter(String search, String SupId) {
        List<Order> list = new ArrayList<>();
        xSql = "select * from `order` o , supplier s\n"
                + "where o.id = s.id and o.name like ?";
        if (!SupId.isEmpty()) {
            xSql = xSql + " and  o.id = " + SupId;
        }
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setCreatedAt(rs.getTimestamp("createdAt"));
                order.setStatus(rs.getString("status"));
                order.setTotal(rs.getDouble("total"));
                order.setData(rs.getString("data"));
                list.add(order);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    public List<Order> getAll() {
        List<Order> list = new ArrayList<>();
        xSql = "select * from `order` ";
        
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setCreatedAt(rs.getTimestamp("createdAt"));
                order.setStatus(rs.getString("status"));
                order.setTotal(rs.getDouble("total"));
                order.setData(rs.getString("data"));
                list.add(order);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    // delete order
//
//    public List<Order> pagingOrder(int index) {
//        List<Order> list = new ArrayList<>();
//        xSql = "SELECT * FROM `order` ORDER BY createdAt DESC LIMIT 6 OFFSET ?;";
//        try {
//            ps = con.prepareStatement(xSql);
//            ps.setInt(1, (index - 1) * 6);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                Order order = new Order();
//                order.setId(rs.getInt("id"));
//                order.setName(rs.getString("name"));
//                order.setIsDeleted(rs.getBoolean("isDeleted"));
//                order.setQuantity(rs.getInt("quantity"));
//                order.setTotal(rs.getDouble("total"));
//                order.setStatus(rs.getString("status"));
//                list.add(order);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return list;
//    }
    public ArrayList<Order> getAllOrderByName(String query, int index) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            xSql = "SELECT * FROM `order` WHERE (? is null or name like ?) ORDER BY createdAt DESC LIMIT 6 OFFSET ?;";
            ps = con.prepareStatement(xSql);
            ps.setString(1, query);
            ps.setString(2, "%" + query + "%");
            ps.setInt(3, (index - 1) * 6);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setIsDeleted(rs.getBoolean("isDeleted"));
                order.setQuantity(rs.getInt("quantity"));
                order.setTotal(rs.getDouble("total"));
                order.setStatus(rs.getString("status"));
                order.setData(rs.getString("data"));
                list.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Order getOrderById(int id) {
        xSql = "select isDeleted,createdAt,product,price,total,name,quantity,data,id,status from `order` where id = ?";
        Order x = new Order();
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                x.setIsDeleted(rs.getBoolean(1));
                x.setCreatedAt(rs.getTimestamp(2));
                x.setProduct(rs.getInt(3));
                x.setPrice(rs.getDouble(4));
                x.setTotal(rs.getDouble(5));
                x.setName(rs.getString(6));
                x.setQuantity(rs.getInt(7));
                x.setData(rs.getString(8));
                x.setId(rs.getInt(9));
                x.setStatus(rs.getString(10));
                ps.close();
            }
        } catch (Exception e) {
        }
        return x;
    }

    public Order getOrderByTran(int id) {
        xSql = "select isDeleted,createdAt,product,price,total,name,quantity,data,id from `order` where transaction = ?";
        Order x = new Order();
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                x.setIsDeleted(rs.getBoolean(1));
                x.setCreatedAt(rs.getTimestamp(2));
                x.setProduct(rs.getInt(3));
                x.setPrice(rs.getDouble(4));
                x.setTotal(rs.getDouble(5));
                x.setName(rs.getString(6));
                x.setQuantity(rs.getInt(7));
                x.setData(rs.getString(8));
                x.setId(rs.getInt(9));
                ps.close();
            }
        } catch (Exception e) {
        }
        return x;
    }

    public void insertOrder(Order x) {
        try {
            xSql = "insert into `order`(isDeleted,createdAt,product,price,total,name,quantity,status,transaction,email,user) values (?,?,?,?,?,?,?,?,?,?,?) ";
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getTime());
            ps.setInt(3, x.getProduct());
            ps.setDouble(4, x.getPrice());
            ps.setDouble(5, x.getTotal());
            ps.setString(6, x.getName());
            ps.setInt(7, x.getQuantity());
            ps.setString(8, x.getStatus());
            ps.setInt(9, x.getTransaction());
            ps.setString(10, x.getEmail());
            ps.setInt(11, x.getUser());
            ps.executeUpdate();

            con.commit();
            System.out.println("tao order");
            ps.close();
        } catch (Exception e) {
        }
    }

    public void setSatatusOrder(int tran, String status) {
        try {
            xSql = "Update `order` SET status = ? where transaction = ?";
            ps = con.prepareStatement(xSql);
            ps.setString(1, status);
            ps.setInt(2, tran);
            ps.executeUpdate();
            con.commit();
            ps.close();
        } catch (Exception e) {
        }
    }

    public int countOrderByID() {
        xSql = "SELECT COUNT(id) FROM `order`;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Order> getAllOrderByFilter(String search, String SupId, String ProId) {
        List<Order> list = new ArrayList<>();
        xSql = "select * from `order` o , supplier s, product p\n"
                + "where o.id = s.id and o.id = p.id and o.name like ?";
        if (!SupId.isEmpty()) {
            xSql = xSql + " and  o.id = " + SupId;
        }
        if (!ProId.isEmpty()) {
            xSql = xSql + " and o.id = " + ProId;
        }
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setCreatedAt(rs.getTimestamp("createdAt"));
                order.setStatus(rs.getString("status"));
                order.setTotal(rs.getDouble("total"));
                order.setData(rs.getString("data"));
                list.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> getAllOrder() {
        xSql = "SELECT `order`.`id`,`order`.`name`, `order`.`createdAt`,`order`.`status`,`order`.`total`,`user`.`fullname`\n"
                + "FROM `order`\n"
                + "INNER JOIN `user` ON `order`.`user`= `user`.`id`;";
        List<Order> list = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setCreatedAt(rs.getTimestamp("createdAt"));
                order.setStatus(rs.getString("status"));
                order.setTotal(rs.getDouble("total"));
                list.add(order);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    // delete order
    public void deleteOrder(String id) {
        xSql = "DELETE  FROM `order` WHERE id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int sumTransaction() {
        xSql = "SELECT SUM(total) FROM transaction WHERE status = 'Success'";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList<>();
        xSql = "SELECT * FROM `product` ORDER BY name ASC;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product products = new Product();
                products.setId(rs.getInt("id"));
                products.setName(rs.getString("name"));
                list.add(products);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> getOrderDetailById(String id) {
        List<Order> list = new ArrayList<>();
        xSql = "SELECT * FROM `order` WHERE id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setName(rs.getString("name"));
                order.setData(rs.getString("data"));
                order.setQuantity(rs.getInt("quantity"));
                order.setTotal(rs.getInt("total"));
                list.add(order);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> getCustomerByUser(String user) {
        List<Order> list = new ArrayList<>();
        xSql = "SELECT * FROM `order` WHERE user = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, user);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setQuantity(rs.getInt("quantity"));
                order.setPrice(rs.getDouble("price"));
                order.setTotal(rs.getInt("total"));
                list.add(order);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> payging(String sid, String pid, String search) {
        List<Order> list = new ArrayList<>();
        xSql = "SELECT * FROM `order` WHERE 1 = 1 ";

        if (pid != null && !pid.isEmpty()) {
            xSql += " AND order.name like '%" + pid + "%' ";
        } 

        if (sid != null && !sid.isEmpty()) {
            if(pid != null && !pid.isEmpty()){
                xSql += " OR ";
            }else {
                xSql += " AND ";
            }
            xSql += " order.name like '%" + sid + "%' ";
        }

        if (search != null && !search.isEmpty()) {
            if((pid != null && !pid.isEmpty()) || (sid != null && !sid.isEmpty())){
                xSql += " OR ";
            }else {
                xSql += " AND ";
            }
            xSql += " order.name like '%" + search + "%'";
        }
        
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));
                order.setName(rs.getString("name"));
                order.setCreatedAt(rs.getTimestamp("createdAt"));
                order.setStatus(rs.getString("status"));
                order.setTotal(rs.getDouble("total"));
                order.setUser(rs.getInt("user"));
                list.add(order);
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> getAllQuantityList() {
        SupplierDAO sp = new SupplierDAO();
        List<Supplier> lst = sp.getAll();
        List<Order> list = new ArrayList<>();
        for (int i = 0; i <= lst.size() - 1; i++) {
            xSql = "Select Sum(quantity) from `order` where name like '%" + lst.get(i).getName() + "%'";
            try {
                ps = con.prepareStatement(xSql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Order x = new Order();
                    x.setQuantity(rs.getInt(1));
                    x.setName(lst.get(i).getName());
                    list.add(x);
                }
                rs.close();
                ps.close();
            } catch (Exception e) {
            }
        }
        return list;
    }

    // user by id
    public String getNameById(int id) {
        String name = "";
        xSql = "Select name from `user` where id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                name += rs.getString(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }
    
    public List<Order> pagination(List<Order> order, int start, int end){
        List<Order> after = new ArrayList<>();
        for (int i = start; i < end; i++) {
            after.add(order.get(i));
        }
        return after;
    }
}
