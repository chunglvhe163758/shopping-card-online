/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Transaction;

/**
 *
 * @author hieub
 */
public class TransactionAddDAO {
    private static TransactionAddDAO instance;
    private Connection connection;

    private TransactionAddDAO(String url, String user, String password) throws SQLException {
        connection = DriverManager.getConnection(url, user, password);
    }

    public static synchronized TransactionAddDAO getInstance(String url, String user, String password) throws SQLException {
        if (instance == null) {
            instance = new TransactionAddDAO(url, user, password);
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }
    
    public Transaction insertTransaction(int u, double ammount) throws SQLException{
        PreparedStatement ps;
        ResultSet rs ;
        TransactionAddDAO connection = TransactionAddDAO.getInstance("jdbc:mysql://localhost:3306/team3", "root", "");
        Connection conn = connection.getConnection();
        Transaction x = new Transaction();
        int order = 0;
        double wallet = 0;
        String status = "Failed";
        try {
            
            conn.setAutoCommit(false);
            ps = conn.prepareStatement("select wallet from user where id = ?");
            ps.setInt(1, u);
            rs = ps.executeQuery();
            while (rs.next()) {                
                wallet = rs.getDouble(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        if(wallet > ammount){
            status = "Success";
            try {
                ps = conn.prepareStatement("UPDATE user SET wallet = wallet - ? "+ " WHERE id = ?");
                ps.setDouble(1, ammount);
                ps.setInt(2, u);
                ps.executeUpdate();
                ps.close();
            } catch (Exception e) {
            }
        }
        try {
            
            ps = conn.prepareStatement("insert into transaction(isDeleted,createdAt,updatedAt,name,type,total,status,content,user,process) values(?,?,?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS); 
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getTime());
            ps.setTimestamp(3, x.getTime());
            ps.setString(4, "Thanh toán đơn hàng");
            ps.setBoolean(5, false);
            ps.setDouble(6, ammount);
            ps.setString(7, status);
            ps.setString(8, "mua the");
            ps.setInt(9, u);
            ps.setBoolean(10, false);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                order = rs.getInt(1);
            }
            conn.commit();
            conn.setAutoCommit(true);
        } catch (Exception e) {
        }
        x.setId(order);
        x.setStatus(status);
        return x;
    }
}
