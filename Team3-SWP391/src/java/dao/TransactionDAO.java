/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Date;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import model.Transaction;
import vnpay.Config;

/**
 *
 * @author hieub
 */
public class TransactionDAO extends MyDAO {

    public void deleteTransaction(String id) {
        xSql = "DELETE  FROM `transaction` WHERE id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, id);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Transaction> searchTransactionByContent(String search) {
        List<Transaction> list = new ArrayList<>();
        xSql = "SELECT * FROM `transaction` WHERE (? IS NULL OR content LIKE ?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, search);
            ps.setString(2, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Transaction transaction = new Transaction();
                transaction.setId(rs.getInt("id"));
                transaction.setName(rs.getString("name"));
                transaction.setType(rs.getBoolean("type"));
                transaction.setStatus(rs.getString("status"));
                transaction.setContent(rs.getString("content"));
                transaction.setTotal(rs.getDouble("total"));
                transaction.setProcess(rs.getBoolean("process"));
                transaction.setUser(rs.getInt("user"));
                list.add(transaction);
            }
            ps.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Transaction> getAllTotalList() {
        List<Transaction> list = new ArrayList<>();
        xSql = "SELECT SUM(`transaction`.`total`) as total, `transaction`.`type` as type\n"
                + "FROM `transaction`\n"
                + "GROUP BY `transaction`.`type`;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Transaction transaction = new Transaction();
                transaction.setTotal(rs.getDouble("total"));
                transaction.setType(rs.getBoolean("type"));
                list.add(transaction);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int countTypeTransactionByTotal() {
        xSql = "SELECT SUM(total) FROM `transaction` WHERE type = 1;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int countTypeTransactionByTotal1() {
        xSql = "SELECT SUM(total) FROM `transaction` WHERE type = 0;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            ps.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Transaction> getListTransNo() {
        xSql = "Select * from `transaction` where status ='wait' order by createdAt "
                + "Limit 10 offset 0";
        List<Transaction> lst = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Transaction x = new Transaction();
                x.setId(rs.getInt("id"));
                x.setTotal(rs.getInt("total"));
                x.setName(rs.getString("name"));
                x.setCreatedAt(rs.getTimestamp("createdAt"));
                lst.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return lst;
    }

    public int countHistoryTransaction(String code, String from, String to, String money1, String money2, String status, String method, String user) {
        xSql = "SELECT COUNT(*) as total_number FROM `transaction` WHERE id LIKE '%" + code + "%' AND createdAt BETWEEN '" + from + "' AND '" + to + "' AND total BETWEEN " + money1 + " AND " + money2 + " and status like '%" + status + "%' and content LIKE '%" + method + "%' and user = " + user;
        int num = 0;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return num;
    }

    public int countHistoryTransaction2(String code, String from, String to, String money1, String money2, String status, String method, String type, String user) {
        xSql = "SELECT COUNT(*) as total_number FROM `transaction` WHERE id LIKE '%" + code + "%' AND createdAt BETWEEN '" + from + "' AND '" + to + "' AND total BETWEEN " + money1 + " AND " + money2 + " and status like '%" + status + "%' and content LIKE '%" + method + "%' and type = " + type + " and user = " + user;
        int num = 0;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return num;
    }

    public void insertTransaction(Transaction x) {
        xSql = "insert into transaction(isDeleted,createdAt,name,type,total,status,content,user,process) values(?,?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getCreatedAt());
            ps.setString(3, x.getName());
            ps.setBoolean(4, x.isType());
            ps.setDouble(5, x.getTotal() / 100);
            ps.setString(6, x.getStatus());
            ps.setString(7, x.getContent());
            ps.setInt(8, x.getUser());
            ps.setBoolean(9, x.isProcess());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }

    public Transaction addTransaction(int u, double ammount) {
        Transaction x = new Transaction();
        int order = 0;
        double wallet = 0;
        String status = "Failed";
        try {

            con.setAutoCommit(false);
            ps = con.prepareStatement("select wallet from user where id = ?");
            ps.setInt(1, u);
            rs = ps.executeQuery();
            while (rs.next()) {
                wallet = rs.getDouble(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        if (wallet > ammount) {
            status = "Success";
            try {
                ps = con.prepareStatement("UPDATE user SET wallet = wallet - ? " + " WHERE id = ?");
                ps.setDouble(1, ammount);
                ps.setInt(2, u);
                ps.executeUpdate();
                ps.close();
            } catch (Exception e) {
            }
        }
        try {

            ps = con.prepareStatement("insert into transaction(isDeleted,createdAt,updatedAt,name,type,total,status,content,user,process) values(?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getTime());
            ps.setTimestamp(3, x.getTime());
            ps.setString(4, "Thanh toán đơn hàng");
            ps.setBoolean(5, false);
            ps.setDouble(6, ammount);
            ps.setString(7, status);
            ps.setString(8, "mua the");
            ps.setInt(9, u);
            ps.setBoolean(10, false);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                order = rs.getInt(1);
            }
            rs.close();
            ps.close();
            con.commit();
            con.setAutoCommit(true);
        } catch (Exception e) {
        }
        x.setId(order);
        x.setStatus(status);
        return x;
    }

    public int insertTransaction1(Transaction x) {
        xSql = "insert into transaction(isDeleted,createdAt,name,type,total,status,content,user,process) values(?,?,?,?,?,?,?,?,?)";
        int order = 0;
        try {
            ps = con.prepareStatement(xSql, Statement.RETURN_GENERATED_KEYS);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getCreatedAt());
            ps.setString(3, x.getName());
            ps.setBoolean(4, x.isType());
            ps.setDouble(5, x.getTotal() / 100);
            ps.setString(6, x.getStatus());
            ps.setString(7, x.getContent());
            ps.setInt(8, x.getUser());
            ps.setBoolean(9, x.isProcess());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                order = rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {

        }
        return order;
    }

    public List<Transaction> getTransactionByUser(int limit, int off, String codeid, String from, String to, String money1, String money2, String statusid, String contentid, String typeid, String id) {
        List<Transaction> lst = new ArrayList<>();
        xSql = "SELECT * FROM `transaction` WHERE user = " + id + " and id LIKE '%" + codeid + "%' AND createdAt BETWEEN '" + from + "' AND '" + to + "' AND total BETWEEN " + money1 + " AND " + money2 + " and status like '%" + contentid + "%' and content LIKE '%" + statusid + "%' ";
        typeid.trim();
        if (!typeid.equals("")) {
            xSql += " and type = " + typeid;
        }
        xSql += " ORDER BY createdAt DESC\n"
                + "LIMIT " + limit + " OFFSET " + off + "";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                boolean isDeleted = rs.getBoolean(1);
                Timestamp created = rs.getTimestamp(2);
                Timestamp updatedAt = rs.getTimestamp(3);
                String updatedBy = rs.getString(4);
                Timestamp deletedAt = rs.getTimestamp(5);
                String deletedBy = rs.getString(6);
                String name = rs.getString(7);
                boolean type = rs.getBoolean(8);
                Double total = rs.getDouble(9);
                String status = rs.getString(10);
                String content = rs.getString(11);
                int user = rs.getInt(12);
                boolean code = rs.getBoolean(13);
                int id1 = rs.getInt(14);
                System.out.println(id1);
                Transaction x = new Transaction(isDeleted, created, updatedAt, updatedBy, deletedAt, deletedBy, name, type, total, status, content, user, code, id1);
                lst.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        System.out.println(xSql);

        return lst;
    }

    public void setStatus1(String status, String code) {
        Transaction x = new Transaction();
        xSql = "UPDATE transaction\n"
                + "SET status = '" + status + "' , updatedAt = '" + x.getTime() + "'\n"
                + "WHERE id = ?";
        try {
            ps = con.prepareStatement(xSql);

            ps.setString(1, code);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }

    }

    public int setStatus(String status, String code) {
        Transaction x = new Transaction();
        double ammount = 0;
        int user = 0;
        System.out.println("quaday");
        xSql = "UPDATE transaction\n"
                + "SET status = '" + status + "' , updatedAt = '" + x.getTime() + "'\n"
                + "WHERE id = ?";
        try {

            ps = con.prepareStatement(xSql);
            ps.setString(1, code);
            ps.executeUpdate();
            ps.close();
            System.out.println("qua ham update");
        } catch (Exception e) {
        }
        try {
            xSql = "SELECT total,user FROM `transaction` WHERE id = ?";

            ps = con.prepareStatement(xSql);
            ps.setString(1, code);

            rs = ps.executeQuery();
            while (rs.next()) {
                ammount = rs.getDouble("total");
                user = rs.getInt("user");
                System.out.println(ammount);
            }
            System.out.println("qua ham select");
            rs.close();
            ps.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        if (status.equals("success")) {
            xSql = "UPDATE user SET wallet = wallet + " + ammount + " WHERE id = ?";
            try {
                ps = con.prepareStatement(xSql);
                ps.setInt(1, user);
                ps.executeUpdate();
                ps.close();
            } catch (Exception e) {
                System.out.println("loi database");
            }
        }
        int tien = (int) ammount;
        return tien;
    }

    public void transactionBackMoney(Transaction x, int id) {
        xSql = "insert into transaction(isDeleted,createdAt,name,type,total,status,content,user,process) values(?,?,?,?,?,?,?,?,?)";
        if (x.getUser() == 0) {
            x.setStatus("wait");
        }
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getCreatedAt());
            ps.setString(3, x.getName());
            ps.setBoolean(4, x.isType());
            ps.setDouble(5, x.getTotal());
            ps.setString(6, x.getStatus());
            ps.setString(7, x.getContent());
            ps.setInt(8, x.getUser());
            ps.setBoolean(9, x.isProcess());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        try {
            xSql = "UPDATE user SET wallet = wallet + " + x.getTotal() + " WHERE id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getUser());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        try {
            xSql = "Update `order` SET status='fail' where id = ?";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            ps.executeUpdate();
            ps.close();
            con.commit();
        } catch (Exception e) {
        }
    }

    public int countTransaction(int id) {
        xSql = "select COUNT(id) as counttrans from transaction where user =" + id;
        int count = 0;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
        }
        if (count == 0) {
            count = 1;
        }
        return count;
    }

    public int buyCardTransaction(int id, double ammount) {
        int order = 0;
        Transaction x = new Transaction();

        String str = "Thanh toán đơn hàng";
        xSql = "insert into transaction(isDeleted,createdAt,name,type,total,status,content,user,process) values(?,?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql, Statement.RETURN_GENERATED_KEYS);
            ps.setBoolean(1, false);
            ps.setTimestamp(2, x.getTime());
            ps.setString(3, str);
            ps.setBoolean(4, false);
            ps.setDouble(5, ammount);
            ps.setString(6, "Failed");
            ps.setString(7, "mua the");
            ps.setInt(8, id);
            ps.setBoolean(9, false);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                order = rs.getInt(1);
            }
            ps.close();
        } catch (Exception e) {
        }
        return order;
    }

    

    public double getMoneyByTypeAnDateAndStatus(int type, String date, String status) {
        String[] s = date.split("-");
        xSql = "SELECT SUM(total) FROM `transaction` WHERE YEAR(`createdAt`) in (?) and month(`createdAt`) in (?) and DAY(`createdAt`) in (?) and status like ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, s[0]);
            ps.setString(2, s[1]);
            ps.setString(3, s[2]);
            ps.setString(4, "%" + status + "%");

            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getDouble(1);
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;

    }
//    public static void main(String[] args) {
//        Transaction x = null;
//        TransactionDAO td = new TransactionDAO();
//       x = td.addTransaction(2, 40000);
//        System.out.println(x.getId()+ x.getStatus());
//    }
//    public static void main(String[] args) {
////        TransactionDAO td = new TransactionDAO();
//////       System.out.println(td.getTransactionByUser(2,5,"", "2023-02-16T20:39", "2023-06-28 00:00:00", "10000", "9999999", "", "","0","2").size());
//////        System.out.println(td.countHistoryTransaction2("", "2023-02-16T20:39", "2023-06-28 00:00:00", "10000", "30000", "", "","0","2"));
////td.buyCardTransaction(2, 20000);
//    }
//    public static void main(String[] args) {
//         TransactionDAO td = new TransactionDAO();
//         System.out.println(td.setStatus("success", "53"));
//    }
}
