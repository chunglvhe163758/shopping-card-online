package cronjob;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dao.MyDAO;
import dao.TransactionDAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Order;
import model.Storage;
import model.Transaction;
import utils.SendEmail;
import utils.SendEmailProduct;

public class OrderProcessor extends MyDAO implements Runnable {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/team3";
    private static final String DB_USER = "user";
    private static final String DB_PASS = "password";
    private int orderId;

    public OrderProcessor(int orderId) {
        this.orderId = orderId;
    }

    public void run() {
        try {
            xSql = "SELECT * FROM `order` WHERE id = ?";
            con.setAutoCommit(true);
            ps = con.prepareStatement(xSql);
            ps.setInt(1, orderId);
            rs = ps.executeQuery();
            Order x = new Order();
            rs.next();
            x.setId(orderId);
            x.setQuantity(rs.getInt("quantity"));
            x.setProduct(rs.getInt("product"));
            x.setUser(rs.getInt("user"));
            x.setEmail(rs.getString("email"));
            x.setTotal(rs.getDouble("total"));
            x.setPrice(rs.getDouble("price"));
            x.setName(rs.getString("name"));
            x.setCreatedAt(rs.getTimestamp("createdAt"));
            System.out.println(xSql);
            System.out.println(x.getName());
            rs.close();
            ps.close();
            System.out.println("orderprocesssor "+x.getId());
            xSql = "Select quantity from `product` where id = ?";
            con.setAutoCommit(true);
            ps = con.prepareStatement(xSql);
            ps.setInt(1, x.getProduct());
            rs = ps.executeQuery();
            int quantity = 0;
            while (rs.next()) {                
               quantity = rs.getInt("quantity"); 
            }
            System.out.println("so luong =" + quantity);
            rs.close();
            ps.close();
            if (x.getQuantity() < quantity) {
                xSql = "Update `product` SET quantity = quantity - ? Where id = ?";
                con.setAutoCommit(true);
                ps = con.prepareStatement(xSql);
                ps.setInt(1, x.getQuantity());
                ps.setInt(2, x.getProduct());
                System.out.println(xSql);
                ps.executeUpdate();
                ps.close();
                List<Storage> lst = new ArrayList<>();
                xSql = "SELECT * FROM storage\n"
                        + "WHERE status = 1 and product = ?\n"
                        + "LIMIT ?";
                System.out.println(xSql);
                con.setAutoCommit(true);
                ps = con.prepareStatement(xSql);
                ps.setInt(1, x.getProduct());
                ps.setInt(2, x.getQuantity());
                 
                rs = ps.executeQuery();
                while (rs.next()) {
                    Storage y = new Storage();
                    y.setCode(rs.getString("code"));
                    y.setSeri(rs.getString("seri"));
                    y.setExp(rs.getTimestamp("exp"));
                    lst.add(y);
                }
                rs.close();
                ps.close();

                JsonArray jsonArray = new JsonArray();
                for (Storage s : lst) {
                    JsonObject o = new JsonObject();
                    o.addProperty("seri", s.getSeri());
                    o.addProperty("code", s.getCode());
                    String time = s.getExp().toString();
                    o.addProperty("exp", time);
                    jsonArray.add(o);
                }
                String result = jsonArray.toString();
                System.out.println(result);
                con.setAutoCommit(true);
                xSql = "Update `order` SET data = ?, status='success' where id = ?";
                ps = con.prepareStatement(xSql);
                ps.setString(1, result);
                ps.setInt(2, orderId);
                System.out.println(xSql);
                ps.executeUpdate();
                ps.close();
                x.setData(result);
                SendEmailProduct sd = new SendEmailProduct();
                sd.threadMail(x.getEmail(), x);
            } else {
                String str = "Hoàn trả tiền đơn hàng " + orderId;
                Transaction t = new Transaction(false, x.getTime(), x.getTime(), "", x.getTime(), "", str, true, x.getTotal(), "success", "hoan tien", x.getUser(), true, orderId);
                TransactionDAO td = new TransactionDAO();
                td.transactionBackMoney(t,orderId);
                if (x.getUser() == 0) {
                    SendEmail sd1 = new SendEmail();
                    sd1.threadBackMoney(x.getEmail(), String.valueOf(orderId));                  
                } else {
                    SendEmail sd1 = new SendEmail();
                    sd1.threadBackMoney(x.getEmail(), String.valueOf(orderId)); 
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
