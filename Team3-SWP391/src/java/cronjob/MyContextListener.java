package cronjob;

import cronjob.OrderScannerJob;
import java.util.*;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebListener;

@WebListener
public class MyContextListener implements ServletContextListener {
    private Timer timer;

    /**
     *
     * @param event
     */
    @Override
    public void contextInitialized(ServletContextEvent event) {
        // khởi tạo Timer và đặt lịch trình cho việc thực hiện tác vụ định kỳ
        timer = new Timer();
        MyTimerTask mt = new MyTimerTask();
        timer.scheduleAtFixedRate(mt, 0, 3000);
    }

//    public void contextDestroyed(ServletContextEvent event) {
//        // huỷ bỏ Timer khi ServletContext bị huỷ bỏ
//        timer.cancel();
//    }
}

class MyTimerTask extends TimerTask {
    @Override
    public void run() {
        // code của tác vụ định kỳ
        new Thread(new OrderScannerJob()).start();
        
    }
}