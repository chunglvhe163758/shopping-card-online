package cronjob;

import cronjob.OrderScannerJob;
import it.sauronsoftware.cron4j.Scheduler;
import it.sauronsoftware.cron4j.Task;
import it.sauronsoftware.cron4j.TaskExecutionContext;

public class OrderJobScheduler {
    public static void main(String[] args) {
        // Tạo một task để quét các đơn hàng chưa được xử lý
       
        Task orderScannerTask = new Task() {
            @Override
            public void execute(TaskExecutionContext context) throws RuntimeException {
                new Thread(new OrderScannerJob()).start();
            }
        };

        // Lập lịch cho task quét các đơn hàng chưa được xử lý
        Scheduler scheduler = new Scheduler();
        scheduler.schedule(" * * * * *", orderScannerTask); // Lập lịch cho task chạy mỗi 10 phút

        // Chạy scheduler
        scheduler.start();
    }
}