package cronjob;

import cronjob.OrderProcessor;
import dao.MyDAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import model.Order;

public class  OrderScannerJob extends MyDAO implements Runnable{
    private ExecutorService executor;

    public synchronized void run() {
        
        try {
            xSql = "select * from `order` where status like '%process%'";
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            executor = Executors.newSingleThreadExecutor();
            List<Order> list = new ArrayList<>();
            while (rs.next()) {                
                int orderId = rs.getInt("id");
                Order x = new Order();
                x.setId(orderId);
                System.out.println("vong while"+orderId);
                list.add(x);
            }
            rs.close();
            ps.close();
            for (int i = 0; i < list.size(); i++) {
                executor.submit(new OrderProcessor(list.get(i).getId()));
                System.out.println("vong for" +list.get(i).getId());
            }
//            while (rs.next()) {                
//                 int orderId = rs.getInt("id");
//                executor.submit(new OrderProcessor(orderId));
//                System.out.println("order tu ham order sacnnerjob"+orderId);
//            }
             executor.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}