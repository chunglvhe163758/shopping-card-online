/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package admin.controller;

import dao.OrderDAO;
import dao.ProductDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Order;
import model.Product;
import model.Supplier;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ManagerOrder", urlPatterns = {"/admin/managerorder"})
public class ManagerOrder extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String indexPage = request.getParameter("index");
        if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);

        ProductDAO dao = new ProductDAO();
        OrderDAO odao = new OrderDAO();
        int count = odao.countOrderByID();
        int counto = odao.countOrderByID();

        String search = request.getParameter("txt");
        String SupId = request.getParameter("SupId");
        String ProId = request.getParameter("ProId");

        List<Supplier> suppliers = dao.getAllSupplier();
        List<Product> products = odao.getAllProduct();
//        List<Order> orders = odao.getAllOrderByFilter(search, SupId, ProId);
        List<Order> order = odao.getAll();
        if (search != null && !search.isEmpty() || SupId != null && !SupId.isEmpty() || ProId != null && !ProId.isEmpty()) {
            order = odao.payging(SupId, ProId, search);
        }
        
        String perPage = request.getParameter("perPage");
        int size = order.size();
        int numberPerPage = ( perPage == null ) ? 8 : Integer.parseInt(perPage);
        int start = (index - 1) * numberPerPage;
        int end = Math.min(index * numberPerPage, size);
        int numberOfPage = (size / numberPerPage == 0 ? size / numberPerPage : size/numberPerPage + 1);
        
        List<Order> data = odao.pagination(order, start, end);
        
        request.setAttribute("order", data);
        request.setAttribute("tag", index);

        request.setAttribute("endP", numberOfPage);

        request.setAttribute("ocount", counto);
        request.setAttribute("products", products);
        request.setAttribute("suppliers", suppliers);
//        request.setAttribute("order", orders);
        request.setAttribute("txtS", search);
        request.setAttribute("SupId", SupId);
        request.setAttribute("ProId", ProId);
        request.getRequestDispatcher("order.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
