/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vnpay;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import dao.OrderDAO;
import dao.TransactionDAO;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.PrintWriter;
import model.Order;
import model.Transaction;
import model.User;

/**
 *
 * @author CTT VNPAY
 */
public class ajaxSeverlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       resp.sendRedirect("login.jsp");
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
       
//        if(accout.equals("0")){
//            req.setAttribute("erorr", "Vui lòng đăng nhập trước khi nạp tiền!");
//            resp.sendRedirect("login.jsp");
//        }else{
        String vnp_Version = "2.1.0";
        String vnp_Command = "pay";
        String orderType = "sale";
        long amount = Integer.parseInt(req.getParameter("amount"))*100;
        String bankCode = req.getParameter("bankCode");
        String account = "0";
        String email = "";
        account = req.getParameter("account");
        String typepay = "",nameProduct ="",giaProduct ="",quantityProduct = "",idProduct = "";
         String vnp_TxnRef = Config.getRandomNumber(6);
        try {
        email = req.getParameter("email");
        typepay = (String)req.getParameter("typeCard");
        nameProduct = (String) req.getParameter("nameProduct");
        giaProduct = (String) req.getParameter("giaProduct");
        idProduct = (String) req.getParameter("idProduct");
        quantityProduct = (String) req.getParameter("quantityProduct");
       
        } catch (Exception e) {
        }
        
        Transaction t = new Transaction();
        t.setCreatedAt(t.getTime());
        t.setName("Nạp tiền bằng VNpay");
        t.setType(true);
        t.setTotal(Double.valueOf(amount));
        t.setStatus("Failed");
        t.setContent("nap tien");
        t.setUser(Integer.parseInt(account));
        t.setProcess(false);
        TransactionDAO td = new TransactionDAO();
        OrderDAO od = new OrderDAO();
        String strreturn = Config.vnp_Returnurl;
        String odid = vnp_TxnRef;
        if(typepay.equals("vnpay")){
               t.setContent("Order qua Vnpay");
               int orderid = td.insertTransaction1(t);
                Order y = new Order();
                y.setEmail(email);
                y.setProduct(Integer.valueOf(idProduct));
                y.setName(nameProduct);
                y.setPrice(Double.parseDouble(giaProduct));
                y.setQuantity(Integer.valueOf(quantityProduct));
                y.setTotal(Double.valueOf(amount)/100);
                y.setUser(Integer.valueOf(account));
                y.setTransaction(orderid);
                y.setStatus("wait");
                od.insertOrder(y);
                odid = String.valueOf(orderid);
               strreturn = "http://localhost:8080/autothecao/client/buycardvnpay";
            }else{
              int orderid1 =  td.insertTransaction1(t);
              odid = String.valueOf(orderid1);
            }
        
        String vnp_IpAddr = Config.getIpAddress(req);
        String vnp_TmnCode = Config.vnp_TmnCode;
        Map<String, String> vnp_Params = new HashMap<>();
        vnp_Params.put("vnp_Version", vnp_Version);
        vnp_Params.put("vnp_Command", vnp_Command);
        vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
        vnp_Params.put("vnp_Amount", String.valueOf(amount));
        vnp_Params.put("vnp_CurrCode", "VND");
        
        if (bankCode != null && !bankCode.isEmpty()) {
            vnp_Params.put("vnp_BankCode", bankCode);
        }
        vnp_Params.put("vnp_TxnRef", odid);
        vnp_Params.put("vnp_OrderInfo", "Thanh toan don hang: " + odid);
        vnp_Params.put("vnp_OrderType", orderType);

        String locate = req.getParameter("language");
        if (locate != null && !locate.isEmpty()) {
            vnp_Params.put("vnp_Locale", locate);
        } else {
            vnp_Params.put("vnp_Locale", "vn");
        }
       
        vnp_Params.put("vnp_IpAddr", vnp_IpAddr);
        Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String vnp_CreateDate = formatter.format(cld.getTime());
        vnp_Params.put("vnp_CreateDate", vnp_CreateDate);
     
         vnp_Params.put("vnp_TxnRef", odid);
         vnp_Params.put("vnp_ReturnUrl", strreturn);
        cld.add(Calendar.MINUTE, 15);
        String vnp_ExpireDate = formatter.format(cld.getTime());
        vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);
        
        List fieldNames = new ArrayList(vnp_Params.keySet());
        Collections.sort(fieldNames);
        StringBuilder hashData = new StringBuilder();
        StringBuilder query = new StringBuilder();
        Iterator itr = fieldNames.iterator();
        while (itr.hasNext()) {
            String fieldName = (String) itr.next();
            String fieldValue = (String) vnp_Params.get(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                //Build hash data
                hashData.append(fieldName);
                hashData.append('=');
                hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                //Build query
                query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                query.append('=');
                query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                if (itr.hasNext()) {
                    query.append('&');
                    hashData.append('&');
                }
            }
        }
        String queryUrl = query.toString();
        String vnp_SecureHash = Config.hmacSHA512(Config.vnp_HashSecret, hashData.toString());
        queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
        String paymentUrl = Config.vnp_PayUrl + "?" + queryUrl;
        com.google.gson.JsonObject job = new JsonObject();
        job.addProperty("code", "00");
        job.addProperty("message", "success");
        job.addProperty("data", paymentUrl);
        Gson gson = new Gson();
        resp.getWriter().write(gson.toJson(job));
        
        }
   // }

}
