<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="bg-white" id="sidebar-wrapper">
    <div class="sidebar-heading text-center py-4 primary-text fs-4 fw-bold text-uppercase border-bottom"><i
            class="fas fa-user-secret me-2"></i>Admin</div>
    <div class="list-group list-group-flush my-3">
        <a href="admin" class="list-group-item list-group-item-action bg-transparent second-text <%= (request.getRequestURI().endsWith("home.jsp") || request.getRequestURI().endsWith("/admin")) ? "active" : "" %>"><i 
                class="fa-solid fa-house me-2"></i>Trang Chủ</a>
        <a href="manageruser" class="list-group-item list-group-item-action bg-transparent second-text <%= (request.getRequestURI().endsWith("user.jsp") || request.getRequestURI().endsWith("/manageruser")) ? "active" : "" %> fw-bold"><i 
                class="fa-sharp fa-solid fa-users-gear me-2"></i>Người dùng</a>
        <a href="chartImport" class="list-group-item list-group-item-action bg-transparent second-text <%= (request.getRequestURI().endsWith("statistical.jsp") || request.getRequestURI().endsWith("/chartImport")) ? "active" : "" %> fw-bold"><i 
                class="fa-solid fa-chart-simple me-2"></i>Thông kê</a>
        <a href="managerstorage" class="list-group-item list-group-item-action bg-transparent second-text <%= (request.getRequestURI().endsWith("storage.jsp") || request.getRequestURI().endsWith("/managerstorage")) ? "active" : "" %> fw-bold"><i 
                class="fa-solid fa-database me-2"></i>Kho hàng</a>
        <a href="managerorder" class="list-group-item list-group-item-action bg-transparent second-text <%= (request.getRequestURI().endsWith("order.jsp") || request.getRequestURI().endsWith("/managerorder")) ? "active" : "" %> fw-bold"><i
                class="fas fa-shopping-cart me-2"></i>Ðơn hàng</a>
        <a href="manager" class="list-group-item list-group-item-action bg-transparent second-text <%= (request.getRequestURI().endsWith("products.jsp") || request.getRequestURI().endsWith("/manager")) ? "active" : "" %> fw-bold"><i
                class="fas fa-gift me-2"></i>Sản phẩm</a>
        <a href="managertransaction" class="list-group-item list-group-item-action bg-transparent second-text <%= (request.getRequestURI().endsWith("transaction.jsp") || request.getRequestURI().endsWith("/managertransaction")) ? "active" : "" %> fw-bold"><i 
                class="fa-brands fa-cc-paypal me-2"></i>Thanh toán</a> 
        <a href="transactionBackMoney.jsp" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i 
                class="fa-solid fa-right-from-bracket me-2"></i>Hoàn tiền</a> 
        <a href="logout" class="list-group-item list-group-item-action bg-transparent text-danger fw-bold"><i
                class="fas fa-power-off me-2"></i>Đăng xuất</a>
    </div>
</div>