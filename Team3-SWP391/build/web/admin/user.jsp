<%-- 
    Document   : user_vesion2
    Created on : Jun 21, 2023, 8:41:54 PM
    Author     : Admin
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "dao.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.google.gson.Gson"%>
<%@ page import="com.google.gson.JsonObject"%>
<%@ page import="jakarta.servlet.http.HttpServlet"%>
<%@ page import="jakarta.servlet.http.HttpServletRequest"%>
<%@ page import="jakarta.servlet.http.HttpServletResponse"%>
<%@ page import="jakarta.servlet.http.HttpSession"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>


<%
            List<User> user = (List<User>)request.getAttribute("user");
            List<User> categories = (List<User>)request.getAttribute("categories");

%>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/user.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manager User</title>

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">

    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <%@include file="sidebar.jsp"%>
            <div id="page-content-wrapper">
                <div class="container-xl">
                    <div class="table-responsive">
                        <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h2>Customer <b>Details</b></h2></div>
                                    <div class="col-sm-4">
                                        <form action="searchuser" method="post">
                                            <div class="search-box">                                                
                                                <i class="material-icons"></i>
                                                <input type="text" value="${txtS}" name="txt" class="form-control" placeholder="Search…">                                                                                                                                             
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name <i class="fa fa-sort"></i></th>
                                        <th>Email</th>
                                        <th>Role <i class="fa fa-sort"></i></th>
                                        <th>Status</th>
                                        <th>Wallet <i class="fa fa-sort"></i></th>
                                        <th>Phone</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${user}" var="u">   
                                        <fmt:setLocale value="vi_VN"/>
                                        <tr>
                                            <td>${u.id}</td>
                                            <td>${u.name}</td>
                                            <td>${u.email}</td>
                                            <td>${u.role}</td>
                                            <td>${u.status}</td>
                                            <td><fmt:formatNumber value="${u.wallet}" type="currency" currencyCode="VND" /></td>
                                            <td>${u.phone}</td>
                                            <td>
                                                <a href="#deleteEmployeeModal" class="delete" data-toggle="modal"><i onclick="setId(${u.id})" class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                            </td>
                                        </tr>
                                    </c:forEach>

                                </tbody>
                            </table>
                            <div class="hint-text">Showing <b>13</b> out of <b>${count}</b> entries</div>
                            <nav aria-label="...">
                                <ul class="pagination">
                                    <c:if test="${tag > 1}">
                                        <li class="page-item">
                                            <a class="page-link" href="manageruser?index=${tag-1}">Previous</a>
                                        </li>
                                    </c:if>
                                    <c:forEach begin="1" end="${endP}" var="i">
                                        <li class="page-item ${tag==i?"active":""}">
                                            <a class="page-link" href="manageruser?index=${i}">${i}</a>
                                        </li>
                                    </c:forEach>
                                    <c:if test="${tag < endP}">
                                        <li class="page-item">
                                            <a class="page-link" href="manageruser?index=${tag+1}">Next</a>
                                        </li>
                                    </c:if>
                                </ul>
                            </nav>
                        </div>
                    </div>  
                </div>
            </div>
            <!--            Add Modal-->
            <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="createUserModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="createUserModalLabel">Create User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="products" enctype="multipart/form-data">
                            <input hidden="" name="action" value="create"/>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" required="" name="name" class="form-control" placeholder="Enter name">
                                </div>
                                <div class="form-group">
                                    <label for="name">Email</label>
                                    <input type="text" required="" name="des" class="form-control" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="name">Password</label>
                                    <input type="number" required="" name="price" class="form-control" placeholder="Enter password">
                                </div>
                                <div class="form-group">
                                    <label for="name">Phone</label>
                                    <input type="number" required="" name="quantity"  class="form-control" placeholder="Enter phone">
                                </div>
                                <div class="form-group">
                                    <label for="gender">Category</label>
                                    <select name="categoryId" >
                                        <c:forEach items="${categories}" var="c">
                                            <option value="${c.id}">${c.role}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="gender">Status</label>
                                    <select class="form-control" >
                                        <option selected="" value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Create User Modal -->
            <div class="modal fade" id="editEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="createUserModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="createUserModalLabel">Update User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="products" enctype="multipart/form-data">
                            <input hidden="" name="action" value="update"/>
                            <input hidden="" id="updateId" name="id" />
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" required="" id="name" name="name" class="form-control" placeholder="Enter name">
                                </div>
                                <div class="form-group">
                                    <label for="name">Email</label>
                                    <input type="text" required="" id="des" name="des" class="form-control" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="name">Password</label>
                                    <input type="number" required="" name="price" id="price" class="form-control" placeholder="Enter password">
                                </div>
                                <div class="form-group">
                                    <label for="name">Phone</label>
                                    <input type="number" required="" name="quantity" id="quantity" class="form-control" placeholder="Enter phone">
                                </div>
                                <div class="form-group">
                                    <label for="gender">Role</label>
                                    <select name="categoryId" id="updateCid">
                                        <c:forEach items="${categories}" var="c">
                                            <option value="${c.id}">${c.role}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="gender">Status</label>
                                    <select id="status" class="form-control" >
                                        <option selected="" value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div id="deleteEmployeeModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="deleteuser" method="post">
                            <input hidden="" name="action" value="delete"/>
                            <input hidden="" name="id" id="id"/>
                            <div class="modal-header">						
                                <h4 class="modal-title">Delete Employee</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">					
                                <p>Are you sure you want to delete these Records?</p>
                                <p class="text-warning"><small>This action cannot be undone.</small></p>
                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            const setId = (id) => {
                $('#id').val(id);
            };
        </script>
    </body>
</html>