<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Product</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        
        h1 {
            text-align: center;
        }
        
        form {
            margin: 0 auto;
            width: 400px;
        }
        
        label {
            display: block;
            margin-top: 10px;
        }
        
        input[type="checkbox"],
        input[type="text"],
        input[type="datetime-local"],
        input[type="number"] {
            width: 100%;
            padding: 5px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        
        textarea {
            width: 100%;
            height: 100px;
            padding: 5px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        
        input[type="submit"] {
            display: block;
            margin-top: 10px;
            padding: 10px;
            background-color: #4CAF50;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        
        input[type="submit"]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <h1>Add Product</h1>

    <form action="/autothecao/admin/addProduct" method="post">
        <input type="hidden" name="isdelete" value="1" />

<!--        <label for="createdBy">Created By:</label>
        <input type="text" name="createdBy" id="createdBy">-->

<!--        <label for="createdAt">Created At:</label>
        <input type="datetime-local" name="createdAt" id="createdAt">-->

<!--        <label for="updatedAt">Updated At:</label>
        <input type="datetime-local" name="updatedAt" id="updatedAt">

        <label for="updatedBy">Updated By:</label>
        <input type="text" name="updatedBy" id="updatedBy">-->

<!--        <label for="deletedAt">Deleted At:</label>
        <input type="datetime-local" name="deletedAt" id="deletedAt">

        <label for="deletedBy">Deleted By:</label>
        <input type="text" name="deletedBy" id="deletedBy">-->

        <label for="key">Key:</label>
        <input required type="text" name="key" id="key">

        <label for="name">Name:</label>
        <input type="text" required name="name" id="name">

        <label for="image">Image:</label>
        <input type="text" required name="image" id="image">

        <label for="describe">Describe:</label>
        <textarea name="describe" required id="describe"></textarea>

        <label for="price">Price:</label>
        <input type="number" name="price" id="price" required min="0" step="0.01">

        <label for="profit">Profit:</label>
        <input type="number" name="profit" min="0" required id="profit" step="0.01">

        <label for="supplier">Supplier:</label>
        <select name="supplier">
            <c:forEach var="item" items="${listSuppliers}">
                <option value="${item.id}">${item.name}</option>
            </c:forEach>
        </select>

        <label for="quantity">Quantity:</label>
        <input type="number" name="quantity" min="0" required id="quantity">

<!--        <label for="id">ID:</label>
        <input type="text" name="id" id="id">-->

        <input type="submit" value="Add Product">
    </form>
</body>
</html>