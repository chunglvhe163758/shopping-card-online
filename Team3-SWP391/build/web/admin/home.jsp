<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : index
    Created on : May 27, 2023, 11:07:37 PM
    Author     : hieub
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "dao.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.google.gson.Gson"%>
<%@ page import="com.google.gson.JsonObject"%>
<%@ page import="jakarta.servlet.http.HttpServlet"%>
<%@ page import="jakarta.servlet.http.HttpServletRequest"%>
<%@ page import="jakarta.servlet.http.HttpServletResponse"%>
<%@ page import="jakarta.servlet.http.HttpSession"%>

<%
    List<Order> order = (List) request.getAttribute("order");
    List<Order> orders = (List) request.getAttribute("orders");
    int i1 = order.size()-1;
    int i = orders.size()-1;
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Managements</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <%@include file="sidebar.jsp"%>
            <div id="page-content-wrapper">
                <nav class="navbar navbar-expand-lg navbar-light bg-transparent py-4 px-4">
                    <div class="d-flex align-items-center">
                        <i class="fa-solid fa-bars primary-text fs-4 me-3" id="menu-toggle"></i>
                    </div>

                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle second-text fw-bold" href="#" id="navbarDropdown"
                                   role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-user me-2"></i>Admin
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="changeprofile">Change Profile</a></li>
                                    <li><a class="dropdown-item" href="logout">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div class="container-fluid px-4">
                    <div class="row g-3 my-2">
                        <div class="col-md-3">
                            <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">                               
                                <div>
                                    <p class="fs-5">PRODUCTS</p>
                                    <h6 class="fs-2">${scount}</h6>
                                </div>
                                <i class="fas fa-gift fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                                <div>
                                    <p class="fs-5">INCOME</p>
                                    <h3 class="fs-2"><%= request.getAttribute("tcount") != null 
                                            ? String.format("%,d", request.getAttribute("tcount")) 
                                            : 0 %> $</h3>
                                </div>
                                <i
                                    class="fa-solid fa-sack-dollar fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                                <div>
                                    <p class="fs-5">ORDERS</p>
                                    <h3 class="fs-2">${ocount}</h3>
                                </div>
                                <i class="fa-solid fa-cart-plus fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                                <div>
                                    <p class="fs-5">USER</p>
                                    <h3 class="fs-2">${ucount}</h3>
                                </div>
                                <i class="fa-solid fa-users fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                            </div>
                        </div>
                    </div>
                    <div style="display: flex">        
                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        <div
                            id="myChart" style="width:100%; max-width:600px; height:460px;">
                        </div>
                        <script>
                            google.charts.load('current', {'packages': ['corechart']});
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                var data = google.visualization.arrayToDataTable([
                                    ['Card Type', 'Mhl'],
                                    <%
                                        for(int j = 0; j <= i1; j++){
                                    %>
                                        ['<%=order.get(j).getName()%>', <%=order.get(j).getTotal()%>],    
                                    <%}%>
                                ]);

                                var options = {
                                    title: 'Statistics of sales revenue',
                                    is3D: true
                                };

                                var chart = new google.visualization.PieChart(document.getElementById('myChart'));
                                chart.draw(data, options);
                            }
                        </script>
                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        <div id="myChart1" style="width:100%; max-width:600px; height:460px; "></div>
                        <script>
                            google.charts.load('current', {'packages': ['corechart']});
                            google.charts.setOnLoadCallback(drawChart1);
                            
                            function drawChart1() {
                                
                                var data = google.visualization.arrayToDataTable([
                                    ['Card Type', 'number'],
                                    <%
                                        for(int j = 0; j <= i; j++){
                                    %>
                                        ['<%=orders.get(j).getName()%>', <%=orders.get(j).getQuantity()%>],    
                                    <%}%>
                                ]);

                                var options = {
                                    title: 'Statistics of products sold'
                                };

                                var chart = new google.visualization.BarChart(document.getElementById('myChart1'));
                                chart.draw(data, options);
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
        <script>
                            var el = document.getElementById("wrapper");
                            var toggleButton = document.getElementById("menu-toggle");

                            toggleButton.onclick = function () {
                                el.classList.toggle("toggled");
                            };
        </script>
    </body>
</html>