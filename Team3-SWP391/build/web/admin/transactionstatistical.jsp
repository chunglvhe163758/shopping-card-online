<%-- 
    Document   : transactionstatistical
    Created on : Jul 20, 2023, 1:55:32 PM
    Author     : Admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "dao.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.google.gson.Gson"%>
<%@ page import="com.google.gson.JsonObject"%>
<%@ page import="jakarta.servlet.http.HttpServlet"%>
<%@ page import="jakarta.servlet.http.HttpServletRequest"%>
<%@ page import="jakarta.servlet.http.HttpServletResponse"%>
<%@ page import="jakarta.servlet.http.HttpSession"%>
<%
    List<Transaction> transactions = (List) request.getAttribute("transactions");
    int i = transactions.size() - 1;
%>

<jsp:useBean class="dao.TransactionDAO" id="td"/>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link rel="stylesheet" type="text/css" href="css/transactionstatistical.css">
        <title>Transaction Statistical</title>
        <style>
            a.active{
                color:white;
                font-weight: bolder;
            }
            h6{
                font-size: 16px;
                font-family: 'Roboto', sans-serif;
                font-weight: bold;
                color: #333;
                border-bottom: 1px solid #ccc;
                font-weight: 600;
                letter-spacing: 1px;
                font-style: italic;
                background-color: #f5f5f5;
            }
        </style>
    </head>
    <body> 
        <div class="container-xl" style="display: flex">
            <%@include file="sidebar.jsp"%>
            <div class="table-responsive">
                <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-top: 30px; padding-left: 50px">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" aria-current="page" href="managertransaction">Tra Cứu GD</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="transactionstatistical">Thống Kê</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <h6>Tổng số tiền nạp tiền : <%= request.getAttribute("count1") != null 
                                            ? String.format("%,d", request.getAttribute("count1")) 
                                            : 0 %> $</h6>
                <h6>Tổng số tiền mua thẻ : <%= request.getAttribute("count0") != null 
                                            ? String.format("%,d", request.getAttribute("count0")) 
                                            : 0 %> $</h6>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <div
                    id="myChart" style="width:100%; max-width:600px; height:200px;">
                </div>
                <script>
                    google.charts.load('current', {'packages': ['corechart']});
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Card Type', 'Mhl'],
                    <%
                                for(int j = 0; j <= i; j++){
                    %>
                            ['<%=transactions.get(j).isType()%>', <%=transactions.get(j).getTotal()%>],
                    <%}%>
                        ]);

                        var options = {
                            is3D: true
                        };

                        var chart = new google.visualization.PieChart(document.getElementById('myChart'));
                        chart.draw(data, options);
                    }
                </script>

                <form class="my-4 d-flex justify-content-end" action="" method="post">
                    <div class="col-3">
                        <input type="date" name="date" class="form-control" value="${date}" onchange="this.form.submit()">
                    </div>
                </form>
                <div class="analytics" >
                    <div class="card">
                        <div class="card-head">
                            <h2>${chartDonateSuccess}</h2>
                            <span><i class="fa-solid fa-comments-dollar"></i></span>
                        </div>
                        <div class="card-progress">
                            <small>Nạp tiền thành công</small>
                            <div class="card-indicator">
                                <div class="indicator one" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-head">
                            <h2>${chartDonateFaild}</h2>
                            <span><i class="fa-solid fa-comments-dollar"></i></span>
                        </div>
                        <div class="card-progress">
                            <small>Nạp tiền không thành công</small>
                            <div class="card-indicator">
                                <div class="indicator four" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-head">
                            <h2>${chartBuySuccess}</h2>
                            <span><i class="fa-solid fa-comments-dollar"></i></span>
                        </div>
                        <div class="card-progress">
                            <small>Mua thẻ thành công</small>
                            <div class="card-indicator">
                                <div class="indicator one" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-head">
                            <h2>${chartBuyFaild}</h2>
                            <span><i class="fa-solid fa-comments-dollar"></i></span>
                        </div>
                        <div class="card-progress">
                            <small>Mua thẻ thất bại</small>
                            <div class="card-indicator">
                                <div class="indicator four" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </body>
</html>
