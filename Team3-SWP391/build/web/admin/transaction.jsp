<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%-- 
    Document   : transaction
    Created on : Jul 19, 2023, 10:54:44 PM
    Author     : Admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/transaction.css">
        <title>Transaction Manager</title>

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <style>
            a.active{
                color:white;
                font-weight: bolder;
            }
        </style>
    </head>
    <body>
        <div class="container-xl" style="display: flex">            
            <%@include file="sidebar.jsp"%>
            <div class="table-responsive">
                <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-left: 6.5%; margin-bottom: 4%">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="managertransaction">Tra Cứu GD</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="transactionstatistical">Thống Kê</a>
                                </li>
                            </ul>
                            <form class="d-flex" action="managertransaction" method="post">
                                <input class="form-control me-2" type="text" name="search" value="${txtS}" placeholder="Search" aria-label="Search">
                                <button class="btn btn-outline-success" type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                </nav>
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6"><h2>Manage <b>Transaction</b></h2></div>
                            <div class="col-sm-6">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-info active">
                                        <input type="radio" name="status" value="all" checked="checked"> All
                                    </label>
                                    <label class="btn btn-success">
                                        <input type="radio" name="status" value="success"> Success
                                    </label>
                                    <label class="btn btn-danger">
                                        <input type="radio" name="status" value="Failed"> Failed
                                    </label>
                                    <label class="btn btn-warning">
                                        <input type="radio" name="status" value="wait"> Wait
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Content</th>
                                <th>User</th>
                                <th>Process</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="transaction" items="${transaction}">
                                <c:set var="myColor" value="#1FFF00"/>
                                <c:set var="myColor1" value="#FF0019"/>
                                <c:set var="myColor2" value="#FFDB00"/>
                                <fmt:setLocale value="vi_VN"/>
                                <tr data-status=${transaction.status}>
                                    <td>${transaction.id}</td>
                                    <td>${transaction.name}</td>
                                    <td>
                                        <c:if test="${transaction.type == true}"><span style="color: ${myColor};">Nạp Tiền</span></c:if>
                                        <c:if test="${transaction.type == false}"><span style="color: ${myColor1};">Mua Thẻ</span></c:if>
                                        </td>
                                        <td>
                                        <c:if test="${transaction.type == true && transaction.status =='success'}"><span style="color: ${myColor};">+<fmt:formatNumber value="${transaction.total}" type="currency" currencyCode="VND"/></span></c:if>
                                        <c:if test="${transaction.type == true && transaction.status =='Failed'}"><span style="color: ${myColor1};">-<fmt:formatNumber value="${transaction.total}" type="currency" currencyCode="VND"/></span></c:if>
                                        <c:if test="${transaction.type == true && transaction.status =='wait'}"><span style="color: ${myColor2};"><fmt:formatNumber value="${transaction.total}" type="currency" currencyCode="VND"/></span></c:if>
                                        <c:if test="${transaction.type == false && transaction.status =='success'}"><span style="color: ${myColor1};">-<fmt:formatNumber value="${transaction.total}" type="currency" currencyCode="VND"/></span></c:if>
                                        <c:if test="${transaction.type == false && transaction.status =='Failed'}"><span style="color: ${myColor};">+<fmt:formatNumber value="${transaction.total}" type="currency" currencyCode="VND"/></span></c:if>
                                        <c:if test="${transaction.type == false && transaction.status =='wait'}"><span style="color: ${myColor2};"><fmt:formatNumber value="${transaction.total}" type="currency" currencyCode="VND"/></span></c:if>
                                        </td>
                                        <td>
                                        <c:if test="${transaction.status == 'success'}"><span class="label label-success">Success</span></c:if>
                                        <c:if test="${transaction.status == 'Failed'}"><span <span class="label label-danger">Failed</span></c:if>
                                        <c:if test="${transaction.status == 'wait'}"><span class="label label-warning">Wait</span></c:if>
                                        </td>
                                        <td>${transaction.content}</td>
                                    <td>${transaction.user}</td>
                                    <td>
                                        <c:if test="${transaction.process == true}"><span style="color: ${myColor};">success</span></c:if>
                                        <c:if test="${transaction.process == false}"><span style="color: ${myColor1};">failed</span></c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <c:if test="${empty transaction}">
                        <p>There is no data.</p>
                    </c:if>
                </div> 
            </div>   
        </div>
        <script src="js/transaction.js"></script>
    </body>
</html>
