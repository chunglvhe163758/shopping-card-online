<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- Coding By CodingNepal - codingnepalweb.com -->
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>OTP Verification Form</title>
        <link rel="stylesheet" href="style.css" />
        <!-- Boxicons CSS -->
        <link href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css" rel="stylesheet" />
        <link
                rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
                />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />

        <script src="script.js" defer></script>

        <style>
            @import url("https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap");
            * {
                    margin: 0;
                    padding: 0;
                    box-sizing: border-box;
                    font-family: "Poppins", sans-serif;
            }
            body {
                    min-height: 100vh;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    background: #4070f4;
            }

            .container {
                    background: #fff;
                    padding: 30px 65px;
                    border-radius: 12px;
                    row-gap: 20px;
                    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
            }
            .container header {
                    height: 65px;
                    width: 65px;
                    background: #4070f4;
                    color: #fff;
                    font-size: 2.5rem;
                    border-radius: 50%;
            }
            .container h4 {
                    font-size: 1.25rem;
                    color: #333;
                    font-weight: 500;
            }
            form .input-field {
                    flex-direction: row;
                    column-gap: 10px;
            }
            .input-field input {
                    height: 45px;
                    width: 42px;
                    border-radius: 6px;
                    outline: none;
                    font-size: 1.125rem;
                    text-align: center;
                    border: 1px solid #ddd;
            }
            .input-field input:focus {
                    box-shadow: 0 1px 0 rgba(0, 0, 0, 0.1);
            }
            .input-field input::-webkit-inner-spin-button,
            .input-field input::-webkit-outer-spin-button {
                    display: none;
            }
            form button {
                    margin-top: 25px;
                    width: 100%;
                    color: #fff;
                    font-size: 1rem;
                    border: none;
                    padding: 9px 0;
                    cursor: pointer;
                    border-radius: 6px;
                    pointer-events: none;
                    background: #6e93f7;
                    transition: all 0.2s ease;
            }
            form button.active {
                    background: #4070f4;
                    pointer-events: auto;
            }
            form button:hover {
                    background: #0e4bf1;
            }


            #toast {
                    position: fixed;
                    top: 32px;
                    right: 32px;
                    z-index: 999999;
            }

            .toast {
                    display: flex;
                    align-items: center;
                    background-color: #fff;
                    border-radius: 2px;
                    padding: 20px 0;
                    min-width: 400px;
                    max-width: 450px;
                    border-left: 4px solid;
                    box-shadow: 0 5px 8px rgba(0, 0, 0, 0.08);
                    transition: all linear 0.3s;
            }

            @keyframes slideInLeft {
                    from {
                            opacity: 0;
                            transform: translateX(calc(100% + 32px));
                    }
                    to {
                            opacity: 1;
                            transform: translateX(0);
                    }
            }

            @keyframes fadeOut {
                    to {
                            opacity: 0;
                    }
            }

            .toast--success {
                    border-color: #47d864;
            }

            .toast--success .toast__icon {
                    color: #47d864;
            }

            .toast--info {
                    border-color: #2f86eb;
            }

            .toast--info .toast__icon {
                    color: #2f86eb;
            }

            .toast--warning {
                    border-color: #ffc021;
            }

            .toast--warning .toast__icon {
                    color: #ffc021;
            }

            .toast--error {
                    border-color: #ff623d;
            }

            .toast--error .toast__icon {
                    color: #ff623d;
            }

            .toast + .toast {
                    margin-top: 24px;
            }

            .toast__icon {
                    font-size: 24px;
            }

            .toast__icon,
            .toast__close {
                    padding: 0 16px;
            }

            .toast__body {
                    flex-grow: 1;
            }

            .toast__title {
                    font-size: 16px;
                    font-weight: 600;
                    color: #333;
            }

            .toast__msg {
                    font-size: 14px;
                    color: #888;
                    margin-top: 6px;
                    line-height: 1.5;
            }

            .toast__close {
                    font-size: 20px;
                    color: rgba(0, 0, 0, 0.3);
                    cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div id="toast"></div>
        <%
        String erorr = (String) request.getAttribute("erorr")+"";
        if(!erorr.trim().equals("null")){
        %>
        <input hidden=""  id="error"  value="<%=erorr%>"/>
        <%}%>
        <!--         :where(.container, form, .input-field, header) {
                        display: flex;
                        flex-direction: column;
                        align-items: center;
                        justify-content: center;
                    }-->

        <%
         String status = (String) request.getAttribute("status")+"";
          String email= request.getAttribute("email")+"";
          email.trim();
         if(email.equals("null")){
          request.setAttribute("error", "Bạn không có quyền truy cập đường dẫn này.");
        request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        %>
        <div class="container">
            <header>
                <i class="bx bxs-check-shield"></i>
            </header>
            <span>Vui lòng kiểm tra mã trong email của bạn. Mã này gồm 4 chữ số.</span>
            <h4>Nhập mã OTP  vào đây</h4>
            <form action="verification" method="post">
                <input hidden="" name="action" value="submit"/>
                <input hidden="" name="status" value="<%=status%>"/>
                <input style="display: none" name="email" value="<%=email%>" type="text"/>
                <div class="input-field">
                    <input class="hieu"  type="number" name="num1"/>
                    <input class="hieu"  type="number" name="num2" disabled />
                    <input class="hieu" type="number" name="num3" disabled />
                    <input class="hieu"  type="number" name="num4" disabled />
                </div>

                <button id="button1" type="submit">Verify OTP</button>

            </form>
            <span style="margin-top: 10%; margin-bottom: 2px; color: black">
                Nếu không nhận được email, vui lòng nhấn vào nút dưới đây !!!
            </span>
            <form action="verification" method="post">
                <input hidden="" name="action" value="reOtp"/>
                <input hidden="" name="status" value="<%=status%>"/>
                <input style="display: none" name="email" value="<%=email%>" type="text"/>
                <div style="margin-top: 8px;" class="form-group">
                    <div style="display: flex" class="Captcha dang-ky-wrapper">
                        <div class="column" style="margin-top: 4%; margin-left: 0%;">
                            <img style="border-radius: 10px;" id="captcha-image" src="Captcha" alt="Captcha Image">
                        </div>
                        <button id="refresh-captcha" style=" position: inherit;
                                color: #fff;
                                font-size: 24px;
                                size: 80%;
                                letter-spacing: 0px;
                                background-color: white;
                                border: 0px solid #fff;
                                margin-left: 0%;
                                color: black;
                                margin-right: 3px;
                                width: 20%;
                                margin-top: 2%;
                                " class="refresh-captcha active"><i class="fas fa-sync-alt"></i>
                        </button>
                        <div style="size: 80%; margin-top: 15px;"  class="column dang-ky">
                            <input style="font-size: 14px; padding-right: 0px;padding-top: 3%;padding-bottom: 3%; margin-left: 0%;"  name="captcha" id="captcha-input" type="text" placeholder="Nh?p Captcha" class="form-control"
                                   required="">
                        </div>
                        <button type="submit" style=" position: inherit;
                                color: #fff;
                                font-size: 24px;
                                size: 80%;
                                letter-spacing: 0px;
                                background-color: white;
                                border: 0px solid #fff;
                                margin-left: 0%;
                                color: black;
                                margin-right: 3px;
                                width: 20%;
                                margin-top: 2%;" class="active"><i class="fa-regular fa-paper-plane fa-bounce"></i></button>
                    </div>
                </div> 
            </form>
            <span>Nếu bạn muốn trở lại vui lòng bấm vào <a href="login.jsp"> Đây</a>!</span>
        </div>

        <script src="message.js"></script>
        <script>
            const captchaImage = document.getElementById('captcha-image');
            const captchaInput = document.getElementById('captcha-input');
            const refreshCaptchaButton = document.getElementById('refresh-captcha');
            refreshCaptchaButton.addEventListener('click', refreshCaptcha);
            function refreshCaptcha(event) {
                    event.preventDefault(); // Ng?n ch?n hành vi m?c ??nh c?a nút "Refresh"
                    captchaInput.value = ''; // Xóa giá tr? captcha c?

                    var s = 'Captcha?' + new Date().getTime(); // G?i l?i servlet Captcha v?i th?i gian m?i ?? c?p nh?t hình ?nh captcha
                    captchaImage.src = s;

            }
            const inputs = document.querySelectorAll(".hieu"),
                    button = document.getElementById("button1");
            console.log(inputs);
            // iterate over all inputs
            inputs.forEach((input, index1) => {
                    input.addEventListener("keyup", (e) => {
                            // This code gets the current input element and stores it in the currentInput variable
                            // This code gets the next sibling element of the current input element and stores it in the nextInput variable
                            // This code gets the previous sibling element of the current input element and stores it in the prevInput variable
                            const currentInput = input,
                                    nextInput = input.nextElementSibling,
                                    prevInput = input.previousElementSibling;
                            // if the value has more than one character then clear it
                            if (currentInput.value.length > 1) {
                                    currentInput.value = "";
                                    return;
                            }
                            // if the next input is disabled and the current value is not empty
                            //  enable the next input and focus on it
                            if (nextInput && nextInput.hasAttribute("disabled") && currentInput.value !== "") {
                                    nextInput.removeAttribute("disabled");
                                    nextInput.focus();
                            }
                            // if the backspace key is pressed
                            if (e.key === "Backspace") {
                                    // iterate over all inputs again
                                    inputs.forEach((input, index2) => {
                                            // if the index1 of the current input is less than or equal to the index2 of the input in the outer loop
                                            // and the previous element exists, set the disabled attribute on the input and focus on the previous element
                                            if (index1 <= index2 && prevInput) {
                                                    input.setAttribute("disabled", true);
                                                    input.value = "";
                                                    prevInput.focus();
                                            }
                                    });
                            }
                            //if the fourth input( which index number is 3) is not empty and has not disable attribute then
                            //add active class if not then remove the active class.
                            if (!inputs[3].disabled && inputs[3].value !== "") {
                                    button.classList.add("active");
                                    return;
                            }
                            button.classList.remove("active");
                    });
            });
            //focus the first input which index is 0 on window load
            window.addEventListener("load", () => inputs[0].focus());
            function showErrorToast(error) {
                    toast({
                            title: "Thông báo",
                            message: error,
                            type: "error",
                            duration: 5000
                    });
            }
            const error = document.getElementById("error").value;
            console.log(error);
            if (!error == "") {
                    console.log(error);
                    const myTimeout = setTimeout(showErrorToast(error), 1000)
            }
        </script>
    </body>
</html>