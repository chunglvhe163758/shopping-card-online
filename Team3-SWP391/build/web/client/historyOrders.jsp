<%-- 
    Document   : historyTransaction
    Created on : Jun 20, 2023, 10:44:30 PM
    Author     : hieub
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.text.NumberFormat" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.User" %>
<%@page import = "model.Supplier" %>
<%@page import = "model.Transaction" %>
<%@page import = "model.Order" %>
<%@page import = "dao.TransactionDAO" %>
<%@page import = "dao.OrderDAO" %>
<%@page import = "dao.ProductDAO" %>
<%@page import = "model.Product" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link
            rel="stylesheet"
            href="https://cardvip.vn/assets/default/plugins/bootstrap/css/bootstrap.min.css"
            />
        <style>
            .external-link {
                cursor: pointer;
                color: blue;
            }

            body {
                background: #eee;
            }

            input[type=checkbox] {
                position: relative;
                width: 15px;
                height: 15px;
                color: #000;
                border: 1px solid grey;
                border-radius: 4px;
                appearance: none;
                outline: 0;
                cursor: pointer;
                transition: background 175ms cubic-bezier(0.1, 0.1, 0.25, 1);
                :before {
                    position: absolute;
                    content: '';
                    display: block;
                    top: 2px;
                    left: 7px;
                    width: 8px;
                    height: 16px;
                    border-style: solid;
                    border-color: #fff;
                    border-width: 0 2px 2px 0;
                    transform: rotate(45deg);
                    opacity: 0;
                }
                checked {
                    color: #000;
                    border-color: green;
                    background: green;
                    :before {
                        opacity: 1;
                    }
                    label :before {
                        clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
                    }
                </style>
            </head>
            <%Object obj = session.getAttribute("account");
   User u = null;
   if(obj !=null){
   u = (User)obj;
                }
   TransactionDAO td = new TransactionDAO();
   OrderDAO od = new OrderDAO();
   List<Transaction> lst = (List<Transaction>) td.getTransactionByUser(5,0,"", "2023-02-16T20:39", "2023-07-25 T06:45", "10000", "999999", "", "","","1");
   ProductDAO pd = new ProductDAO();
   List<Integer> listPrice = (List<Integer>) pd.getPriceOfProduct();
   List<Supplier> listSup = (List<Supplier>) pd.getAllSuppliers();
   List<Order> listOrder = (List<Order>) od.getListOrderPage("5","0","","all","2023-02-16T20:39", "2023-07-25 T06:45","","",String.valueOf(u.getId()));
   request.setAttribute("listOd",listOrder);
   request.setAttribute("listSup",listSup);
   request.setAttribute("listPrice",listPrice);
   int count = (int) od.countOrderPage("","all","2023-02-16T20:39", "2023-07-25 T06:45","","",String.valueOf(u.getId()));
   request.setAttribute("list",lst);
            %>
            <jsp:include page="header.jsp"></jsp:include>
                <body style="margin-top: 8%;">
                    <style>
                        /* Thiết lập kích thước và vị trí của form */
                        .popup {
                            width: 300px;
                            height: 200px;
                            position: fixed;
                            top: 50%;
                            left: 50%;
                            margin-top: -100px;
                            margin-left: -150px;
                            border: 1px solid black;
                            background-color: white;
                            display: none;
                        }

                        /* Thiết lập màn đen phủ lên màn hình khi form được hiển thị */
                        .overlay {
                            position: fixed;
                            top: 0;
                            left: 0;
                            width: 100%;
                            height: 100%;
                            background-color: rgba(0,0,0,0.5);
                            display: none;
                        }
                    </style>
                </head>
            <body>
                <style>
                    /* Thiết lập kích thước và vị trí của form */
                    .popup {
                        width: 50%;
                        height: 80%;
                        position: fixed;
                        top: 30%;
                        left: 35%;
                        margin-top: -100px;
                        margin-left: -150px;
                        border: 1px solid black;
                        background-color: white;
                        display: none;
                    }

                    /* Thiết lập màn đen phủ lên màn hình khi form được hiển thị */
                    .overlay {
                        position: fixed;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 100%;
                        background-color: rgba(0,0,0,0.5);
                        display: none;
                    }
                </style>

                <style>
                    .card {
                        margin: auto;
                        width: 150%;
                        max-width: 600px;
                        padding: 4vh 0;
                        box-shadow: 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                        border-top: 3px solid rgb(252, 103, 49);
                        border-bottom: 3px solid rgb(252, 103, 49);
                        border-left: none;
                        border-right: none;
                    }

                    .title {
                        color: rgb(252, 103, 49);
                        font-weight: 600;
                        margin-bottom: 2vh;
                        padding: 0 8%;
                        font-size: initial;
                    }
                    #details {
                        font-weight: 400;
                    }
                    .info {
                        padding: 5% 8%;
                    }
                    .info .col-5 {
                        padding: 0;
                    }
                    #heading {
                        color: grey;
                        line-height: 6vh;
                    }
                    .pricing {
                        background-color: #ddd3;
                        padding: 2vh 8%;
                        font-weight: 400;
                        line-height: 2.5;
                    }
                    .pricing .col-3 {
                        padding: 0;
                    }
                    .total {
                        padding: 2vh 8%;
                        color: rgb(252, 103, 49);
                        font-weight: bold;
                    }
                    .total .col-3 {
                        padding: 0;
                    }
                    .footer {
                        padding: 0 8%;
                        font-size: x-small;
                        color: black;
                    }
                    .footer img {
                        height: 5vh;
                        opacity: 0.2;
                    }
                    .footer a {
                        color: rgb(252, 103, 49);
                    }
                    .footer .col-10,
                    .col-2 {
                        display: flex;
                        padding: 3vh 0 0;
                        align-items: center;
                    }
                    .footer .row {
                        margin: 0;
                    }
                    #progressbar {
                        margin-bottom: 3vh;
                        overflow: hidden;
                        color: rgb(252, 103, 49);
                        padding-left: 0px;
                        margin-top: 3vh;
                    }

                    #progressbar li {
                        list-style-type: none;
                        font-size: x-small;
                        width: 25%;
                        float: left;
                        position: relative;
                        font-weight: 400;
                        color: rgb(160, 159, 159);
                    }

                    #progressbar #step1:before {
                        content: "";
                        color: rgb(252, 103, 49);
                        width: 5px;
                        height: 5px;
                        margin-left: 0px !important;
                        /* padding-left: 11px !important */
                    }

                    #progressbar #step2:before {
                        content: "";
                        color: #fff;
                        width: 5px;
                        height: 5px;
                        margin-left: 32%;
                    }

                    #progressbar #step3:before {
                        content: "";
                        color: #fff;
                        width: 5px;
                        height: 5px;
                        margin-right: 32%;
                        /* padding-right: 11px !important */
                    }

                    #progressbar #step4:before {
                        content: "";
                        color: #fff;
                        width: 5px;
                        height: 5px;
                        margin-right: 0px !important;
                        /* padding-right: 11px !important */
                    }

                    #progressbar li:before {
                        line-height: 29px;
                        display: block;
                        font-size: 12px;
                        background: #ddd;
                        border-radius: 50%;
                        margin: auto;
                        z-index: -1;
                        margin-bottom: 1vh;
                    }

                    #progressbar li:after {
                        content: "";
                        height: 2px;
                        background: #ddd;
                        position: absolute;
                        left: 0%;
                        right: 0%;
                        margin-bottom: 2vh;
                        top: 1px;
                        z-index: 1;
                    }
                    .progress-track {
                        padding: 0 8%;
                    }
                    #progressbar li:nth-child(2):after {
                        margin-right: auto;
                    }

                    #progressbar li:nth-child(1):after {
                        margin: auto;
                    }

                    #progressbar li:nth-child(3):after {
                        float: left;
                        width: 68%;
                    }
                    #progressbar li:nth-child(4):after {
                        margin-left: auto;
                        width: 132%;
                    }

                    #progressbar li.active {
                        color: black;
                    }

                    #progressbar li.active:before,
                        #progressbar li.active:after {
                        background: rgb(252, 103, 49);
                    }
                    .card {
                        display: flex;
                        flex-direction: column;
                    }

                    .row {
                        display: flex;
                        flex-direction: row;
                        align-items: center;
                        justify-content: space-between;
                    }

                    .col-7,
                    .col-5,
                    .col-9,
                    .col-3 {
                        flex-basis: 0;
                        flex-grow: 1;
                        white-space: nowrap;
                    }
                    .card{
                        background: #ddd3;
                        height: 100vh;
                        vertical-align: middle;
                        display: flex;
                        font-family: Muli;
                        font-size: 14px;
                    }
                </style>
            </head>
        <body>

            
            <!-- Form và màn đen phủ lên màn hình -->
            <div class="overlay"></div>
            <div class="popup">
                <button onclick="closePopup()">X</button>
                <div id="card" class="card">
                    <div class="title">Purchase Receipt</div>
                    <div class="info">
                        <div class="row">
                            <div class="col-7">
                                <span id="heading">Date</span>
                                <span id="details">10 October 2018</span>
                            </div>
                            <div class="col-5">
                                <span id="heading">Order No.</span>
                                <span id="details">012j1gvs356c</span>
                            </div>
                        </div>
                    </div>
                    <div class="pricing">
                        <div class="row">
                            <div class="col-9">
                                <span >BEATS Solo 3 Wireless Headphones</span>
                            </div>
                            <div class="col-3">
                                <span >&pound;299.99</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-9">
                                <span >Shipping</span>
                            </div>
                            <div class="col-3">
                                <span>&pound;33.00</span>
                            </div>
                        </div>
                    </div>
                    <div class="total">
                        <div class="row">
                            <div class="col-9"></div>
                            <div class="col-3"></div>
                        </div>
                        <div class="tracking">
                            <div class="title">Tracking Order</div>
                        </div>
                        <br>
                        <div class="progress-track">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <span >Code:</span>
                                        <span >2939298328828</span>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <span >Seri:</span>
                                        <span >2939298328828</span>
                                        
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>


        <script>
            function openPopup() {
                // Lấy các đối tượng HTML
                var overlay = document.querySelector('.overlay');
                var popup = document.querySelector('.popup');

                // Hiển thị form và màn đen phủ lên màn hình
                overlay.style.display = 'block';
                popup.style.display = 'block';

                // Thêm sự kiện click vào màn đen để tắt form
                overlay.addEventListener('click', closePopup);
            }

            function closePopup() {
                // Lấy các đối tượng HTML
                var overlay = document.querySelector('.overlay');
                var popup = document.querySelector('.popup');

                // Ẩn form và màn đen phủ lên màn hình
                overlay.style.display = 'none';
                popup.style.display = 'none';

                // Xóa sự kiện click khỏi màn đen
                overlay.removeEventListener('click', closePopup);
            }
        </script>
 <script>
        function showValue(button) {
            const value = button.value;
              $.ajax({
                url: "/autothecao/ShowOrderDetail",
                type: "get", //send it through get method
                data: {
                   id: value
                },
                success: function (data) {
                    var row = document.getElementById("card");
                    row.innerHTML = data;
                    openPopup();
                },
                error: function (xhr) {
                    document.window.alert("hieuuu");
                    lert("loi load page");
                }
            });
        }

    </script>
        <script>
            function clickcopy() {
                var copyText = this.getAttribute("data-copy-text");
                var textarea = document.createElement("textarea");
                textarea.textContent = copyText;
                document.body.appendChild(textarea);

                textarea.select();
                document.execCommand("copy");
                textarea.remove();
            }

            var buttons = document.querySelectorAll("[data-copy-text]");
            buttons.forEach(function (button) {
                button.addEventListener("click", clickcopy);
            });
        </script>
        <input hidden="" id="count" value="<%=count%>"/>
    <input hidden="" id="user" value="<%=u.getId()%>"/>
    <div style="width: 90%;
         margin-left: 5%;" >
        <div class="table-responsive">
            <table class="table table-striped table-dark text-dark" style="background-color: white;">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th colspan="2">Mã giao dịch</th>
                        <th>Giá thẻ</th>
                        <th colspan="2">Ngày mua</th>
                        <th>Loại thẻ</th>
                        <th>Trạng thái</th>
                        <th>Số lượng</th>
                        <th>Chi tiết</th>
                    </tr>
                </thead>
                <tr id="loc">
                    <td>Lọc</td>
                    <td colspan="2" style="width: 18%;"><input style="width: 50%;" name="timma" id="transac" placeholder="Mã GD" onchange="search()"/></td>
                    <td style="width: 15%;">
                        <select id="price" onchange="search()">
                            <option value="all" >All</option>
                            <c:forEach items="${listPrice}" var="lpri">
                                <option value="${lpri}"><fmt:formatNumber value="${lpri}" type="number" pattern="#,###" /></option>
                            </c:forEach>
                        </select>
                        <span>VND</span>
                    </td>
                    <td colspan="2" style="width: 10%;">
                        <input type="datetime-local" style="width: 35%;" name="timeFrom" id="timeFrom" placeholder="Mã GD" onchange="search()"/>
                        <input type="datetime-local" style="width: 35%;" name="timeTo" id="timeTo" placeholder="Mã GD" onchange="search()"/>
                    </td>

                    <td>
                        <select id="name" name="name" onchange="search()">
                            <option value="">All</option>
                            <c:forEach items="${listSup}" var="listSup">
                                <option value="${listSup.name}">${listSup.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>
                        <select id="status" name="status" onchange="search()">
                            <option value="">All</option>
                            <option value="success">Thành công</option>
                            <option value="fail">Thất bại</option>
                        </select>
                    </td>
                    <td>

                    </td>
                </tr>
                <tbody id="content1">
                    <c:set var="i" value="${1}"></c:set>
                    <c:forEach items="${listOd}" var="listOd">


                        <tr>
                            <td>${i}</td>
                            <c:set var="i" value="${i+1}"></c:set>
                            <td colspan="2" style="width: 18%;">${listOd.id}</td>
                            <td style="width: 15%;"><fmt:formatNumber value="${listOd.price}" type="number" pattern="#,###" /></td>
                            <td colspan="2" style="width: 10%;">${listOd.createdAt}</td>
                            <td>${listOd.name}</td>
                            <td>${listOd.status}</td>
                            <td>${listOd.quantity}</td>
                            <td><button class="my-button" value="${listOd.id}" onclick="showValue(this)">Chi tiết</button></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

        <div class="pagination-bottom" style="display: flex;">
            <div class="-pagination" style="display: flex;
                 position: fixed;
                 bottom: 0;
                 left: 0;
                 right: 0;
                 background-color: white;
                 z-index: 1;
                 display: -ms-flexbox;
                 display: flex;
                 -ms-flex-pack: justify;
                 justify-content: space-between;
                 -ms-flex-align: stretch;
                 align-items: stretch;
                 -ms-flex-wrap: wrap;
                 flex-wrap: wrap;
                 padding: 3px;
                 -webkit-box-shadow: 0 0 15px 0 rgba(0,0,0,0.1);
                 box-shadow: 0 0 15px 0 rgba(0,0,0,0.1);
                 border-top: 2px solid rgba(0,0,0,0.1);">
                <div class="-previous" style="display: flex;"><button id="nuttruoc" type="button" disabled="" class="-btn">Trước</button></div>
                <div class="-center" style="display: flex;"><span style="display: flex;" class="-pageInfo">Trang <div class="-pageJump"><input id="page" type="number" value="1"
                                                                                                                                               fdprocessedid="2tada8" min="1" max="" onchange="loadPage()"></div> / <span id="showpage" class="-totalPages">1</span></span>
                    <span class="select-wrap -pageSizeOptions">
                        <select id="selectpage" fdprocessedid="g1rmyr" onchange="loadGhi()">
                            <option value="5">5 Bản ghi</option>
                            <option value="10">10 Bản ghi</option>
                            <option value="20">20 Bản ghi</option>
                            <option value="25">25 Bản ghi</option>
                            <option value="50">50 Bản ghi</option>                                        
                        </select>
                    </span>
                </div>
                <div class="-next"><button id="nutsau" type="button" disabled="" class="-btn">Sau</button></div>
            </div>
        </div>
    </div>
   
    <script>
        // Lấy ngày hiện tại
        var now = new Date();
// Tính toán ngày trước đó 30 ngày
        var bientami = new Date();
        now.setDate(bientami.getDate() + 1);
        var thirtyDaysAgo = new Date(now.getTime() - 30 * 24 * 60 * 60 * 1000);
// Định dạng ngày cho định dạng chuẩn ISO 8601
        var fromDateString = thirtyDaysAgo.toISOString().slice(0, 16);
        var toDateString = now.toISOString().slice(0, 16);
// Đặt giá trị cho các thẻ input
        document.getElementById('timeFrom').value = fromDateString;
        document.getElementById('timeTo').value = toDateString;
        var selectpage = document.getElementById("selectpage");
        var selected = parseFloat(selectpage.value);
        var total = parseFloat(document.getElementById("count").value);
        console.log("tong trang page" + total + ";" + "tong trang chon" + selected);
        console.log(typeof total);
        var page = Math.ceil(total / selected);
        var inputpage = document.getElementById("page");
        var showpage = document.getElementById("showpage").innerHTML = page;
        inputpage.setAttribute("max", page);
        console.log(page);
        function loadGhi() {
            var timma = document.getElementById("transac");
            var price = document.getElementById("price");
            var name = document.getElementById("name");
            var status = document.getElementById("status");
            var dateFrom = document.getElementById("timeFrom");
            var dateTo = document.getElementById("timeTo");
            var dateFromValue = dateFrom.value;
            var dateToValue = dateTo.value;
            //var amount = document.getElementsByClassName("product").length;
            var totalPage = parseFloat(document.getElementById("count").value);
            var userid = document.getElementById("user").value;
            var pageNow = parseFloat(document.getElementById("selectpage").value); // ban ghi
            var selectedpage = document.getElementById("page").value;
            var countPage = Math.ceil(totalPage / pageNow);
            var nutsau = document.getElementById("nutsau");
            if (countPage > 1) {
                nutsau.removeAttribute("disabled");
            }
            var showpage = document.getElementById("showpage").innerHTML = countPage;
            document.getElementById("page").value = 1;
            inputpage.setAttribute("max", countPage);
            $.ajax({
                url: "/autothecao/HistoryOrders",
                type: "get", //send it through get method
                data: {
                    page: 1,
                    ghi: pageNow,
                    totalGhi: totalPage,
                    price: price.value,
                    timeFrom: dateFromValue,
                    timeTo: dateToValue,
                    name: name.value,
                    status: status.value,

                    id: timma.value,
                    user: userid
                },
                success: function (data) {
                    var row = document.getElementById("content1");
                    row.innerHTML = data;
                },
                error: function (xhr) {
                    document.window.alert("hieuuu");
                    //Do Something to handle error
                    alert("loi load ghi");
                }
            });
        }
        function loadPage() {
            var price = document.getElementById("price");
            var transac = document.getElementById("transac");
            var dateFrom = document.getElementById("timeFrom");
            var dateTo = document.getElementById("timeTo");
            var dateFromValue = dateFrom.value;
            var dateToValue = dateTo.value;
            var name = document.getElementById("name");
            var status = document.getElementById("status");

            //var amount = document.getElementsByClassName("product").length;
            var userid = document.getElementById("user").value;
            var pageNow = document.getElementById("selectpage").value; // ban ghi
            var selectedpage = document.getElementById("page").value;
            var totalPage = parseFloat(document.getElementById("count").value);
            $.ajax({
                url: "/autothecao/HistoryOrders",
                type: "get", //send it through get method
                data: {
                    page: selectedpage,
                    ghi: pageNow,
                    totalGhi: totalPage,
                    price: price.value,
                    timeFrom: dateFromValue,
                    timeTo: dateToValue,
                    name: name.value,
                    status: status.value,
                    id: transac.value,
                    user: userid
                },
                success: function (data) {
                    var row = document.getElementById("content1");
                    row.innerHTML = data;
                },
                error: function (xhr) {
                    document.window.alert("hieuuu");
                    lert("loi load page");
                }
            });
        }

        function search() {
            console.log("da qua day");
            var userid = document.getElementById("user").value;
            var price = document.getElementById("price").value;
            console.log(price);
            var totalPage = parseFloat(document.getElementById("count").value);

            var transac = document.getElementById("transac");
            var dateFrom = document.getElementById("timeFrom");
            var dateTo = document.getElementById("timeTo");
            var dateFromValue = dateFrom.value;
            var dateToValue = dateTo.value;
            var tomorrow = new Date(now.getTime() + (24 * 60 * 60 * 1000));
            tomorrow.setDate(now.getDate() + 1);
            if (dateToValue > tomorrow.value) {
                alert("Xin lỗi, vui lòng chọn ngày trước ngày hiện tại");
                dateTo.value = tomorrow.value;
                return;
            }
            if (dateFromValue > dateToValue) {
                alert("Xin lỗi, vui lòng nhập ngày bắt đầu nhỏ hơn ngày đến.");
                var datetam = new Date(dateToValue);
                var datemoi = new Date(datetam.getTime() - 30 * 24 * 60 * 60 * 1000);
                var fromDateString1 = datemoi.toISOString().slice(0, 16);
                dateFrom.value = fromDateString1;
                return;
            }
            console.log(dateFromValue + " from:to " + dateToValue);

            var status = document.getElementById("status");
            var name = document.getElementById("name").value;
            $.ajax({
                url: "/autothecao/CountOrders",
                type: "get", //send it through get method
                data: {
                    price: price,
                    timeFrom: dateFromValue,
                    timeTo: dateToValue,
                    id: transac.value,
                    name: name,
                    status: status.value,
                    user: userid
                },
                success: function (data) {
                    var numberData = parseInt(data);
                    var changeCount = document.getElementById("count");
                    changeCount.value = numberData;
                    changeCount.setAttribute("value", numberData);
                    if (numberData === 0) {
                        var row = document.getElementById("content1");

                    }
                    var countPage = Math.ceil(numberData / 5);
                    document.getElementById("page").value = 1;
                    document.getElementById("selectpage").value = 5;
                    if (countPage === 0)
                        countPage = 1;
                    inputpage.setAttribute("max", countPage);
                    document.getElementById("showpage").innerHTML = countPage;
                },
                error: function (xhr) {
                    document.window.alert("hieuuu");
                    //Do Something to handle error
                }
            });
            $.ajax({
                url: "/autothecao/HistoryOrders",
                type: "get", //send it through get method
                data: {
                    page: 1,
                    ghi: 5,
                    totalGhi: totalPage,
                    price: price,
                    timeFrom: dateFromValue,
                    timeTo: dateToValue,
                    id: transac.value,
                    name: name,
                    status: status.value,
                    user: userid
                },
                success: function (data) {
                    var row = document.getElementById("content1");
                    row.innerHTML = data;
                },
                error: function (xhr) {
                    document.window.alert("hieuuu");
                    //Do Something to handle error
                    alert("loi load ghi");
                }
            });
        }
        ;
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>
